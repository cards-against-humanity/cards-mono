#[doc = r" Generated client implementations."]
pub mod admin_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct AdminServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl AdminServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> AdminServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn clear_user_search_index(
            &mut self,
            request: impl tonic::IntoRequest<()>,
        ) -> Result<tonic::Response<()>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.AdminService/ClearUserSearchIndex",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn refresh_user_search_index(
            &mut self,
            request: impl tonic::IntoRequest<()>,
        ) -> Result<tonic::Response<()>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.AdminService/RefreshUserSearchIndex",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for AdminServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for AdminServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "AdminServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod admin_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with AdminServiceServer."]
    #[async_trait]
    pub trait AdminService: Send + Sync + 'static {
        async fn clear_user_search_index(
            &self,
            request: tonic::Request<()>,
        ) -> Result<tonic::Response<()>, tonic::Status>;
        async fn refresh_user_search_index(
            &self,
            request: tonic::Request<()>,
        ) -> Result<tonic::Response<()>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct AdminServiceServer<T: AdminService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: AdminService> AdminServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for AdminServiceServer<T>
    where
        T: AdminService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/crusty_cards_api.AdminService/ClearUserSearchIndex" => {
                    #[allow(non_camel_case_types)]
                    struct ClearUserSearchIndexSvc<T: AdminService>(pub Arc<T>);
                    impl<T: AdminService> tonic::server::UnaryService<()> for ClearUserSearchIndexSvc<T> {
                        type Response = ();
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<()>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).clear_user_search_index(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ClearUserSearchIndexSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.AdminService/RefreshUserSearchIndex" => {
                    #[allow(non_camel_case_types)]
                    struct RefreshUserSearchIndexSvc<T: AdminService>(pub Arc<T>);
                    impl<T: AdminService> tonic::server::UnaryService<()> for RefreshUserSearchIndexSvc<T> {
                        type Response = ();
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(&mut self, request: tonic::Request<()>) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).refresh_user_search_index(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = RefreshUserSearchIndexSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: AdminService> Clone for AdminServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: AdminService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: AdminService> tonic::transport::NamedService for AdminServiceServer<T> {
        const NAME: &'static str = "crusty_cards_api.AdminService";
    }
}
/// Top-level resource representing a user.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct User {
    /// The resource name of the user.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The display name of the user.
    #[prost(string, tag = "2")]
    pub display_name: ::prost::alloc::string::String,
    /// When the user was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
    /// When the user was last modified.
    #[prost(message, optional, tag = "4")]
    pub update_time: ::core::option::Option<::prost_types::Timestamp>,
}
/// Singleton resource belonging to a user.
/// Contains bytes representing an image.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserProfileImage {
    /// The resource name of the user profile image.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// Image data. Empty by default for new users.
    #[prost(bytes = "vec", tag = "2")]
    pub image_data: ::prost::alloc::vec::Vec<u8>,
}
/// Configuration settings used to create a game.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GameConfig {
    /// The displayed name of the game.
    /// Does not have to be unique among all
    /// games since each game has a unique game_id.
    #[prost(string, tag = "1")]
    pub display_name: ::prost::alloc::string::String,
    /// The maximum number of players that can be in the game at once.
    /// Artificial players do not count towards this limit.
    /// Value must be at least 3 and at most 100.
    #[prost(int32, tag = "2")]
    pub max_players: i32,
    /// The hand limit of each player.
    #[prost(int32, tag = "5")]
    pub hand_size: i32,
    /// A list of custom cardpacks that should be used when making the game.
    /// All cards from all custom cardpacks listed will be pulled from
    /// the main api at game creation.
    #[prost(string, repeated, tag = "6")]
    pub custom_cardpack_names: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
    /// A list of default cardpacks that should be used when making the game.
    /// All cards from all default cardpacks listed will be pulled from
    /// the main api at game creation.
    #[prost(string, repeated, tag = "7")]
    pub default_cardpack_names: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
    /// Defines how blank white cards behave in-game
    /// and how many should be added to the deck.
    #[prost(message, optional, tag = "8")]
    pub blank_white_card_config: ::core::option::Option<game_config::BlankWhiteCardConfig>,
    /// The condition through which
    /// a game will automatically end.
    #[prost(oneof = "game_config::EndCondition", tags = "3, 4")]
    pub end_condition: ::core::option::Option<game_config::EndCondition>,
}
/// Nested message and enum types in `GameConfig`.
pub mod game_config {
    /// Settings defining how blank white cards behave in-game.
    #[derive(Clone, PartialEq, ::prost::Message)]
    pub struct BlankWhiteCardConfig {
        /// Specifies behavior of blank white cards.
        #[prost(enumeration = "blank_white_card_config::Behavior", tag = "1")]
        pub behavior: i32,
        #[prost(oneof = "blank_white_card_config::BlankWhiteCardsAdded", tags = "2, 3")]
        pub blank_white_cards_added:
            ::core::option::Option<blank_white_card_config::BlankWhiteCardsAdded>,
    }
    /// Nested message and enum types in `BlankWhiteCardConfig`.
    pub mod blank_white_card_config {
        /// How blank white cards can be used.
        #[derive(
            Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration,
        )]
        #[repr(i32)]
        pub enum Behavior {
            /// Default value. This value is unused.
            Unspecified = 0,
            /// No blank white cards are added to the deck.
            Disabled = 1,
            /// Allow users to play blank white cards from their hand
            /// and write any text (limited to 100 characters).
            OpenText = 2,
            /// Similar to OPEN_TEXT, except the card text must exactly match
            /// another white card that is in the current game. To search for
            /// cards to copy, use the ListWhiteCardTexts rpc.
            DuplicateText = 3,
        }
        #[derive(Clone, PartialEq, ::prost::Oneof)]
        pub enum BlankWhiteCardsAdded {
            /// Adds a fixed number of blank white cards to the deck.
            #[prost(int32, tag = "2")]
            CardCount(i32),
            /// A number between 0.0 and 0.8 specifying what percentage of the total
            /// deck size should be blank white cards. For example, if there are 900
            /// non-blank white cards in your game and you specify a percentage
            /// of 0.1, there will be 100 blank white cards added for a total of 1000.
            ///
            /// The upper limit is 0.8 because, as this value approaches 1.0, the
            /// number of blank white cards needed increases exponentially. A value of 0.5
            /// doubles the size of the deck, a value of 0.8 increases the size by 5x
            /// and a value of 0.99 would increase it by 100x.
            #[prost(double, tag = "3")]
            Percentage(f64),
        }
    }
    /// The condition through which
    /// a game will automatically end.
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum EndCondition {
        /// Once a player reaches this score, the game
        /// is stopped automatically and that player wins.
        #[prost(int32, tag = "3")]
        MaxScore(i32),
        /// The game never ends automatically.
        /// Scores are still tracked.
        #[prost(message, tag = "4")]
        EndlessMode(()),
    }
}
/// Singleton resource belonging to a user. Contains user-specific settings.
/// Exactly one exists for every user, and are created automatically whenever a user is created.
/// All values are unset for new users and can only be changed by calling UpdateUserSettings.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserSettings {
    /// The resource name of the user settings.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The color scheme to show for the user.
    #[prost(enumeration = "user_settings::ColorScheme", tag = "2")]
    pub color_scheme: i32,
    /// Default game settings for a user to quickly start a game.
    #[prost(message, optional, tag = "3")]
    pub quick_start_game_config: ::core::option::Option<GameConfig>,
}
/// Nested message and enum types in `UserSettings`.
pub mod user_settings {
    /// Color schemes the frontend supports.
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum ColorScheme {
        /// Default value. This value is unused.
        Unspecified = 0,
        /// Default color scheme for new users.
        DefaultLight = 1,
        /// Dark mode.
        DefaultDark = 2,
    }
}
/// Resource belonging to a user. Represents a collection
/// of related cards that can be used when creating games.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CustomCardpack {
    /// The resource name of the custom cardpack.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The display name of the custom cardpack.
    #[prost(string, tag = "2")]
    pub display_name: ::prost::alloc::string::String,
    /// When the custom cardpack was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
    /// This field is reset anytime any changes are made to the custom cardpack including adding, modifying
    /// and deleting cards as well as renaming the custom cardpack.
    #[prost(message, optional, tag = "4")]
    pub update_time: ::core::option::Option<::prost_types::Timestamp>,
    /// If this field is not empty, it indicates that the custom cardpack has been deleted. CustomCardpacks that
    /// have been deleted are guaranteed to remain available to be undeleted for at least 30 days.
    #[prost(message, optional, tag = "5")]
    pub delete_time: ::core::option::Option<::prost_types::Timestamp>,
}
/// A black card. Behaves like a black card in Cards Against Humanity.
/// Belongs to a CustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CustomBlackCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The content of the card. Limited to 300 characters.
    /// Whitespace at the beginning and end is automatically trimmed when this field is set or modified.
    #[prost(string, tag = "2")]
    pub text: ::prost::alloc::string::String,
    /// The number of white cards that should be played when this card is used.
    /// Value must be 1, 2, or 3.
    #[prost(int32, tag = "3")]
    pub answer_fields: i32,
    /// When the card was created.
    #[prost(message, optional, tag = "4")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
    /// When the card was last modified.
    #[prost(message, optional, tag = "5")]
    pub update_time: ::core::option::Option<::prost_types::Timestamp>,
    /// If this field is not empty, it indicates that the card has been deleted. Cards that
    /// have been deleted are guaranteed to remain available to be undeleted for at least 30 days.
    #[prost(message, optional, tag = "6")]
    pub delete_time: ::core::option::Option<::prost_types::Timestamp>,
}
/// A white card. Behaves like a white card in Cards Against Humanity.
/// Belongs to a CustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CustomWhiteCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The content of the card. Limited to 100 characters.
    /// Whitespace at the beginning and end is automatically trimmed when this field is set or modified.
    #[prost(string, tag = "2")]
    pub text: ::prost::alloc::string::String,
    /// When the card was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
    /// When the card was last modified.
    #[prost(message, optional, tag = "4")]
    pub update_time: ::core::option::Option<::prost_types::Timestamp>,
    /// If this field is not empty, it indicates that the card has been deleted. Cards that
    /// have been deleted are guaranteed to remain available to be undeleted for at least 30 days.
    #[prost(message, optional, tag = "5")]
    pub delete_time: ::core::option::Option<::prost_types::Timestamp>,
}
/// Represents a collection of related cards that can be used when creating games.
/// Different from a custom cardpack because this is a top-level resource and therefore
/// has no owner, and it is immutable along with all cards in it.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DefaultCardpack {
    /// The resource name of the default cardpack.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The display name of the default cardpack.
    #[prost(string, tag = "2")]
    pub display_name: ::prost::alloc::string::String,
}
/// An immutable default black card. Behaves like a black card in Cards Against Humanity.
/// Belongs to a DefaultCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DefaultBlackCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The content of the card.
    #[prost(string, tag = "2")]
    pub text: ::prost::alloc::string::String,
    /// The number of white cards that should be played when this card is used.
    /// Value must be 1, 2, or 3.
    #[prost(int32, tag = "3")]
    pub answer_fields: i32,
}
/// An immutable default white card. Behaves like a white card in Cards Against Humanity.
/// Belongs to a DefaultCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DefaultWhiteCard {
    /// The resource name of the card.
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
    /// The content of the card.
    #[prost(string, tag = "2")]
    pub text: ::prost::alloc::string::String,
}
/// Request message for CreateCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateCustomCardpackRequest {
    /// The parent user.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The custom cardpack to create.
    #[prost(message, optional, tag = "2")]
    pub custom_cardpack: ::core::option::Option<CustomCardpack>,
}
/// Request message for GetCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetCustomCardpackRequest {
    /// The name of the custom cardpack to retrieve.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for ListCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomCardpacksRequest {
    /// The parent user.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
    /// Show soft-deleted custom cardpacks instead.
    #[prost(bool, tag = "4")]
    pub show_deleted: bool,
}
/// Response message for ListCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomCardpacksResponse {
    /// The requested custom cardpacks.
    #[prost(message, repeated, tag = "1")]
    pub custom_cardpacks: ::prost::alloc::vec::Vec<CustomCardpack>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for UpdateCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateCustomCardpackRequest {
    /// The custom cardpack to update.
    #[prost(message, optional, tag = "1")]
    pub custom_cardpack: ::core::option::Option<CustomCardpack>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::core::option::Option<::prost_types::FieldMask>,
}
/// Request message for DeleteCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteCustomCardpackRequest {
    /// The name of the custom cardpack to delete.
    /// Deleted custom cardpacks are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for CreateCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateCustomBlackCardRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The black card to create.
    #[prost(message, optional, tag = "2")]
    pub custom_black_card: ::core::option::Option<CustomBlackCard>,
}
/// Request message for CreateCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateCustomWhiteCardRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The white card to create.
    #[prost(message, optional, tag = "2")]
    pub custom_white_card: ::core::option::Option<CustomWhiteCard>,
}
/// Request message for ListCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomBlackCardsRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
    /// Show soft-deleted cards instead.
    #[prost(bool, tag = "4")]
    pub show_deleted: bool,
}
/// Response message for ListCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomBlackCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub custom_black_cards: ::prost::alloc::vec::Vec<CustomBlackCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for ListCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomWhiteCardsRequest {
    /// The parent custom cardpack.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
    /// Show soft-deleted cards instead.
    #[prost(bool, tag = "4")]
    pub show_deleted: bool,
}
/// Response message for ListCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomWhiteCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub custom_white_cards: ::prost::alloc::vec::Vec<CustomWhiteCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for UpdateCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateCustomBlackCardRequest {
    /// The card to update.
    #[prost(message, optional, tag = "1")]
    pub custom_black_card: ::core::option::Option<CustomBlackCard>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::core::option::Option<::prost_types::FieldMask>,
}
/// Request message for UpdateCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateCustomWhiteCardRequest {
    /// The card to update.
    #[prost(message, optional, tag = "1")]
    pub custom_white_card: ::core::option::Option<CustomWhiteCard>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::core::option::Option<::prost_types::FieldMask>,
}
/// Request message for DeleteCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteCustomBlackCardRequest {
    /// The name of the black card to delete.
    /// Deleted black cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/blackCards/{custom_black_card}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for DeleteCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteCustomWhiteCardRequest {
    /// The name of the white card to delete.
    /// Deleted white cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/whiteCards/{custom_white_card}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for BatchCreateCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BatchCreateCustomBlackCardsRequest {
    /// The parent resource shared by all cards being created.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    /// The `parent` field in all nested request messages
    /// must either be empty or match this field.
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The request message specifying the resources to create.
    /// A maximum of 10000 cards can be created in a batch.
    #[prost(message, repeated, tag = "2")]
    pub requests: ::prost::alloc::vec::Vec<CreateCustomBlackCardRequest>,
}
/// Response message for BatchCreateCustomBlackCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BatchCreateCustomBlackCardsResponse {
    /// Black cards created. Guaranteed to be in the same order as the request.
    #[prost(message, repeated, tag = "1")]
    pub custom_black_cards: ::prost::alloc::vec::Vec<CustomBlackCard>,
}
/// Request message for BatchCreateCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BatchCreateCustomWhiteCardsRequest {
    /// The parent resource shared by all cards being created.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    /// The `parent` field in all nested request messages
    /// must either be empty or match this field.
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The request message specifying the resources to create.
    /// A maximum of 10000 cards can be created in a batch.
    #[prost(message, repeated, tag = "2")]
    pub requests: ::prost::alloc::vec::Vec<CreateCustomWhiteCardRequest>,
}
/// Response message for BatchCreateCustomWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BatchCreateCustomWhiteCardsResponse {
    /// White cards created. Guaranteed to be in the same order as the request.
    #[prost(message, repeated, tag = "1")]
    pub custom_white_cards: ::prost::alloc::vec::Vec<CustomWhiteCard>,
}
/// Request message for GetDefaultCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetDefaultCardpackRequest {
    /// The name of the default cardpack to retrieve.
    /// Format: defaultCardpacks/{default_cardpack}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for ListDefaultCardpacks. Items are not sorted by any particular field, but rather by importance (i.e. base packs are first, then large expansion packs, etc.).
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListDefaultCardpacksRequest {
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "1")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "2")]
    pub page_token: ::prost::alloc::string::String,
}
/// Response message for ListDefaultCardpacks.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListDefaultCardpacksResponse {
    /// The requested default cardpacks.
    #[prost(message, repeated, tag = "1")]
    pub default_cardpacks: ::prost::alloc::vec::Vec<DefaultCardpack>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for ListDefaultBlackCards. Items are sorted by `text`.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListDefaultBlackCardsRequest {
    /// The parent default cardpack.
    /// Format: defaultCardpacks/{default_cardpack}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
}
/// Response message for ListDefaultBlackCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListDefaultBlackCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub default_black_cards: ::prost::alloc::vec::Vec<DefaultBlackCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for ListDefaultWhiteCards. Items are sorted by `text`.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListDefaultWhiteCardsRequest {
    /// The parent default cardpack.
    /// Format: defaultCardpacks/{default_cardpack}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
}
/// Response message for ListDefaultWhiteCards.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListDefaultWhiteCardsResponse {
    /// The requested cards.
    #[prost(message, repeated, tag = "1")]
    pub default_white_cards: ::prost::alloc::vec::Vec<DefaultWhiteCard>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
/// Request message for UndeleteCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UndeleteCustomCardpackRequest {
    /// The name of the custom cardpack to undelete.
    /// Deleted custom cardpacks are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for UndeleteCustomBlackCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UndeleteCustomBlackCardRequest {
    /// The name of the black card to undelete.
    /// Deleted black cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/blackCards/{custom_black_card}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for UndeleteCustomWhiteCard.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UndeleteCustomWhiteCardRequest {
    /// The name of the white card to undelete.
    /// Deleted white cards are guaranteed to remain available to be undeleted for at least 30 days.
    /// Format: users/{user}/cardpacks/{custom_cardpack}/whiteCards/{custom_white_card}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for ListFavoritedCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListFavoritedCustomCardpacksRequest {
    /// The parent user.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub parent: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
}
/// Response message for ListFavoritedCustomCardpacks.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListFavoritedCustomCardpacksResponse {
    /// The requested custom cardpacks.
    /// The user's favorite list contains references to cardpacks, so if
    /// a cardpack in the list is deleted, the reference still exists in
    /// the user's favorites. When this occurs, a completely empty proto
    /// is included in this repeated field. Also, soft-deleted cardpacks
    /// do still show up in this list - an empty proto is only used when
    /// the cardpack has been permanently deleted.
    #[prost(message, repeated, tag = "1")]
    pub custom_cardpacks: ::prost::alloc::vec::Vec<CustomCardpack>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
}
/// Request message for LikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LikeCustomCardpackRequest {
    /// The user who is liking the custom cardpack.
    #[prost(string, tag = "1")]
    pub user: ::prost::alloc::string::String,
    /// The custom cardpack they are liking.
    #[prost(string, tag = "2")]
    pub custom_cardpack: ::prost::alloc::string::String,
}
/// Request message for UnlikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UnlikeCustomCardpackRequest {
    /// The user who is unliking the custom cardpack.
    #[prost(string, tag = "1")]
    pub user: ::prost::alloc::string::String,
    /// The custom cardpack they are unliking.
    #[prost(string, tag = "2")]
    pub custom_cardpack: ::prost::alloc::string::String,
}
/// Request message for CheckDoesUserLikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CheckDoesUserLikeCustomCardpackRequest {
    /// The user to check.
    #[prost(string, tag = "1")]
    pub user: ::prost::alloc::string::String,
    /// The custom cardpack to check against.
    #[prost(string, tag = "2")]
    pub custom_cardpack: ::prost::alloc::string::String,
}
/// Response message for CheckDoesUserLikeCustomCardpack.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CheckDoesUserLikeCustomCardpackResponse {
    /// Whether the user has liked the custom cardpack.
    #[prost(bool, tag = "1")]
    pub is_liked: bool,
}
#[doc = r" Generated client implementations."]
pub mod cardpack_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = " Enables users to create cardpacks containing black and white cards."]
    #[doc = ""]
    #[doc = " Resources:"]
    #[doc = " - DefaultCardpack: Top-level resource, contain sets of cards that can be used in games"]
    #[doc = " - DefaultBlackCard: Belong to a DefaultCardpack and act like a black card from Cards Against Humanity"]
    #[doc = " - DefaultWhiteCard: Belong to a DefaultCardpack and act like a white card from Cards Against Humanity"]
    #[doc = " - CustomCardpack: Belong to a user, contain sets of cards that can be used in games"]
    #[doc = " - CustomBlackCard: Belong to a CustomCardpack and act like a black card from Cards Against Humanity"]
    #[doc = " - CustomWhiteCard: Belong to a CustomCardpack and act like a white card from Cards Against Humanity"]
    #[doc = ""]
    #[doc = " ( -- See resource proto documentation for more information. -- )"]
    pub struct CardpackServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl CardpackServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> CardpackServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        #[doc = " Creates a CustomCardpack."]
        pub async fn create_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CreateCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a CustomCardpack."]
        pub async fn get_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::GetCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/GetCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists CustomCardpacks."]
        pub async fn list_custom_cardpacks(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListCustomCardpacksResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListCustomCardpacks",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a CustomCardpack."]
        pub async fn update_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UpdateCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Soft deletes a CustomCardpack. Deleted CustomCardpacks can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomCardpacks cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        pub async fn delete_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::DeleteCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/DeleteCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a CustomBlackCard."]
        pub async fn create_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CreateCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a CustomWhiteCard."]
        pub async fn create_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CreateCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists CustomBlackCards."]
        pub async fn list_custom_black_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomBlackCardsRequest>,
        ) -> Result<tonic::Response<super::ListCustomBlackCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListCustomBlackCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists CustomWhiteCards."]
        pub async fn list_custom_white_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::ListCustomWhiteCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListCustomWhiteCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a CustomBlackCard."]
        pub async fn update_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UpdateCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a CustomWhiteCard."]
        pub async fn update_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UpdateCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Soft deletes a CustomBlackCard. Deleted CustomBlackCards can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomBlackCards cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        pub async fn delete_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::DeleteCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/DeleteCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Soft deletes a CustomWhiteCard. Deleted CustomWhiteCards can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomWhiteCards cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        pub async fn delete_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::DeleteCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/DeleteCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a batch of CustomBlackCards."]
        #[doc = " This operation is atomic. Partial writes are not supported."]
        pub async fn batch_create_custom_black_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::BatchCreateCustomBlackCardsRequest>,
        ) -> Result<tonic::Response<super::BatchCreateCustomBlackCardsResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/BatchCreateCustomBlackCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Creates a batch of CustomWhiteCards."]
        #[doc = " This operation is atomic. Partial writes are not supported."]
        pub async fn batch_create_custom_white_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::BatchCreateCustomWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::BatchCreateCustomWhiteCardsResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/BatchCreateCustomWhiteCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a DefaultCardpack."]
        pub async fn get_default_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::GetDefaultCardpackRequest>,
        ) -> Result<tonic::Response<super::DefaultCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/GetDefaultCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists DefaultCardpacks."]
        pub async fn list_default_cardpacks(
            &mut self,
            request: impl tonic::IntoRequest<super::ListDefaultCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListDefaultCardpacksResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListDefaultCardpacks",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists DefaultBlackCards."]
        pub async fn list_default_black_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListDefaultBlackCardsRequest>,
        ) -> Result<tonic::Response<super::ListDefaultBlackCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListDefaultBlackCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Lists DefaultWhiteCards."]
        pub async fn list_default_white_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::ListDefaultWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::ListDefaultWhiteCardsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListDefaultWhiteCards",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Undeletes a CustomCardpack, returning it to its previous state."]
        #[doc = " Deleted CustomCardpacks can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomCardpacks, use"]
        #[doc = " the ListCustomCardpacks rpc with `show_deleted` set to true."]
        pub async fn undelete_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::UndeleteCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UndeleteCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Undeletes a CustomBlackCard, returning it to its previous state."]
        #[doc = " Deleted CustomBlackCards can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomBlackCards, use"]
        #[doc = " the ListCustomBlackCards rpc with `show_deleted` set to true."]
        pub async fn undelete_custom_black_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UndeleteCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UndeleteCustomBlackCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Undeletes a CustomWhiteCard, returning it to its previous state."]
        #[doc = " Deleted CustomWhiteCards can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomWhiteCards, use"]
        #[doc = " the ListCustomWhiteCards rpc with `show_deleted` set to true."]
        pub async fn undelete_custom_white_card(
            &mut self,
            request: impl tonic::IntoRequest<super::UndeleteCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UndeleteCustomWhiteCard",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn list_favorited_custom_cardpacks(
            &mut self,
            request: impl tonic::IntoRequest<super::ListFavoritedCustomCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListFavoritedCustomCardpacksResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/ListFavoritedCustomCardpacks",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Adds CustomCardpack to a User's favorites list."]
        #[doc = " To view favorited custom cardpacks, use the ListFavoritedCustomCardpacks rpc."]
        pub async fn like_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::LikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<()>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/LikeCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Removes CustomCardpack from a User's favorites list."]
        #[doc = " To view favorited custom cardpacks, use the ListFavoritedCustomCardpacks rpc."]
        pub async fn unlike_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::UnlikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<()>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/UnlikeCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns whether a User has added a CustomCardpack to their favorites."]
        pub async fn check_does_user_like_custom_cardpack(
            &mut self,
            request: impl tonic::IntoRequest<super::CheckDoesUserLikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CheckDoesUserLikeCustomCardpackResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.CardpackService/CheckDoesUserLikeCustomCardpack",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for CardpackServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for CardpackServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "CardpackServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod cardpack_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with CardpackServiceServer."]
    #[async_trait]
    pub trait CardpackService: Send + Sync + 'static {
        #[doc = " Creates a CustomCardpack."]
        async fn create_custom_cardpack(
            &self,
            request: tonic::Request<super::CreateCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status>;
        #[doc = " Returns a CustomCardpack."]
        async fn get_custom_cardpack(
            &self,
            request: tonic::Request<super::GetCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status>;
        #[doc = " Lists CustomCardpacks."]
        async fn list_custom_cardpacks(
            &self,
            request: tonic::Request<super::ListCustomCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListCustomCardpacksResponse>, tonic::Status>;
        #[doc = " Updates a CustomCardpack."]
        async fn update_custom_cardpack(
            &self,
            request: tonic::Request<super::UpdateCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status>;
        #[doc = " Soft deletes a CustomCardpack. Deleted CustomCardpacks can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomCardpacks cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        async fn delete_custom_cardpack(
            &self,
            request: tonic::Request<super::DeleteCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status>;
        #[doc = " Creates a CustomBlackCard."]
        async fn create_custom_black_card(
            &self,
            request: tonic::Request<super::CreateCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status>;
        #[doc = " Creates a CustomWhiteCard."]
        async fn create_custom_white_card(
            &self,
            request: tonic::Request<super::CreateCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status>;
        #[doc = " Lists CustomBlackCards."]
        async fn list_custom_black_cards(
            &self,
            request: tonic::Request<super::ListCustomBlackCardsRequest>,
        ) -> Result<tonic::Response<super::ListCustomBlackCardsResponse>, tonic::Status>;
        #[doc = " Lists CustomWhiteCards."]
        async fn list_custom_white_cards(
            &self,
            request: tonic::Request<super::ListCustomWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::ListCustomWhiteCardsResponse>, tonic::Status>;
        #[doc = " Updates a CustomBlackCard."]
        async fn update_custom_black_card(
            &self,
            request: tonic::Request<super::UpdateCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status>;
        #[doc = " Updates a CustomWhiteCard."]
        async fn update_custom_white_card(
            &self,
            request: tonic::Request<super::UpdateCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status>;
        #[doc = " Soft deletes a CustomBlackCard. Deleted CustomBlackCards can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomBlackCards cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        async fn delete_custom_black_card(
            &self,
            request: tonic::Request<super::DeleteCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status>;
        #[doc = " Soft deletes a CustomWhiteCard. Deleted CustomWhiteCards can be undeleted"]
        #[doc = " for 30 days before they are permanently removed."]
        #[doc = " Deleted CustomWhiteCards cannot be modified, they can only be"]
        #[doc = " viewed and undeleted."]
        async fn delete_custom_white_card(
            &self,
            request: tonic::Request<super::DeleteCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status>;
        #[doc = " Creates a batch of CustomBlackCards."]
        #[doc = " This operation is atomic. Partial writes are not supported."]
        async fn batch_create_custom_black_cards(
            &self,
            request: tonic::Request<super::BatchCreateCustomBlackCardsRequest>,
        ) -> Result<tonic::Response<super::BatchCreateCustomBlackCardsResponse>, tonic::Status>;
        #[doc = " Creates a batch of CustomWhiteCards."]
        #[doc = " This operation is atomic. Partial writes are not supported."]
        async fn batch_create_custom_white_cards(
            &self,
            request: tonic::Request<super::BatchCreateCustomWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::BatchCreateCustomWhiteCardsResponse>, tonic::Status>;
        #[doc = " Returns a DefaultCardpack."]
        async fn get_default_cardpack(
            &self,
            request: tonic::Request<super::GetDefaultCardpackRequest>,
        ) -> Result<tonic::Response<super::DefaultCardpack>, tonic::Status>;
        #[doc = " Lists DefaultCardpacks."]
        async fn list_default_cardpacks(
            &self,
            request: tonic::Request<super::ListDefaultCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListDefaultCardpacksResponse>, tonic::Status>;
        #[doc = " Lists DefaultBlackCards."]
        async fn list_default_black_cards(
            &self,
            request: tonic::Request<super::ListDefaultBlackCardsRequest>,
        ) -> Result<tonic::Response<super::ListDefaultBlackCardsResponse>, tonic::Status>;
        #[doc = " Lists DefaultWhiteCards."]
        async fn list_default_white_cards(
            &self,
            request: tonic::Request<super::ListDefaultWhiteCardsRequest>,
        ) -> Result<tonic::Response<super::ListDefaultWhiteCardsResponse>, tonic::Status>;
        #[doc = " Undeletes a CustomCardpack, returning it to its previous state."]
        #[doc = " Deleted CustomCardpacks can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomCardpacks, use"]
        #[doc = " the ListCustomCardpacks rpc with `show_deleted` set to true."]
        async fn undelete_custom_cardpack(
            &self,
            request: tonic::Request<super::UndeleteCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CustomCardpack>, tonic::Status>;
        #[doc = " Undeletes a CustomBlackCard, returning it to its previous state."]
        #[doc = " Deleted CustomBlackCards can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomBlackCards, use"]
        #[doc = " the ListCustomBlackCards rpc with `show_deleted` set to true."]
        async fn undelete_custom_black_card(
            &self,
            request: tonic::Request<super::UndeleteCustomBlackCardRequest>,
        ) -> Result<tonic::Response<super::CustomBlackCard>, tonic::Status>;
        #[doc = " Undeletes a CustomWhiteCard, returning it to its previous state."]
        #[doc = " Deleted CustomWhiteCards can be undeleted for 30 days before they"]
        #[doc = " are permanently removed. To view deleted CustomWhiteCards, use"]
        #[doc = " the ListCustomWhiteCards rpc with `show_deleted` set to true."]
        async fn undelete_custom_white_card(
            &self,
            request: tonic::Request<super::UndeleteCustomWhiteCardRequest>,
        ) -> Result<tonic::Response<super::CustomWhiteCard>, tonic::Status>;
        async fn list_favorited_custom_cardpacks(
            &self,
            request: tonic::Request<super::ListFavoritedCustomCardpacksRequest>,
        ) -> Result<tonic::Response<super::ListFavoritedCustomCardpacksResponse>, tonic::Status>;
        #[doc = " Adds CustomCardpack to a User's favorites list."]
        #[doc = " To view favorited custom cardpacks, use the ListFavoritedCustomCardpacks rpc."]
        async fn like_custom_cardpack(
            &self,
            request: tonic::Request<super::LikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<()>, tonic::Status>;
        #[doc = " Removes CustomCardpack from a User's favorites list."]
        #[doc = " To view favorited custom cardpacks, use the ListFavoritedCustomCardpacks rpc."]
        async fn unlike_custom_cardpack(
            &self,
            request: tonic::Request<super::UnlikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<()>, tonic::Status>;
        #[doc = " Returns whether a User has added a CustomCardpack to their favorites."]
        async fn check_does_user_like_custom_cardpack(
            &self,
            request: tonic::Request<super::CheckDoesUserLikeCustomCardpackRequest>,
        ) -> Result<tonic::Response<super::CheckDoesUserLikeCustomCardpackResponse>, tonic::Status>;
    }
    #[doc = " Enables users to create cardpacks containing black and white cards."]
    #[doc = ""]
    #[doc = " Resources:"]
    #[doc = " - DefaultCardpack: Top-level resource, contain sets of cards that can be used in games"]
    #[doc = " - DefaultBlackCard: Belong to a DefaultCardpack and act like a black card from Cards Against Humanity"]
    #[doc = " - DefaultWhiteCard: Belong to a DefaultCardpack and act like a white card from Cards Against Humanity"]
    #[doc = " - CustomCardpack: Belong to a user, contain sets of cards that can be used in games"]
    #[doc = " - CustomBlackCard: Belong to a CustomCardpack and act like a black card from Cards Against Humanity"]
    #[doc = " - CustomWhiteCard: Belong to a CustomCardpack and act like a white card from Cards Against Humanity"]
    #[doc = ""]
    #[doc = " ( -- See resource proto documentation for more information. -- )"]
    #[derive(Debug)]
    pub struct CardpackServiceServer<T: CardpackService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: CardpackService> CardpackServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for CardpackServiceServer<T>
    where
        T: CardpackService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/crusty_cards_api.CardpackService/CreateCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct CreateCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::CreateCustomCardpackRequest>
                        for CreateCustomCardpackSvc<T>
                    {
                        type Response = super::CustomCardpack;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CreateCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).create_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/GetCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct GetCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::GetCustomCardpackRequest>
                        for GetCustomCardpackSvc<T>
                    {
                        type Response = super::CustomCardpack;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListCustomCardpacks" => {
                    #[allow(non_camel_case_types)]
                    struct ListCustomCardpacksSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListCustomCardpacksRequest>
                        for ListCustomCardpacksSvc<T>
                    {
                        type Response = super::ListCustomCardpacksResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListCustomCardpacksRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).list_custom_cardpacks(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListCustomCardpacksSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UpdateCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UpdateCustomCardpackRequest>
                        for UpdateCustomCardpackSvc<T>
                    {
                        type Response = super::CustomCardpack;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpdateCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).update_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/DeleteCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::DeleteCustomCardpackRequest>
                        for DeleteCustomCardpackSvc<T>
                    {
                        type Response = super::CustomCardpack;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::DeleteCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).delete_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = DeleteCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/CreateCustomBlackCard" => {
                    #[allow(non_camel_case_types)]
                    struct CreateCustomBlackCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::CreateCustomBlackCardRequest>
                        for CreateCustomBlackCardSvc<T>
                    {
                        type Response = super::CustomBlackCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CreateCustomBlackCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).create_custom_black_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateCustomBlackCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/CreateCustomWhiteCard" => {
                    #[allow(non_camel_case_types)]
                    struct CreateCustomWhiteCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::CreateCustomWhiteCardRequest>
                        for CreateCustomWhiteCardSvc<T>
                    {
                        type Response = super::CustomWhiteCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CreateCustomWhiteCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).create_custom_white_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateCustomWhiteCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListCustomBlackCards" => {
                    #[allow(non_camel_case_types)]
                    struct ListCustomBlackCardsSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListCustomBlackCardsRequest>
                        for ListCustomBlackCardsSvc<T>
                    {
                        type Response = super::ListCustomBlackCardsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListCustomBlackCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).list_custom_black_cards(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListCustomBlackCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListCustomWhiteCards" => {
                    #[allow(non_camel_case_types)]
                    struct ListCustomWhiteCardsSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListCustomWhiteCardsRequest>
                        for ListCustomWhiteCardsSvc<T>
                    {
                        type Response = super::ListCustomWhiteCardsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListCustomWhiteCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).list_custom_white_cards(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListCustomWhiteCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UpdateCustomBlackCard" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateCustomBlackCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UpdateCustomBlackCardRequest>
                        for UpdateCustomBlackCardSvc<T>
                    {
                        type Response = super::CustomBlackCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpdateCustomBlackCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).update_custom_black_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateCustomBlackCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UpdateCustomWhiteCard" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateCustomWhiteCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UpdateCustomWhiteCardRequest>
                        for UpdateCustomWhiteCardSvc<T>
                    {
                        type Response = super::CustomWhiteCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpdateCustomWhiteCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).update_custom_white_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateCustomWhiteCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/DeleteCustomBlackCard" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteCustomBlackCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::DeleteCustomBlackCardRequest>
                        for DeleteCustomBlackCardSvc<T>
                    {
                        type Response = super::CustomBlackCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::DeleteCustomBlackCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).delete_custom_black_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = DeleteCustomBlackCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/DeleteCustomWhiteCard" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteCustomWhiteCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::DeleteCustomWhiteCardRequest>
                        for DeleteCustomWhiteCardSvc<T>
                    {
                        type Response = super::CustomWhiteCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::DeleteCustomWhiteCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).delete_custom_white_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = DeleteCustomWhiteCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/BatchCreateCustomBlackCards" => {
                    #[allow(non_camel_case_types)]
                    struct BatchCreateCustomBlackCardsSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::BatchCreateCustomBlackCardsRequest>
                        for BatchCreateCustomBlackCardsSvc<T>
                    {
                        type Response = super::BatchCreateCustomBlackCardsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::BatchCreateCustomBlackCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move {
                                (*inner).batch_create_custom_black_cards(request).await
                            };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = BatchCreateCustomBlackCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/BatchCreateCustomWhiteCards" => {
                    #[allow(non_camel_case_types)]
                    struct BatchCreateCustomWhiteCardsSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::BatchCreateCustomWhiteCardsRequest>
                        for BatchCreateCustomWhiteCardsSvc<T>
                    {
                        type Response = super::BatchCreateCustomWhiteCardsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::BatchCreateCustomWhiteCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move {
                                (*inner).batch_create_custom_white_cards(request).await
                            };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = BatchCreateCustomWhiteCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/GetDefaultCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct GetDefaultCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::GetDefaultCardpackRequest>
                        for GetDefaultCardpackSvc<T>
                    {
                        type Response = super::DefaultCardpack;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetDefaultCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_default_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetDefaultCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListDefaultCardpacks" => {
                    #[allow(non_camel_case_types)]
                    struct ListDefaultCardpacksSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListDefaultCardpacksRequest>
                        for ListDefaultCardpacksSvc<T>
                    {
                        type Response = super::ListDefaultCardpacksResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListDefaultCardpacksRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).list_default_cardpacks(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListDefaultCardpacksSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListDefaultBlackCards" => {
                    #[allow(non_camel_case_types)]
                    struct ListDefaultBlackCardsSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListDefaultBlackCardsRequest>
                        for ListDefaultBlackCardsSvc<T>
                    {
                        type Response = super::ListDefaultBlackCardsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListDefaultBlackCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).list_default_black_cards(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListDefaultBlackCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListDefaultWhiteCards" => {
                    #[allow(non_camel_case_types)]
                    struct ListDefaultWhiteCardsSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListDefaultWhiteCardsRequest>
                        for ListDefaultWhiteCardsSvc<T>
                    {
                        type Response = super::ListDefaultWhiteCardsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListDefaultWhiteCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).list_default_white_cards(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListDefaultWhiteCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UndeleteCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct UndeleteCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UndeleteCustomCardpackRequest>
                        for UndeleteCustomCardpackSvc<T>
                    {
                        type Response = super::CustomCardpack;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UndeleteCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).undelete_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UndeleteCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UndeleteCustomBlackCard" => {
                    #[allow(non_camel_case_types)]
                    struct UndeleteCustomBlackCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UndeleteCustomBlackCardRequest>
                        for UndeleteCustomBlackCardSvc<T>
                    {
                        type Response = super::CustomBlackCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UndeleteCustomBlackCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).undelete_custom_black_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UndeleteCustomBlackCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UndeleteCustomWhiteCard" => {
                    #[allow(non_camel_case_types)]
                    struct UndeleteCustomWhiteCardSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UndeleteCustomWhiteCardRequest>
                        for UndeleteCustomWhiteCardSvc<T>
                    {
                        type Response = super::CustomWhiteCard;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UndeleteCustomWhiteCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).undelete_custom_white_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UndeleteCustomWhiteCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/ListFavoritedCustomCardpacks" => {
                    #[allow(non_camel_case_types)]
                    struct ListFavoritedCustomCardpacksSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::ListFavoritedCustomCardpacksRequest>
                        for ListFavoritedCustomCardpacksSvc<T>
                    {
                        type Response = super::ListFavoritedCustomCardpacksResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListFavoritedCustomCardpacksRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move {
                                (*inner).list_favorited_custom_cardpacks(request).await
                            };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListFavoritedCustomCardpacksSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/LikeCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct LikeCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::LikeCustomCardpackRequest>
                        for LikeCustomCardpackSvc<T>
                    {
                        type Response = ();
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::LikeCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).like_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = LikeCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/UnlikeCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct UnlikeCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::UnlikeCustomCardpackRequest>
                        for UnlikeCustomCardpackSvc<T>
                    {
                        type Response = ();
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UnlikeCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).unlike_custom_cardpack(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UnlikeCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.CardpackService/CheckDoesUserLikeCustomCardpack" => {
                    #[allow(non_camel_case_types)]
                    struct CheckDoesUserLikeCustomCardpackSvc<T: CardpackService>(pub Arc<T>);
                    impl<T: CardpackService>
                        tonic::server::UnaryService<super::CheckDoesUserLikeCustomCardpackRequest>
                        for CheckDoesUserLikeCustomCardpackSvc<T>
                    {
                        type Response = super::CheckDoesUserLikeCustomCardpackResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CheckDoesUserLikeCustomCardpackRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move {
                                (*inner).check_does_user_like_custom_cardpack(request).await
                            };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CheckDoesUserLikeCustomCardpackSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: CardpackService> Clone for CardpackServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: CardpackService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: CardpackService> tonic::transport::NamedService for CardpackServiceServer<T> {
        const NAME: &'static str = "crusty_cards_api.CardpackService";
    }
}
/// Returns all games that match the search criteria.
/// Has no pagination, simply returns the entire list.
/// Results are always ordered by create_time.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SearchGamesRequest {
    /// Filters games by name, i.e. games must contain the
    /// exact text of the query in their name to match.
    #[prost(string, tag = "1")]
    pub query: ::prost::alloc::string::String,
    /// The minimum number of slots games must have available. Used to filter out games that are full
    /// or nearly full. Value cannot be negative.
    #[prost(int32, tag = "2")]
    pub min_available_player_slots: i32,
    #[prost(enumeration = "search_games_request::GameStageFilter", tag = "3")]
    pub game_stage_filter: i32,
}
/// Nested message and enum types in `SearchGamesRequest`.
pub mod search_games_request {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum GameStageFilter {
        /// Default value. This value is unused.
        Unspecified = 0,
        /// Bypass game stage filter.
        FilterNone = 1,
        /// Filter all non-running games.
        FilterStopped = 2,
        /// Filter all running games.
        FilterRunning = 3,
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SearchGamesResponse {
    #[prost(message, repeated, tag = "1")]
    pub games: ::prost::alloc::vec::Vec<GameInfo>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateGameRequest {
    /// The name of the user creating the game.
    /// This user will be the owner once the game is created.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// Game configuration options.
    #[prost(message, optional, tag = "2")]
    pub game_config: ::core::option::Option<GameConfig>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StartGameRequest {
    /// The name of the user trying to start the game.
    /// Must be the name of someone who owns the game that they are in.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StopGameRequest {
    /// The name of the user trying to stop the game.
    /// Must be the name of someone who owns the game that they are in.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinGameRequest {
    /// The user who is joining a game.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The unique identifier of the game to join.
    #[prost(string, tag = "2")]
    pub game_id: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveGameRequest {
    /// The user who is leaving a game.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct KickUserRequest {
    /// The user who is kicking another user.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The user being kicked.
    #[prost(string, tag = "2")]
    pub troll_user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BanUserRequest {
    /// The user who is banning another user.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The user being banned.
    #[prost(string, tag = "2")]
    pub troll_user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UnbanUserRequest {
    /// The user who is unbanning another user.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The user being unbanned.
    #[prost(string, tag = "2")]
    pub troll_user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PlayCardsRequest {
    /// The user who is playing cards for the current round.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The cards to play. All cards must be in the user's hand or the request will fail.
    /// For each card in this list, if the `custom_white_card` property is present
    /// then only `custom_white_card.name` is required and all other fields are ignored.
    /// If the `default_white_card` property is present
    /// then only `default_white_card.name` is required and all other fields are ignored.
    /// If the `blank_white_card` property is present then `blank_white_card.id` and `blank_white_card.open_text` are required.
    /// The `blank_white_card.open_text` field should contain user-specified open text from a blank white card.
    /// The `blank_white_card` property is only allowed for games that have OPEN_TEXT behavior for blank white cards.
    #[prost(message, repeated, tag = "2")]
    pub cards: ::prost::alloc::vec::Vec<PlayableWhiteCard>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UnplayCardsRequest {
    /// The user who is unplaying their cards for the current round.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct VoteCardRequest {
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The 1-based index of which set of cards to vote for as the judge.
    #[prost(int32, tag = "2")]
    pub choice: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct VoteStartNextRoundRequest {
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddArtificialPlayerRequest {
    /// The user who is adding an artificial player.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The display name of the new artificial player.
    /// A name is chosen automatically if it isn't provided here.
    #[prost(string, tag = "2")]
    pub display_name: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveArtificialPlayerRequest {
    /// The user who is removing an artificial player.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The artificial player to remove.
    /// If unspecified, removes the first artificial player, prioritizing queued players.
    #[prost(string, tag = "2")]
    pub artificial_player_id: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateChatMessageRequest {
    /// The name of the user creating the chat message.
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
    /// The chat message to create.
    #[prost(message, optional, tag = "2")]
    pub chat_message: ::core::option::Option<ChatMessage>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetGameViewRequest {
    #[prost(string, tag = "1")]
    pub user_name: ::prost::alloc::string::String,
}
/// Used to search among all cards that are in a game - including the dicard pile and all players' hands.
/// The purpose of this rpc is for games that use DUPLICATE_TEXT blank white cards.
/// Returned items are sorted alphabetically.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListWhiteCardTextsRequest {
    /// The game to search within.
    #[prost(string, tag = "1")]
    pub game_id: ::prost::alloc::string::String,
    /// The maximum page size.
    /// If unspecified, at most 50 items will be returned.
    /// The maximum value is 1000; values above 1000 will be coerced to 1000.
    #[prost(int32, tag = "2")]
    pub page_size: i32,
    /// Page token returned from a previous rpc.
    #[prost(string, tag = "3")]
    pub page_token: ::prost::alloc::string::String,
    /// Filter cards by their text. Right now this does not have any
    /// special functionality described in https://google.aip.dev/160.
    /// It is strictly an exact text filter.
    #[prost(string, tag = "4")]
    pub filter: ::prost::alloc::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListWhiteCardTextsResponse {
    /// The text of each card.
    #[prost(string, repeated, tag = "1")]
    pub card_texts: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
    /// Opaque string. Can be passed to a subsequent
    /// request to retrieve the next page of items.
    #[prost(string, tag = "2")]
    pub next_page_token: ::prost::alloc::string::String,
    /// The total number of items in the list specified in the request.
    #[prost(int64, tag = "3")]
    pub total_size: i64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ChatMessage {
    /// The user who sent the message.
    #[prost(message, optional, tag = "1")]
    pub user: ::core::option::Option<User>,
    /// The contents of the message. Cannot be blank.
    #[prost(string, tag = "2")]
    pub text: ::prost::alloc::string::String,
    /// When the message was created.
    #[prost(message, optional, tag = "3")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ArtificialUser {
    /// A server-generated uuid.
    #[prost(string, tag = "1")]
    pub id: ::prost::alloc::string::String,
    /// The displayed name of the artificial user.
    /// Since each artificial user has a unique id,
    /// the name does not have to be unique.
    #[prost(string, tag = "2")]
    pub display_name: ::prost::alloc::string::String,
}
/// Represents a real user or an artificial player in a game.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Player {
    /// The number of rounds this player has won.
    #[prost(int32, tag = "3")]
    pub score: i32,
    /// When the player was added to the game. For real users this is
    /// when the JoinGame RPC is called. For artificial players
    /// this is when the AddArtificialPlayer RPC is called.
    #[prost(message, optional, tag = "4")]
    pub join_time: ::core::option::Option<::prost_types::Timestamp>,
    #[prost(oneof = "player::Identifier", tags = "1, 2")]
    pub identifier: ::core::option::Option<player::Identifier>,
}
/// Nested message and enum types in `Player`.
pub mod player {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Identifier {
        #[prost(message, tag = "1")]
        User(super::User),
        #[prost(message, tag = "2")]
        ArtificialUser(super::ArtificialUser),
    }
}
/// Can be one of several different types of black cards.
/// This proto is used during gameplay to represent any type of black card.
/// The black card equivalent of the PlayableWhiteCard message.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BlackCardInRound {
    #[prost(oneof = "black_card_in_round::Card", tags = "1, 2")]
    pub card: ::core::option::Option<black_card_in_round::Card>,
}
/// Nested message and enum types in `BlackCardInRound`.
pub mod black_card_in_round {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Card {
        #[prost(message, tag = "1")]
        CustomBlackCard(super::CustomBlackCard),
        #[prost(message, tag = "2")]
        DefaultBlackCard(super::DefaultBlackCard),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BlankWhiteCard {
    /// A server-generated uuid.
    #[prost(string, tag = "1")]
    pub id: ::prost::alloc::string::String,
    /// Text entered by the user (limited to 100 characters).
    /// Always empty for cards in a player's hand and
    /// always non-empty for cards that have been played.
    /// Required when playing a blank white card using the PlayCards rpc.
    #[prost(string, tag = "2")]
    pub open_text: ::prost::alloc::string::String,
}
/// Can be one of several different types of white cards.
/// This proto is used during gameplay to represent any type of white card.
/// The white card equivalent of the BlackCardInRound message.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PlayableWhiteCard {
    #[prost(oneof = "playable_white_card::Card", tags = "1, 2, 3")]
    pub card: ::core::option::Option<playable_white_card::Card>,
}
/// Nested message and enum types in `PlayableWhiteCard`.
pub mod playable_white_card {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Card {
        #[prost(message, tag = "1")]
        CustomWhiteCard(super::CustomWhiteCard),
        #[prost(message, tag = "2")]
        BlankWhiteCard(super::BlankWhiteCard),
        #[prost(message, tag = "3")]
        DefaultWhiteCard(super::DefaultWhiteCard),
    }
}
/// Contains cards played by
/// a single user for one round.
/// See the description for GameView.white_played
/// for details on how this is used
/// and when certain fields are filled.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct WhiteCardsPlayed {
    /// The player who played the cards.
    #[prost(message, optional, tag = "1")]
    pub player: ::core::option::Option<Player>,
    /// The text of the cards played by the user.
    /// We're including only the card text to hide
    /// whether it includes any blank white cards.
    /// Since artificial players never draw blank
    /// white cards, including full card protos
    /// could give away that an answer belongs to
    /// a real user if it contained blank white
    /// cards. By including only the text, even
    /// users with direct access to this api are
    /// unable to tell where it came from.
    #[prost(string, repeated, tag = "2")]
    pub card_texts: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PastRound {
    /// The black card that was used for the round.
    #[prost(message, optional, tag = "1")]
    pub black_card: ::core::option::Option<BlackCardInRound>,
    /// The white cards that were played for this round. Each entry includes the player who played the cards.
    #[prost(message, repeated, tag = "2")]
    pub white_played: ::prost::alloc::vec::Vec<WhiteCardsPlayed>,
    /// The user who judged the round.
    #[prost(message, optional, tag = "3")]
    pub judge: ::core::option::Option<User>,
    /// The player who won the round.
    #[prost(message, optional, tag = "4")]
    pub winner: ::core::option::Option<Player>,
}
/// Game list view used when users are browsing games.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GameInfo {
    /// A server-generated uuid.
    #[prost(string, tag = "1")]
    pub game_id: ::prost::alloc::string::String,
    /// Game configuration options.
    #[prost(message, optional, tag = "2")]
    pub config: ::core::option::Option<GameConfig>,
    /// The number of players currently in the game.
    /// This does not include artificial players.
    #[prost(int32, tag = "3")]
    pub player_count: i32,
    /// The current game owner.
    #[prost(message, optional, tag = "5")]
    pub owner: ::core::option::Option<User>,
    /// Whether the game is currently in progress.
    #[prost(bool, tag = "6")]
    pub is_running: bool,
    /// When the underlying game was created.
    #[prost(message, optional, tag = "7")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
    /// When the game was last active.
    /// This only accounts for game-related actions, not users joining
    /// or leaving the game or sending messages in the chat.
    #[prost(message, optional, tag = "8")]
    pub last_activity_time: ::core::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GameView {
    /// The unique id of the game.
    #[prost(string, tag = "1")]
    pub game_id: ::prost::alloc::string::String,
    /// The config that was used to create this game.
    #[prost(message, optional, tag = "2")]
    pub config: ::core::option::Option<GameConfig>,
    /// What stage the game is in. This determines what actions are available.
    #[prost(enumeration = "game_view::Stage", tag = "3")]
    pub stage: i32,
    /// The player's current hand.
    #[prost(message, repeated, tag = "4")]
    pub hand: ::prost::alloc::vec::Vec<PlayableWhiteCard>,
    /// All real and artificial players that are currently playing.
    /// Guaranteed to be ordered by join_time.
    #[prost(message, repeated, tag = "5")]
    pub players: ::prost::alloc::vec::Vec<Player>,
    /// All real and artificial players that joined while the
    /// game was running (any Stage other than NOT_RUNNING).
    /// Guaranteed to be ordered by join_time.
    /// These players will be automatically moved from queued_players to
    /// players when StopGame is called (or if every user in the game
    /// calls VoteStartNextRound).
    #[prost(message, repeated, tag = "6")]
    pub queued_players: ::prost::alloc::vec::Vec<Player>,
    /// All users that have been banned from the game.
    /// Always listed in the order they were banned.
    /// These users are unable to join the game.
    #[prost(message, repeated, tag = "7")]
    pub banned_users: ::prost::alloc::vec::Vec<User>,
    /// The current judge for the round.
    /// This might be empty if the game stage is NOT_RUNNING
    /// (if there is a previous judge to show from the past game
    /// then it will be filled, otherwise it will be empty).
    /// If the game is in any other stage then this is always filled.
    /// When the game stage is PLAY_PHASE or JUDGE_PHASE
    /// it represents the current judge, and the judge does not change
    /// when the stage is changed from JUDGE_PHASE to ROUND_END_PHASE.
    /// Instead, it changes when the game stage changes from ROUND_END_PHASE to PLAY_PHASE.
    /// Whenever a game is started, a new random judge is selected.
    #[prost(message, optional, tag = "8")]
    pub judge: ::core::option::Option<User>,
    /// The current owner of the game.
    /// The game creator is the initial
    /// owner, but if the current owner
    /// leaves then ownership is transferred
    /// to the next user who has been
    /// in the game the longest.
    #[prost(message, optional, tag = "9")]
    pub owner: ::core::option::Option<User>,
    /// The white cards played this round.
    /// Data contained is dependent on the game stage.
    /// ----------------------------------------------
    /// When the game stage is PLAY_PHASE, each repeated value
    /// represents who has played this round. The 'user' field
    /// is filled for each value so that players know who has
    /// played, but the 'cards' field is empty so cards remain
    /// anonymous until after the judge phase. Values are
    /// in no particular order.
    ///
    /// When the game stage is JUDGE_PHASE, this will contain
    /// one value for each player that has played this round.
    /// For each repeated value, the 'cards' property will be
    /// filled but the 'user' property will be left empty and
    /// the order of values randomized so users don't know who
    /// submitted each card.
    ///
    /// When the game stage is ROUND_END_PHASE, this will
    /// contain one value for every player that played in
    /// the current round. For each repeated value, both
    /// 'user' and 'cards' properties will be filled.
    /// Values are in the same order as they were during
    /// the judge phase.
    ///
    /// When the game stage is NOT_RUNNING, behavior is the same as ROUND_END_PHASE
    /// except that the repeated field could contain no values. This happens if
    /// the game was just created and there is no past round to show cards for.
    /// Otherwise this will contain cards from the previous game
    /// that just ended (or was stopped).
    #[prost(message, repeated, tag = "10")]
    pub white_played: ::prost::alloc::vec::Vec<WhiteCardsPlayed>,
    /// The black card that's active for the current round.
    /// This is empty if stage is NOT_RUNNING, otherwise
    /// this will contain the current card.
    #[prost(message, optional, tag = "11")]
    pub current_black_card: ::core::option::Option<BlackCardInRound>,
    /// Contains the winner of the most recent game.
    /// This is only filled when the stage is NOT_RUNNING
    /// and there is a winner to display. There is not
    /// always a winner to display, such as if the game
    /// was just created or if it was stopped
    /// before someone won.
    #[prost(message, optional, tag = "12")]
    pub winner: ::core::option::Option<Player>,
    /// Chat messages sent by players in the game.
    /// Contains, at most, the 100 most recent messages.
    #[prost(message, repeated, tag = "13")]
    pub chat_messages: ::prost::alloc::vec::Vec<ChatMessage>,
    /// A list of previous rounds from the current game.
    /// This is cleared whenever the game is started/restarted.
    #[prost(message, repeated, tag = "14")]
    pub past_rounds: ::prost::alloc::vec::Vec<PastRound>,
    /// When the game was created.
    #[prost(message, optional, tag = "15")]
    pub create_time: ::core::option::Option<::prost_types::Timestamp>,
    /// When the game was last active.
    /// This only accounts for game-related actions, not users joining
    /// or leaving the game or sending messages in the chat.
    #[prost(message, optional, tag = "16")]
    pub last_activity_time: ::core::option::Option<::prost_types::Timestamp>,
}
/// Nested message and enum types in `GameView`.
pub mod game_view {
    /// Represents what stage this game is currently in, which
    /// determines what actions are allowed for certain players.
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum Stage {
        /// Default value. This value is unused.
        Unspecified = 0,
        /// Game is not running.
        NotRunning = 1,
        /// Players (other than the judge) can play cards for the current round.
        PlayPhase = 2,
        /// The current judge can pick their favorite card(s).
        /// Cards from the play phase are visible but anonymous.
        JudgePhase = 3,
        /// Player scores are updated and players can view who played each card.
        RoundEndPhase = 4,
    }
}
#[doc = r" Generated client implementations."]
pub mod game_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    pub struct GameServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl GameServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> GameServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        pub async fn search_games(
            &mut self,
            request: impl tonic::IntoRequest<super::SearchGamesRequest>,
        ) -> Result<tonic::Response<super::SearchGamesResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/SearchGames");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create_game(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/CreateGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn start_game(
            &mut self,
            request: impl tonic::IntoRequest<super::StartGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/StartGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn stop_game(
            &mut self,
            request: impl tonic::IntoRequest<super::StopGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/StopGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn join_game(
            &mut self,
            request: impl tonic::IntoRequest<super::JoinGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/JoinGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn leave_game(
            &mut self,
            request: impl tonic::IntoRequest<super::LeaveGameRequest>,
        ) -> Result<tonic::Response<()>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/LeaveGame");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn kick_user(
            &mut self,
            request: impl tonic::IntoRequest<super::KickUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/KickUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn ban_user(
            &mut self,
            request: impl tonic::IntoRequest<super::BanUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/BanUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn unban_user(
            &mut self,
            request: impl tonic::IntoRequest<super::UnbanUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/UnbanUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn play_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::PlayCardsRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/PlayCards");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn unplay_cards(
            &mut self,
            request: impl tonic::IntoRequest<super::UnplayCardsRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/UnplayCards");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn vote_card(
            &mut self,
            request: impl tonic::IntoRequest<super::VoteCardRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/VoteCard");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn vote_start_next_round(
            &mut self,
            request: impl tonic::IntoRequest<super::VoteStartNextRoundRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/VoteStartNextRound",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn add_artificial_player(
            &mut self,
            request: impl tonic::IntoRequest<super::AddArtificialPlayerRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/AddArtificialPlayer",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn remove_artificial_player(
            &mut self,
            request: impl tonic::IntoRequest<super::RemoveArtificialPlayerRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/RemoveArtificialPlayer",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create_chat_message(
            &mut self,
            request: impl tonic::IntoRequest<super::CreateChatMessageRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/CreateChatMessage",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_game_view(
            &mut self,
            request: impl tonic::IntoRequest<super::GetGameViewRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.GameService/GetGameView");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn list_white_card_texts(
            &mut self,
            request: impl tonic::IntoRequest<super::ListWhiteCardTextsRequest>,
        ) -> Result<tonic::Response<super::ListWhiteCardTextsResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.GameService/ListWhiteCardTexts",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for GameServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for GameServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "GameServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod game_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with GameServiceServer."]
    #[async_trait]
    pub trait GameService: Send + Sync + 'static {
        async fn search_games(
            &self,
            request: tonic::Request<super::SearchGamesRequest>,
        ) -> Result<tonic::Response<super::SearchGamesResponse>, tonic::Status>;
        async fn create_game(
            &self,
            request: tonic::Request<super::CreateGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn start_game(
            &self,
            request: tonic::Request<super::StartGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn stop_game(
            &self,
            request: tonic::Request<super::StopGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn join_game(
            &self,
            request: tonic::Request<super::JoinGameRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn leave_game(
            &self,
            request: tonic::Request<super::LeaveGameRequest>,
        ) -> Result<tonic::Response<()>, tonic::Status>;
        async fn kick_user(
            &self,
            request: tonic::Request<super::KickUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn ban_user(
            &self,
            request: tonic::Request<super::BanUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn unban_user(
            &self,
            request: tonic::Request<super::UnbanUserRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn play_cards(
            &self,
            request: tonic::Request<super::PlayCardsRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn unplay_cards(
            &self,
            request: tonic::Request<super::UnplayCardsRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn vote_card(
            &self,
            request: tonic::Request<super::VoteCardRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn vote_start_next_round(
            &self,
            request: tonic::Request<super::VoteStartNextRoundRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn add_artificial_player(
            &self,
            request: tonic::Request<super::AddArtificialPlayerRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn remove_artificial_player(
            &self,
            request: tonic::Request<super::RemoveArtificialPlayerRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn create_chat_message(
            &self,
            request: tonic::Request<super::CreateChatMessageRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn get_game_view(
            &self,
            request: tonic::Request<super::GetGameViewRequest>,
        ) -> Result<tonic::Response<super::GameView>, tonic::Status>;
        async fn list_white_card_texts(
            &self,
            request: tonic::Request<super::ListWhiteCardTextsRequest>,
        ) -> Result<tonic::Response<super::ListWhiteCardTextsResponse>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct GameServiceServer<T: GameService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: GameService> GameServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for GameServiceServer<T>
    where
        T: GameService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/crusty_cards_api.GameService/SearchGames" => {
                    #[allow(non_camel_case_types)]
                    struct SearchGamesSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::SearchGamesRequest> for SearchGamesSvc<T> {
                        type Response = super::SearchGamesResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::SearchGamesRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).search_games(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = SearchGamesSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/CreateGame" => {
                    #[allow(non_camel_case_types)]
                    struct CreateGameSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::CreateGameRequest> for CreateGameSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CreateGameRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).create_game(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateGameSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/StartGame" => {
                    #[allow(non_camel_case_types)]
                    struct StartGameSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::StartGameRequest> for StartGameSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::StartGameRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).start_game(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = StartGameSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/StopGame" => {
                    #[allow(non_camel_case_types)]
                    struct StopGameSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::StopGameRequest> for StopGameSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::StopGameRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).stop_game(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = StopGameSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/JoinGame" => {
                    #[allow(non_camel_case_types)]
                    struct JoinGameSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::JoinGameRequest> for JoinGameSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::JoinGameRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).join_game(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = JoinGameSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/LeaveGame" => {
                    #[allow(non_camel_case_types)]
                    struct LeaveGameSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::LeaveGameRequest> for LeaveGameSvc<T> {
                        type Response = ();
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::LeaveGameRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).leave_game(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = LeaveGameSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/KickUser" => {
                    #[allow(non_camel_case_types)]
                    struct KickUserSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::KickUserRequest> for KickUserSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::KickUserRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).kick_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = KickUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/BanUser" => {
                    #[allow(non_camel_case_types)]
                    struct BanUserSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::BanUserRequest> for BanUserSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::BanUserRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).ban_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = BanUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/UnbanUser" => {
                    #[allow(non_camel_case_types)]
                    struct UnbanUserSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::UnbanUserRequest> for UnbanUserSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UnbanUserRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).unban_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UnbanUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/PlayCards" => {
                    #[allow(non_camel_case_types)]
                    struct PlayCardsSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::PlayCardsRequest> for PlayCardsSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::PlayCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).play_cards(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = PlayCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/UnplayCards" => {
                    #[allow(non_camel_case_types)]
                    struct UnplayCardsSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::UnplayCardsRequest> for UnplayCardsSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UnplayCardsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).unplay_cards(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UnplayCardsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/VoteCard" => {
                    #[allow(non_camel_case_types)]
                    struct VoteCardSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::VoteCardRequest> for VoteCardSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::VoteCardRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).vote_card(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = VoteCardSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/VoteStartNextRound" => {
                    #[allow(non_camel_case_types)]
                    struct VoteStartNextRoundSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService>
                        tonic::server::UnaryService<super::VoteStartNextRoundRequest>
                        for VoteStartNextRoundSvc<T>
                    {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::VoteStartNextRoundRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).vote_start_next_round(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = VoteStartNextRoundSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/AddArtificialPlayer" => {
                    #[allow(non_camel_case_types)]
                    struct AddArtificialPlayerSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService>
                        tonic::server::UnaryService<super::AddArtificialPlayerRequest>
                        for AddArtificialPlayerSvc<T>
                    {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::AddArtificialPlayerRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).add_artificial_player(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AddArtificialPlayerSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/RemoveArtificialPlayer" => {
                    #[allow(non_camel_case_types)]
                    struct RemoveArtificialPlayerSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService>
                        tonic::server::UnaryService<super::RemoveArtificialPlayerRequest>
                        for RemoveArtificialPlayerSvc<T>
                    {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::RemoveArtificialPlayerRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).remove_artificial_player(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = RemoveArtificialPlayerSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/CreateChatMessage" => {
                    #[allow(non_camel_case_types)]
                    struct CreateChatMessageSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService>
                        tonic::server::UnaryService<super::CreateChatMessageRequest>
                        for CreateChatMessageSvc<T>
                    {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CreateChatMessageRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).create_chat_message(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = CreateChatMessageSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/GetGameView" => {
                    #[allow(non_camel_case_types)]
                    struct GetGameViewSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService> tonic::server::UnaryService<super::GetGameViewRequest> for GetGameViewSvc<T> {
                        type Response = super::GameView;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetGameViewRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_game_view(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetGameViewSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.GameService/ListWhiteCardTexts" => {
                    #[allow(non_camel_case_types)]
                    struct ListWhiteCardTextsSvc<T: GameService>(pub Arc<T>);
                    impl<T: GameService>
                        tonic::server::UnaryService<super::ListWhiteCardTextsRequest>
                        for ListWhiteCardTextsSvc<T>
                    {
                        type Response = super::ListWhiteCardTextsResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ListWhiteCardTextsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).list_white_card_texts(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = ListWhiteCardTextsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: GameService> Clone for GameServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: GameService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: GameService> tonic::transport::NamedService for GameServiceServer<T> {
        const NAME: &'static str = "crusty_cards_api.GameService";
    }
}
/// Used to identify a user through their oauth account.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct OAuthCredentials {
    /// The user's oauth provider. Currently Google is the only provider that's used.
    #[prost(string, tag = "1")]
    pub oauth_provider: ::prost::alloc::string::String,
    /// The oauth id of the user from the given provider.
    #[prost(string, tag = "2")]
    pub oauth_id: ::prost::alloc::string::String,
}
/// Request message for GetUser.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetUserRequest {
    /// The name of the user to retrieve.
    /// Format: users/{user}
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for UpdateUser.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateUserRequest {
    /// The user to update.
    #[prost(message, optional, tag = "1")]
    pub user: ::core::option::Option<User>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::core::option::Option<::prost_types::FieldMask>,
}
/// Request message for GetUserSettings.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetUserSettingsRequest {
    /// The name of the user settings to retrieve.
    /// Format: users/{user}/settings
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for UpdateUserSettings.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateUserSettingsRequest {
    /// The user settings to update.
    #[prost(message, optional, tag = "1")]
    pub user_settings: ::core::option::Option<UserSettings>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::core::option::Option<::prost_types::FieldMask>,
}
/// Request message for GetUserProfileImage.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetUserProfileImageRequest {
    /// The name of the profile image to retrieve.
    /// Format: users/{user}/profileImage
    #[prost(string, tag = "1")]
    pub name: ::prost::alloc::string::String,
}
/// Request message for UpdateUserProfileImage.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateUserProfileImageRequest {
    /// The profile image to update.
    #[prost(message, optional, tag = "1")]
    pub user_profile_image: ::core::option::Option<UserProfileImage>,
    /// The list of fields to be updated.
    #[prost(message, optional, tag = "2")]
    pub update_mask: ::core::option::Option<::prost_types::FieldMask>,
}
/// Request message for GetOrCreateUser.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetOrCreateUserRequest {
    /// The user's oauth credentials.
    #[prost(message, optional, tag = "1")]
    pub oauth_credentials: ::core::option::Option<OAuthCredentials>,
    /// Used to create the user if it does not exist yet.
    #[prost(message, optional, tag = "2")]
    pub user: ::core::option::Option<User>,
}
/// Request message for UserSearch.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserSearchRequest {
    /// The search query to suggest results from.
    #[prost(string, tag = "1")]
    pub query: ::prost::alloc::string::String,
}
/// Response message for UserSearch.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserSearchResponse {
    /// Search results.
    #[prost(message, repeated, tag = "1")]
    pub users: ::prost::alloc::vec::Vec<User>,
}
/// Request message for AutocompleteUserSearch.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AutocompleteUserSearchRequest {
    /// The search query to suggest results from.
    #[prost(string, tag = "1")]
    pub query: ::prost::alloc::string::String,
}
/// Response message for AutocompleteUserSearch.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AutocompleteUserSearchResponse {
    /// List of autocomplete suggestions.
    /// Each entry is guaranteed to contain
    /// the original query.
    #[prost(string, repeated, tag = "1")]
    pub autocomplete_entries: ::prost::alloc::vec::Vec<::prost::alloc::string::String>,
}
#[doc = r" Generated client implementations."]
pub mod user_service_client {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = " Enables creating and retrieval of users and user data."]
    #[doc = ""]
    #[doc = " Resources:"]
    #[doc = " - User: Pretty much what you'd expect."]
    #[doc = " - UserSettings: A user's account settings and preferences."]
    #[doc = " - UserProfileImage: A user's profile picture."]
    #[doc = ""]
    #[doc = " ( -- See resource proto documentation for more information. -- )"]
    pub struct UserServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl UserServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> UserServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + HttpBody + Send + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as HttpBody>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = tonic::client::Grpc::with_interceptor(inner, interceptor);
            Self { inner }
        }
        #[doc = " Returns a User."]
        pub async fn get_user(
            &mut self,
            request: impl tonic::IntoRequest<super::GetUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.UserService/GetUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a User."]
        pub async fn update_user(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.UserService/UpdateUser");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a User's settings."]
        pub async fn get_user_settings(
            &mut self,
            request: impl tonic::IntoRequest<super::GetUserSettingsRequest>,
        ) -> Result<tonic::Response<super::UserSettings>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/GetUserSettings",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a User's settings."]
        pub async fn update_user_settings(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateUserSettingsRequest>,
        ) -> Result<tonic::Response<super::UserSettings>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/UpdateUserSettings",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Returns a User's profile image."]
        pub async fn get_user_profile_image(
            &mut self,
            request: impl tonic::IntoRequest<super::GetUserProfileImageRequest>,
        ) -> Result<tonic::Response<super::UserProfileImage>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/GetUserProfileImage",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Updates a User's profile image."]
        pub async fn update_user_profile_image(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateUserProfileImageRequest>,
        ) -> Result<tonic::Response<super::UserProfileImage>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/UpdateUserProfileImage",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Gets a user, or creates one if it does not exist."]
        #[doc = " This method of creating users may seem strange. The reason we have a 'get-or-create' instead of"]
        #[doc = " a simpler 'create' rpc is because of the way that oauth login works. When someone logs in using"]
        #[doc = " oauth, the oauth provider doesn't know whether they already have an account here (i.e. a User"]
        #[doc = " exists that matches their oauth account). So at login time, we need to treat every user the same"]
        #[doc = " regardless of whether they've signed in before since we can't know that ahead of time. With the"]
        #[doc = " get-or-create pattern we provide all data we might need if a new User needs to be created, and"]
        #[doc = " ignore it if the User already exists."]
        pub async fn get_or_create_user(
            &mut self,
            request: impl tonic::IntoRequest<super::GetOrCreateUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/GetOrCreateUser",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Fetches multiple Users based off search query."]
        pub async fn user_search(
            &mut self,
            request: impl tonic::IntoRequest<super::UserSearchRequest>,
        ) -> Result<tonic::Response<super::UserSearchResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/crusty_cards_api.UserService/UserSearch");
            self.inner.unary(request.into_request(), path, codec).await
        }
        #[doc = " Fetches search suggestions that are used to provide autocomplete functionality."]
        pub async fn autocomplete_user_search(
            &mut self,
            request: impl tonic::IntoRequest<super::AutocompleteUserSearchRequest>,
        ) -> Result<tonic::Response<super::AutocompleteUserSearchResponse>, tonic::Status> {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/crusty_cards_api.UserService/AutocompleteUserSearch",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
    impl<T: Clone> Clone for UserServiceClient<T> {
        fn clone(&self) -> Self {
            Self {
                inner: self.inner.clone(),
            }
        }
    }
    impl<T> std::fmt::Debug for UserServiceClient<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "UserServiceClient {{ ... }}")
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod user_service_server {
    #![allow(unused_variables, dead_code, missing_docs)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with UserServiceServer."]
    #[async_trait]
    pub trait UserService: Send + Sync + 'static {
        #[doc = " Returns a User."]
        async fn get_user(
            &self,
            request: tonic::Request<super::GetUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status>;
        #[doc = " Updates a User."]
        async fn update_user(
            &self,
            request: tonic::Request<super::UpdateUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status>;
        #[doc = " Returns a User's settings."]
        async fn get_user_settings(
            &self,
            request: tonic::Request<super::GetUserSettingsRequest>,
        ) -> Result<tonic::Response<super::UserSettings>, tonic::Status>;
        #[doc = " Updates a User's settings."]
        async fn update_user_settings(
            &self,
            request: tonic::Request<super::UpdateUserSettingsRequest>,
        ) -> Result<tonic::Response<super::UserSettings>, tonic::Status>;
        #[doc = " Returns a User's profile image."]
        async fn get_user_profile_image(
            &self,
            request: tonic::Request<super::GetUserProfileImageRequest>,
        ) -> Result<tonic::Response<super::UserProfileImage>, tonic::Status>;
        #[doc = " Updates a User's profile image."]
        async fn update_user_profile_image(
            &self,
            request: tonic::Request<super::UpdateUserProfileImageRequest>,
        ) -> Result<tonic::Response<super::UserProfileImage>, tonic::Status>;
        #[doc = " Gets a user, or creates one if it does not exist."]
        #[doc = " This method of creating users may seem strange. The reason we have a 'get-or-create' instead of"]
        #[doc = " a simpler 'create' rpc is because of the way that oauth login works. When someone logs in using"]
        #[doc = " oauth, the oauth provider doesn't know whether they already have an account here (i.e. a User"]
        #[doc = " exists that matches their oauth account). So at login time, we need to treat every user the same"]
        #[doc = " regardless of whether they've signed in before since we can't know that ahead of time. With the"]
        #[doc = " get-or-create pattern we provide all data we might need if a new User needs to be created, and"]
        #[doc = " ignore it if the User already exists."]
        async fn get_or_create_user(
            &self,
            request: tonic::Request<super::GetOrCreateUserRequest>,
        ) -> Result<tonic::Response<super::User>, tonic::Status>;
        #[doc = " Fetches multiple Users based off search query."]
        async fn user_search(
            &self,
            request: tonic::Request<super::UserSearchRequest>,
        ) -> Result<tonic::Response<super::UserSearchResponse>, tonic::Status>;
        #[doc = " Fetches search suggestions that are used to provide autocomplete functionality."]
        async fn autocomplete_user_search(
            &self,
            request: tonic::Request<super::AutocompleteUserSearchRequest>,
        ) -> Result<tonic::Response<super::AutocompleteUserSearchResponse>, tonic::Status>;
    }
    #[doc = " Enables creating and retrieval of users and user data."]
    #[doc = ""]
    #[doc = " Resources:"]
    #[doc = " - User: Pretty much what you'd expect."]
    #[doc = " - UserSettings: A user's account settings and preferences."]
    #[doc = " - UserProfileImage: A user's profile picture."]
    #[doc = ""]
    #[doc = " ( -- See resource proto documentation for more information. -- )"]
    #[derive(Debug)]
    pub struct UserServiceServer<T: UserService> {
        inner: _Inner<T>,
    }
    struct _Inner<T>(Arc<T>, Option<tonic::Interceptor>);
    impl<T: UserService> UserServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, None);
            Self { inner }
        }
        pub fn with_interceptor(inner: T, interceptor: impl Into<tonic::Interceptor>) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner, Some(interceptor.into()));
            Self { inner }
        }
    }
    impl<T, B> Service<http::Request<B>> for UserServiceServer<T>
    where
        T: UserService,
        B: HttpBody + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/crusty_cards_api.UserService/GetUser" => {
                    #[allow(non_camel_case_types)]
                    struct GetUserSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService> tonic::server::UnaryService<super::GetUserRequest> for GetUserSvc<T> {
                        type Response = super::User;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetUserRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/UpdateUser" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateUserSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService> tonic::server::UnaryService<super::UpdateUserRequest> for UpdateUserSvc<T> {
                        type Response = super::User;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpdateUserRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).update_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/GetUserSettings" => {
                    #[allow(non_camel_case_types)]
                    struct GetUserSettingsSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService> tonic::server::UnaryService<super::GetUserSettingsRequest>
                        for GetUserSettingsSvc<T>
                    {
                        type Response = super::UserSettings;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetUserSettingsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_user_settings(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetUserSettingsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/UpdateUserSettings" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateUserSettingsSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService>
                        tonic::server::UnaryService<super::UpdateUserSettingsRequest>
                        for UpdateUserSettingsSvc<T>
                    {
                        type Response = super::UserSettings;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpdateUserSettingsRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).update_user_settings(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateUserSettingsSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/GetUserProfileImage" => {
                    #[allow(non_camel_case_types)]
                    struct GetUserProfileImageSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService>
                        tonic::server::UnaryService<super::GetUserProfileImageRequest>
                        for GetUserProfileImageSvc<T>
                    {
                        type Response = super::UserProfileImage;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetUserProfileImageRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_user_profile_image(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetUserProfileImageSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/UpdateUserProfileImage" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateUserProfileImageSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService>
                        tonic::server::UnaryService<super::UpdateUserProfileImageRequest>
                        for UpdateUserProfileImageSvc<T>
                    {
                        type Response = super::UserProfileImage;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UpdateUserProfileImageRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).update_user_profile_image(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UpdateUserProfileImageSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/GetOrCreateUser" => {
                    #[allow(non_camel_case_types)]
                    struct GetOrCreateUserSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService> tonic::server::UnaryService<super::GetOrCreateUserRequest>
                        for GetOrCreateUserSvc<T>
                    {
                        type Response = super::User;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::GetOrCreateUserRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_or_create_user(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = GetOrCreateUserSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/UserSearch" => {
                    #[allow(non_camel_case_types)]
                    struct UserSearchSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService> tonic::server::UnaryService<super::UserSearchRequest> for UserSearchSvc<T> {
                        type Response = super::UserSearchResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::UserSearchRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).user_search(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = UserSearchSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/crusty_cards_api.UserService/AutocompleteUserSearch" => {
                    #[allow(non_camel_case_types)]
                    struct AutocompleteUserSearchSvc<T: UserService>(pub Arc<T>);
                    impl<T: UserService>
                        tonic::server::UnaryService<super::AutocompleteUserSearchRequest>
                        for AutocompleteUserSearchSvc<T>
                    {
                        type Response = super::AutocompleteUserSearchResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::AutocompleteUserSearchRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut =
                                async move { (*inner).autocomplete_user_search(request).await };
                            Box::pin(fut)
                        }
                    }
                    let inner = self.inner.clone();
                    let fut = async move {
                        let interceptor = inner.1.clone();
                        let inner = inner.0;
                        let method = AutocompleteUserSearchSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = if let Some(interceptor) = interceptor {
                            tonic::server::Grpc::with_interceptor(codec, interceptor)
                        } else {
                            tonic::server::Grpc::new(codec)
                        };
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(tonic::body::BoxBody::empty())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: UserService> Clone for UserServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self { inner }
        }
    }
    impl<T: UserService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone(), self.1.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: UserService> tonic::transport::NamedService for UserServiceServer<T> {
        const NAME: &'static str = "crusty_cards_api.UserService";
    }
}
