"""
@generated
cargo-raze generated Bazel file.

DO NOT EDIT! Replaced on runs of cargo-raze
"""

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")  # buildifier: disable=load
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")  # buildifier: disable=load
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")  # buildifier: disable=load

def cards_mono_fetch_remote_crates():
    """This function defines a collection of repos and should be called in a WORKSPACE file"""
    maybe(
        http_archive,
        name = "cards_mono__addr2line__0_14_1",
        url = "https://crates.io/api/v1/crates/addr2line/0.14.1/download",
        type = "tar.gz",
        sha256 = "a55f82cfe485775d02112886f4169bde0c5894d75e79ead7eafe7e40a25e45f7",
        strip_prefix = "addr2line-0.14.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.addr2line-0.14.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__adler__1_0_2",
        url = "https://crates.io/api/v1/crates/adler/1.0.2/download",
        type = "tar.gz",
        sha256 = "f26201604c87b1e01bd3d98f8d5d9a8fcbb815e8cedb41ffccbeb4bf593a35fe",
        strip_prefix = "adler-1.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.adler-1.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__aho_corasick__0_7_15",
        url = "https://crates.io/api/v1/crates/aho-corasick/0.7.15/download",
        type = "tar.gz",
        sha256 = "7404febffaa47dac81aa44dba71523c9d069b1bdc50a77db41195149e17f68e5",
        strip_prefix = "aho-corasick-0.7.15",
        build_file = Label("//rust_shared/raze/remote:BUILD.aho-corasick-0.7.15.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__amq_protocol__6_0_3",
        url = "https://crates.io/api/v1/crates/amq-protocol/6.0.3/download",
        type = "tar.gz",
        sha256 = "3c222de30b345b19470d9091414c5bda68cd845c44e8dde2f874373e0b247a54",
        strip_prefix = "amq-protocol-6.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.amq-protocol-6.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__amq_protocol_tcp__6_0_3",
        url = "https://crates.io/api/v1/crates/amq-protocol-tcp/6.0.3/download",
        type = "tar.gz",
        sha256 = "bee3c947a2c5b40b6e506453eeff96998bb2caa2ea616ed028ba1bafbc2b16ce",
        strip_prefix = "amq-protocol-tcp-6.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.amq-protocol-tcp-6.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__amq_protocol_types__6_0_3",
        url = "https://crates.io/api/v1/crates/amq-protocol-types/6.0.3/download",
        type = "tar.gz",
        sha256 = "d198cde4bde0eadf1a946e4f0d113ff471bf8abe4ef728181d7320461da8a3e4",
        strip_prefix = "amq-protocol-types-6.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.amq-protocol-types-6.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__amq_protocol_uri__6_0_3",
        url = "https://crates.io/api/v1/crates/amq-protocol-uri/6.0.3/download",
        type = "tar.gz",
        sha256 = "9f843ece1f9a66cebbccf4162b5505e96e93598d3b2678ac56ad19d60642bd45",
        strip_prefix = "amq-protocol-uri-6.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.amq-protocol-uri-6.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__anyhow__1_0_40",
        url = "https://crates.io/api/v1/crates/anyhow/1.0.40/download",
        type = "tar.gz",
        sha256 = "28b2cd92db5cbd74e8e5028f7e27dd7aa3090e89e4f2a197cc7c8dfb69c7063b",
        strip_prefix = "anyhow-1.0.40",
        build_file = Label("//rust_shared/raze/remote:BUILD.anyhow-1.0.40.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__arrayvec__0_5_2",
        url = "https://crates.io/api/v1/crates/arrayvec/0.5.2/download",
        type = "tar.gz",
        sha256 = "23b62fc65de8e4e7f52534fb52b0f3ed04746ae267519eef2a83941e8085068b",
        strip_prefix = "arrayvec-0.5.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.arrayvec-0.5.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_attributes__1_1_2",
        url = "https://crates.io/api/v1/crates/async-attributes/1.1.2/download",
        type = "tar.gz",
        sha256 = "a3203e79f4dd9bdda415ed03cf14dae5a2bf775c683a00f94e9cd1faf0f596e5",
        strip_prefix = "async-attributes-1.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-attributes-1.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_channel__1_6_1",
        url = "https://crates.io/api/v1/crates/async-channel/1.6.1/download",
        type = "tar.gz",
        sha256 = "2114d64672151c0c5eaa5e131ec84a74f06e1e559830dabba01ca30605d66319",
        strip_prefix = "async-channel-1.6.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-channel-1.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_executor__1_4_0",
        url = "https://crates.io/api/v1/crates/async-executor/1.4.0/download",
        type = "tar.gz",
        sha256 = "eb877970c7b440ead138f6321a3b5395d6061183af779340b65e20c0fede9146",
        strip_prefix = "async-executor-1.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-executor-1.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_global_executor__2_0_2",
        url = "https://crates.io/api/v1/crates/async-global-executor/2.0.2/download",
        type = "tar.gz",
        sha256 = "9586ec52317f36de58453159d48351bc244bc24ced3effc1fce22f3d48664af6",
        strip_prefix = "async-global-executor-2.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-global-executor-2.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_io__1_3_1",
        url = "https://crates.io/api/v1/crates/async-io/1.3.1/download",
        type = "tar.gz",
        sha256 = "9315f8f07556761c3e48fec2e6b276004acf426e6dc068b2c2251854d65ee0fd",
        strip_prefix = "async-io-1.3.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-io-1.3.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_lock__2_3_0",
        url = "https://crates.io/api/v1/crates/async-lock/2.3.0/download",
        type = "tar.gz",
        sha256 = "1996609732bde4a9988bc42125f55f2af5f3c36370e27c778d5191a4a1b63bfb",
        strip_prefix = "async-lock-2.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-lock-2.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_mutex__1_4_0",
        url = "https://crates.io/api/v1/crates/async-mutex/1.4.0/download",
        type = "tar.gz",
        sha256 = "479db852db25d9dbf6204e6cb6253698f175c15726470f78af0d918e99d6156e",
        strip_prefix = "async-mutex-1.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-mutex-1.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_std__1_9_0",
        url = "https://crates.io/api/v1/crates/async-std/1.9.0/download",
        type = "tar.gz",
        sha256 = "d9f06685bad74e0570f5213741bea82158279a4103d988e57bfada11ad230341",
        strip_prefix = "async-std-1.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-std-1.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_stream__0_3_0",
        url = "https://crates.io/api/v1/crates/async-stream/0.3.0/download",
        type = "tar.gz",
        sha256 = "3670df70cbc01729f901f94c887814b3c68db038aad1329a418bae178bc5295c",
        strip_prefix = "async-stream-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-stream-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_stream_impl__0_3_0",
        url = "https://crates.io/api/v1/crates/async-stream-impl/0.3.0/download",
        type = "tar.gz",
        sha256 = "a3548b8efc9f8e8a5a0a2808c5bd8451a9031b9e5b879a79590304ae928b0a70",
        strip_prefix = "async-stream-impl-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-stream-impl-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_task__4_0_3",
        url = "https://crates.io/api/v1/crates/async-task/4.0.3/download",
        type = "tar.gz",
        sha256 = "e91831deabf0d6d7ec49552e489aed63b7456a7a3c46cff62adad428110b0af0",
        strip_prefix = "async-task-4.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-task-4.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__async_trait__0_1_49",
        url = "https://crates.io/api/v1/crates/async-trait/0.1.49/download",
        type = "tar.gz",
        sha256 = "589652ce7ccb335d1e7ecb3be145425702b290dbcb7029bbeaae263fc1d87b48",
        strip_prefix = "async-trait-0.1.49",
        build_file = Label("//rust_shared/raze/remote:BUILD.async-trait-0.1.49.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__atomic_waker__1_0_0",
        url = "https://crates.io/api/v1/crates/atomic-waker/1.0.0/download",
        type = "tar.gz",
        sha256 = "065374052e7df7ee4047b1160cca5e1467a12351a40b3da123c870ba0b8eda2a",
        strip_prefix = "atomic-waker-1.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.atomic-waker-1.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__autocfg__0_1_7",
        url = "https://crates.io/api/v1/crates/autocfg/0.1.7/download",
        type = "tar.gz",
        sha256 = "1d49d90015b3c36167a20fe2810c5cd875ad504b39cff3d4eae7977e6b7c1cb2",
        strip_prefix = "autocfg-0.1.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.autocfg-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__autocfg__1_0_1",
        url = "https://crates.io/api/v1/crates/autocfg/1.0.1/download",
        type = "tar.gz",
        sha256 = "cdb031dd78e28731d87d56cc8ffef4a8f36ca26c38fe2de700543e627f8a464a",
        strip_prefix = "autocfg-1.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.autocfg-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__backtrace__0_3_56",
        url = "https://crates.io/api/v1/crates/backtrace/0.3.56/download",
        type = "tar.gz",
        sha256 = "9d117600f438b1707d4e4ae15d3595657288f8235a0eb593e80ecc98ab34e1bc",
        strip_prefix = "backtrace-0.3.56",
        build_file = Label("//rust_shared/raze/remote:BUILD.backtrace-0.3.56.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__base64__0_10_1",
        url = "https://crates.io/api/v1/crates/base64/0.10.1/download",
        type = "tar.gz",
        sha256 = "0b25d992356d2eb0ed82172f5248873db5560c4721f564b13cb5193bda5e668e",
        strip_prefix = "base64-0.10.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.base64-0.10.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__base64__0_11_0",
        url = "https://crates.io/api/v1/crates/base64/0.11.0/download",
        type = "tar.gz",
        sha256 = "b41b7ea54a0c9d92199de89e20e58d49f02f8e699814ef3fdf266f6f748d15c7",
        strip_prefix = "base64-0.11.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.base64-0.11.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__base64__0_12_3",
        url = "https://crates.io/api/v1/crates/base64/0.12.3/download",
        type = "tar.gz",
        sha256 = "3441f0f7b02788e948e47f457ca01f1d7e6d92c693bc132c22b087d3141c03ff",
        strip_prefix = "base64-0.12.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.base64-0.12.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__base64__0_13_0",
        url = "https://crates.io/api/v1/crates/base64/0.13.0/download",
        type = "tar.gz",
        sha256 = "904dfeac50f3cdaba28fc6f57fdcddb75f49ed61346676a78c4ffe55877802fd",
        strip_prefix = "base64-0.13.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.base64-0.13.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bincode__1_3_3",
        url = "https://crates.io/api/v1/crates/bincode/1.3.3/download",
        type = "tar.gz",
        sha256 = "b1f45e9417d87227c7a56d22e471c6206462cba514c7590c09aff4cf6d1ddcad",
        strip_prefix = "bincode-1.3.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.bincode-1.3.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bitflags__1_2_1",
        url = "https://crates.io/api/v1/crates/bitflags/1.2.1/download",
        type = "tar.gz",
        sha256 = "cf1de2fe8c75bc145a2f577add951f8134889b4795d47466a54a5c846d691693",
        strip_prefix = "bitflags-1.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.bitflags-1.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bitvec__0_19_5",
        url = "https://crates.io/api/v1/crates/bitvec/0.19.5/download",
        type = "tar.gz",
        sha256 = "8942c8d352ae1838c9dda0b0ca2ab657696ef2232a20147cf1b30ae1a9cb4321",
        strip_prefix = "bitvec-0.19.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.bitvec-0.19.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__block_buffer__0_7_3",
        url = "https://crates.io/api/v1/crates/block-buffer/0.7.3/download",
        type = "tar.gz",
        sha256 = "c0940dc441f31689269e10ac70eb1002a3a1d3ad1390e030043662eb7fe4688b",
        strip_prefix = "block-buffer-0.7.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.block-buffer-0.7.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__block_buffer__0_9_0",
        url = "https://crates.io/api/v1/crates/block-buffer/0.9.0/download",
        type = "tar.gz",
        sha256 = "4152116fd6e9dadb291ae18fc1ec3575ed6d84c29642d97890f4b4a3417297e4",
        strip_prefix = "block-buffer-0.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.block-buffer-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__block_cipher__0_8_0",
        url = "https://crates.io/api/v1/crates/block-cipher/0.8.0/download",
        type = "tar.gz",
        sha256 = "f337a3e6da609650eb74e02bc9fac7b735049f7623ab12f2e4c719316fcc7e80",
        strip_prefix = "block-cipher-0.8.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.block-cipher-0.8.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__block_modes__0_6_1",
        url = "https://crates.io/api/v1/crates/block-modes/0.6.1/download",
        type = "tar.gz",
        sha256 = "0c9b14fd8a4739e6548d4b6018696cf991dcf8c6effd9ef9eb33b29b8a650972",
        strip_prefix = "block-modes-0.6.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.block-modes-0.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__block_padding__0_1_5",
        url = "https://crates.io/api/v1/crates/block-padding/0.1.5/download",
        type = "tar.gz",
        sha256 = "fa79dedbb091f449f1f39e53edf88d5dbe95f895dae6135a8d7b881fb5af73f5",
        strip_prefix = "block-padding-0.1.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.block-padding-0.1.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__block_padding__0_2_1",
        url = "https://crates.io/api/v1/crates/block-padding/0.2.1/download",
        type = "tar.gz",
        sha256 = "8d696c370c750c948ada61c69a0ee2cbbb9c50b1019ddb86d9317157a99c2cae",
        strip_prefix = "block-padding-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.block-padding-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__blocking__1_0_2",
        url = "https://crates.io/api/v1/crates/blocking/1.0.2/download",
        type = "tar.gz",
        sha256 = "c5e170dbede1f740736619b776d7251cb1b9095c435c34d8ca9f57fcd2f335e9",
        strip_prefix = "blocking-1.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.blocking-1.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bson__1_2_2",
        url = "https://crates.io/api/v1/crates/bson/1.2.2/download",
        type = "tar.gz",
        sha256 = "38b6553abdb9d2d8f262f0b5bccf807321d5b7d1a12796bcede8e1f150e85f2e",
        strip_prefix = "bson-1.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.bson-1.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bumpalo__3_6_1",
        url = "https://crates.io/api/v1/crates/bumpalo/3.6.1/download",
        type = "tar.gz",
        sha256 = "63396b8a4b9de3f4fdfb320ab6080762242f66a8ef174c49d8e19b674db4cdbe",
        strip_prefix = "bumpalo-3.6.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.bumpalo-3.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__byte_tools__0_3_1",
        url = "https://crates.io/api/v1/crates/byte-tools/0.3.1/download",
        type = "tar.gz",
        sha256 = "e3b5ca7a04898ad4bcd41c90c5285445ff5b791899bb1b0abdd2a2aa791211d7",
        strip_prefix = "byte-tools-0.3.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.byte-tools-0.3.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__byteorder__1_4_3",
        url = "https://crates.io/api/v1/crates/byteorder/1.4.3/download",
        type = "tar.gz",
        sha256 = "14c189c53d098945499cdfa7ecc63567cf3886b3332b312a5b4585d8d3a6a610",
        strip_prefix = "byteorder-1.4.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.byteorder-1.4.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bytes__0_4_12",
        url = "https://crates.io/api/v1/crates/bytes/0.4.12/download",
        type = "tar.gz",
        sha256 = "206fdffcfa2df7cbe15601ef46c813fce0965eb3286db6b56c583b814b51c81c",
        strip_prefix = "bytes-0.4.12",
        build_file = Label("//rust_shared/raze/remote:BUILD.bytes-0.4.12.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bytes__0_5_6",
        url = "https://crates.io/api/v1/crates/bytes/0.5.6/download",
        type = "tar.gz",
        sha256 = "0e4cec68f03f32e44924783795810fa50a7035d8c8ebe78580ad7e6c703fba38",
        strip_prefix = "bytes-0.5.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.bytes-0.5.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__bytes__1_0_1",
        url = "https://crates.io/api/v1/crates/bytes/1.0.1/download",
        type = "tar.gz",
        sha256 = "b700ce4376041dcd0a327fd0097c41095743c4c8af8887265942faf1100bd040",
        strip_prefix = "bytes-1.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.bytes-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cache_padded__1_1_1",
        url = "https://crates.io/api/v1/crates/cache-padded/1.1.1/download",
        type = "tar.gz",
        sha256 = "631ae5198c9be5e753e5cc215e1bd73c2b466a3565173db433f52bb9d3e66dba",
        strip_prefix = "cache-padded-1.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.cache-padded-1.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cc__1_0_67",
        url = "https://crates.io/api/v1/crates/cc/1.0.67/download",
        type = "tar.gz",
        sha256 = "e3c69b077ad434294d3ce9f1f6143a2a4b89a8a2d54ef813d85003a4fd1137fd",
        strip_prefix = "cc-1.0.67",
        build_file = Label("//rust_shared/raze/remote:BUILD.cc-1.0.67.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cfg_if__0_1_10",
        url = "https://crates.io/api/v1/crates/cfg-if/0.1.10/download",
        type = "tar.gz",
        sha256 = "4785bdd1c96b2a846b2bd7cc02e86b6b3dbf14e7e53446c4f54c92a361040822",
        strip_prefix = "cfg-if-0.1.10",
        build_file = Label("//rust_shared/raze/remote:BUILD.cfg-if-0.1.10.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cfg_if__1_0_0",
        url = "https://crates.io/api/v1/crates/cfg-if/1.0.0/download",
        type = "tar.gz",
        sha256 = "baf1de4339761588bc0619e3cbc0120ee582ebb74b53b4efbf79117bd2da40fd",
        strip_prefix = "cfg-if-1.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.cfg-if-1.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__chrono__0_4_19",
        url = "https://crates.io/api/v1/crates/chrono/0.4.19/download",
        type = "tar.gz",
        sha256 = "670ad68c9088c2a963aaa298cb369688cf3f9465ce5e2d4ca10e6e0098a1ce73",
        strip_prefix = "chrono-0.4.19",
        build_file = Label("//rust_shared/raze/remote:BUILD.chrono-0.4.19.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__clokwerk__0_3_4",
        url = "https://crates.io/api/v1/crates/clokwerk/0.3.4/download",
        type = "tar.gz",
        sha256 = "f9797a6d3acefa28d4cc62bf548b6ddc57b8ae51a43702d001cb46fba1dc48c1",
        strip_prefix = "clokwerk-0.3.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.clokwerk-0.3.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cloudabi__0_0_3",
        url = "https://crates.io/api/v1/crates/cloudabi/0.0.3/download",
        type = "tar.gz",
        sha256 = "ddfc5b9aa5d4507acaf872de71051dfd0e309860e88966e1051e462a077aac4f",
        strip_prefix = "cloudabi-0.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.cloudabi-0.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__concurrent_queue__1_2_2",
        url = "https://crates.io/api/v1/crates/concurrent-queue/1.2.2/download",
        type = "tar.gz",
        sha256 = "30ed07550be01594c6026cff2a1d7fe9c8f683caa798e12b68694ac9e88286a3",
        strip_prefix = "concurrent-queue-1.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.concurrent-queue-1.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cookie__0_12_0",
        url = "https://crates.io/api/v1/crates/cookie/0.12.0/download",
        type = "tar.gz",
        sha256 = "888604f00b3db336d2af898ec3c1d5d0ddf5e6d462220f2ededc33a87ac4bbd5",
        strip_prefix = "cookie-0.12.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.cookie-0.12.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cookie_factory__0_3_2",
        url = "https://crates.io/api/v1/crates/cookie-factory/0.3.2/download",
        type = "tar.gz",
        sha256 = "396de984970346b0d9e93d1415082923c679e5ae5c3ee3dcbd104f5610af126b",
        strip_prefix = "cookie-factory-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.cookie-factory-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cookie_store__0_7_0",
        url = "https://crates.io/api/v1/crates/cookie_store/0.7.0/download",
        type = "tar.gz",
        sha256 = "46750b3f362965f197996c4448e4a0935e791bf7d6631bfce9ee0af3d24c919c",
        strip_prefix = "cookie_store-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.cookie_store-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__core_foundation__0_9_1",
        url = "https://crates.io/api/v1/crates/core-foundation/0.9.1/download",
        type = "tar.gz",
        sha256 = "0a89e2ae426ea83155dccf10c0fa6b1463ef6d5fcb44cee0b224a408fa640a62",
        strip_prefix = "core-foundation-0.9.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.core-foundation-0.9.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__core_foundation_sys__0_8_2",
        url = "https://crates.io/api/v1/crates/core-foundation-sys/0.8.2/download",
        type = "tar.gz",
        sha256 = "ea221b5284a47e40033bf9b66f35f984ec0ea2931eb03505246cd27a963f981b",
        strip_prefix = "core-foundation-sys-0.8.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.core-foundation-sys-0.8.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__cpuid_bool__0_1_2",
        url = "https://crates.io/api/v1/crates/cpuid-bool/0.1.2/download",
        type = "tar.gz",
        sha256 = "8aebca1129a03dc6dc2b127edd729435bbc4a37e1d5f4d7513165089ceb02634",
        strip_prefix = "cpuid-bool-0.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.cpuid-bool-0.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crc32fast__1_2_1",
        url = "https://crates.io/api/v1/crates/crc32fast/1.2.1/download",
        type = "tar.gz",
        sha256 = "81156fece84ab6a9f2afdb109ce3ae577e42b1228441eded99bd77f627953b1a",
        strip_prefix = "crc32fast-1.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.crc32fast-1.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crossbeam_channel__0_5_1",
        url = "https://crates.io/api/v1/crates/crossbeam-channel/0.5.1/download",
        type = "tar.gz",
        sha256 = "06ed27e177f16d65f0f0c22a213e17c696ace5dd64b14258b52f9417ccb52db4",
        strip_prefix = "crossbeam-channel-0.5.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.crossbeam-channel-0.5.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crossbeam_deque__0_7_3",
        url = "https://crates.io/api/v1/crates/crossbeam-deque/0.7.3/download",
        type = "tar.gz",
        sha256 = "9f02af974daeee82218205558e51ec8768b48cf524bd01d550abe5573a608285",
        strip_prefix = "crossbeam-deque-0.7.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.crossbeam-deque-0.7.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crossbeam_epoch__0_8_2",
        url = "https://crates.io/api/v1/crates/crossbeam-epoch/0.8.2/download",
        type = "tar.gz",
        sha256 = "058ed274caafc1f60c4997b5fc07bf7dc7cca454af7c6e81edffe5f33f70dace",
        strip_prefix = "crossbeam-epoch-0.8.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.crossbeam-epoch-0.8.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crossbeam_queue__0_2_3",
        url = "https://crates.io/api/v1/crates/crossbeam-queue/0.2.3/download",
        type = "tar.gz",
        sha256 = "774ba60a54c213d409d5353bda12d49cd68d14e45036a285234c8d6f91f92570",
        strip_prefix = "crossbeam-queue-0.2.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.crossbeam-queue-0.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crossbeam_utils__0_7_2",
        url = "https://crates.io/api/v1/crates/crossbeam-utils/0.7.2/download",
        type = "tar.gz",
        sha256 = "c3c7c73a2d1e9fc0886a08b93e98eb643461230d5f1925e4036204d5f2e261a8",
        strip_prefix = "crossbeam-utils-0.7.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.crossbeam-utils-0.7.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crossbeam_utils__0_8_3",
        url = "https://crates.io/api/v1/crates/crossbeam-utils/0.8.3/download",
        type = "tar.gz",
        sha256 = "e7e9d99fa91428effe99c5c6d4634cdeba32b8cf784fc428a2a687f61a952c49",
        strip_prefix = "crossbeam-utils-0.8.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.crossbeam-utils-0.8.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crypto_mac__0_10_0",
        url = "https://crates.io/api/v1/crates/crypto-mac/0.10.0/download",
        type = "tar.gz",
        sha256 = "4857fd85a0c34b3c3297875b747c1e02e06b6a0ea32dd892d8192b9ce0813ea6",
        strip_prefix = "crypto-mac-0.10.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.crypto-mac-0.10.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crypto_mac__0_7_0",
        url = "https://crates.io/api/v1/crates/crypto-mac/0.7.0/download",
        type = "tar.gz",
        sha256 = "4434400df11d95d556bac068ddfedd482915eb18fe8bea89bc80b6e4b1c179e5",
        strip_prefix = "crypto-mac-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.crypto-mac-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__crypto_mac__0_9_1",
        url = "https://crates.io/api/v1/crates/crypto-mac/0.9.1/download",
        type = "tar.gz",
        sha256 = "58bcd97a54c7ca5ce2f6eb16f6bede5b0ab5f0055fedc17d2f0b4466e21671ca",
        strip_prefix = "crypto-mac-0.9.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.crypto-mac-0.9.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ctor__0_1_20",
        url = "https://crates.io/api/v1/crates/ctor/0.1.20/download",
        type = "tar.gz",
        sha256 = "5e98e2ad1a782e33928b96fc3948e7c355e5af34ba4de7670fe8bac2a3b2006d",
        strip_prefix = "ctor-0.1.20",
        build_file = Label("//rust_shared/raze/remote:BUILD.ctor-0.1.20.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__custom_derive__0_1_7",
        url = "https://crates.io/api/v1/crates/custom_derive/0.1.7/download",
        type = "tar.gz",
        sha256 = "ef8ae57c4978a2acd8b869ce6b9ca1dfe817bff704c220209fdef2c0b75a01b9",
        strip_prefix = "custom_derive-0.1.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.custom_derive-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__darling__0_12_3",
        url = "https://crates.io/api/v1/crates/darling/0.12.3/download",
        type = "tar.gz",
        sha256 = "e9d6ddad5866bb2170686ed03f6839d31a76e5407d80b1c334a2c24618543ffa",
        strip_prefix = "darling-0.12.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.darling-0.12.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__darling_core__0_12_3",
        url = "https://crates.io/api/v1/crates/darling_core/0.12.3/download",
        type = "tar.gz",
        sha256 = "a9ced1fd13dc386d5a8315899de465708cf34ee2a6d9394654515214e67bb846",
        strip_prefix = "darling_core-0.12.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.darling_core-0.12.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__darling_macro__0_12_3",
        url = "https://crates.io/api/v1/crates/darling_macro/0.12.3/download",
        type = "tar.gz",
        sha256 = "0a7a1445d54b2f9792e3b31a3e715feabbace393f38dc4ffd49d94ee9bc487d5",
        strip_prefix = "darling_macro-0.12.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.darling_macro-0.12.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__dashmap__4_0_2",
        url = "https://crates.io/api/v1/crates/dashmap/4.0.2/download",
        type = "tar.gz",
        sha256 = "e77a43b28d0668df09411cb0bc9a8c2adc40f9a048afe863e05fd43251e8e39c",
        strip_prefix = "dashmap-4.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.dashmap-4.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__derivative__2_2_0",
        url = "https://crates.io/api/v1/crates/derivative/2.2.0/download",
        type = "tar.gz",
        sha256 = "fcc3dd5e9e9c0b295d6e1e4d811fb6f157d5ffd784b8d202fc62eac8035a770b",
        strip_prefix = "derivative-2.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.derivative-2.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__des__0_5_0",
        url = "https://crates.io/api/v1/crates/des/0.5.0/download",
        type = "tar.gz",
        sha256 = "e084b5048dec677e6c9f27d7abc551dde7d127cf4127fea82323c98a30d7fa0d",
        strip_prefix = "des-0.5.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.des-0.5.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__difference__2_0_0",
        url = "https://crates.io/api/v1/crates/difference/2.0.0/download",
        type = "tar.gz",
        sha256 = "524cbf6897b527295dff137cec09ecf3a05f4fddffd7dfcd1585403449e74198",
        strip_prefix = "difference-2.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.difference-2.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__digest__0_8_1",
        url = "https://crates.io/api/v1/crates/digest/0.8.1/download",
        type = "tar.gz",
        sha256 = "f3d0c8c8752312f9713efd397ff63acb9f85585afbf179282e720e7704954dd5",
        strip_prefix = "digest-0.8.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.digest-0.8.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__digest__0_9_0",
        url = "https://crates.io/api/v1/crates/digest/0.9.0/download",
        type = "tar.gz",
        sha256 = "d3dd60d1080a57a05ab032377049e0591415d2b31afd7028356dbf3cc6dcb066",
        strip_prefix = "digest-0.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.digest-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__doc_comment__0_3_3",
        url = "https://crates.io/api/v1/crates/doc-comment/0.3.3/download",
        type = "tar.gz",
        sha256 = "fea41bba32d969b513997752735605054bc0dfa92b4c56bf1189f2e174be7a10",
        strip_prefix = "doc-comment-0.3.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.doc-comment-0.3.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__downcast__0_10_0",
        url = "https://crates.io/api/v1/crates/downcast/0.10.0/download",
        type = "tar.gz",
        sha256 = "4bb454f0228b18c7f4c3b0ebbee346ed9c52e7443b0999cd543ff3571205701d",
        strip_prefix = "downcast-0.10.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.downcast-0.10.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__dtoa__0_4_8",
        url = "https://crates.io/api/v1/crates/dtoa/0.4.8/download",
        type = "tar.gz",
        sha256 = "56899898ce76aaf4a0f24d914c97ea6ed976d42fec6ad33fcbb0a1103e07b2b0",
        strip_prefix = "dtoa-0.4.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.dtoa-0.4.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__either__1_6_1",
        url = "https://crates.io/api/v1/crates/either/1.6.1/download",
        type = "tar.gz",
        sha256 = "e78d4f1cc4ae33bbfc157ed5d5a5ef3bc29227303d595861deb238fcec4e9457",
        strip_prefix = "either-1.6.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.either-1.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__encoding_rs__0_8_28",
        url = "https://crates.io/api/v1/crates/encoding_rs/0.8.28/download",
        type = "tar.gz",
        sha256 = "80df024fbc5ac80f87dfef0d9f5209a252f2a497f7f42944cff24d8253cac065",
        strip_prefix = "encoding_rs-0.8.28",
        build_file = Label("//rust_shared/raze/remote:BUILD.encoding_rs-0.8.28.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__enum_as_inner__0_3_3",
        url = "https://crates.io/api/v1/crates/enum-as-inner/0.3.3/download",
        type = "tar.gz",
        sha256 = "7c5f0096a91d210159eceb2ff5e1c4da18388a170e1e3ce948aac9c8fdbbf595",
        strip_prefix = "enum-as-inner-0.3.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.enum-as-inner-0.3.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__enum_derive__0_1_7",
        url = "https://crates.io/api/v1/crates/enum_derive/0.1.7/download",
        type = "tar.gz",
        sha256 = "406ac2a8c9eedf8af9ee1489bee9e50029278a6456c740f7454cf8a158abc816",
        strip_prefix = "enum_derive-0.1.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.enum_derive-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__err_derive__0_2_4",
        url = "https://crates.io/api/v1/crates/err-derive/0.2.4/download",
        type = "tar.gz",
        sha256 = "22deed3a8124cff5fa835713fa105621e43bbdc46690c3a6b68328a012d350d4",
        strip_prefix = "err-derive-0.2.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.err-derive-0.2.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__error_chain__0_12_4",
        url = "https://crates.io/api/v1/crates/error-chain/0.12.4/download",
        type = "tar.gz",
        sha256 = "2d2f06b9cac1506ece98fe3231e3cc9c4410ec3d5b1f24ae1c8946f0742cdefc",
        strip_prefix = "error-chain-0.12.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.error-chain-0.12.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__event_listener__2_5_1",
        url = "https://crates.io/api/v1/crates/event-listener/2.5.1/download",
        type = "tar.gz",
        sha256 = "f7531096570974c3a9dcf9e4b8e1cede1ec26cf5046219fb3b9d897503b9be59",
        strip_prefix = "event-listener-2.5.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.event-listener-2.5.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__failure__0_1_8",
        url = "https://crates.io/api/v1/crates/failure/0.1.8/download",
        type = "tar.gz",
        sha256 = "d32e9bd16cc02eae7db7ef620b392808b89f6a5e16bb3497d159c6b92a0f4f86",
        strip_prefix = "failure-0.1.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.failure-0.1.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__failure_derive__0_1_8",
        url = "https://crates.io/api/v1/crates/failure_derive/0.1.8/download",
        type = "tar.gz",
        sha256 = "aa4da3c766cd7a0db8242e326e9e4e081edd567072893ed320008189715366a4",
        strip_prefix = "failure_derive-0.1.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.failure_derive-0.1.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fake_simd__0_1_2",
        url = "https://crates.io/api/v1/crates/fake-simd/0.1.2/download",
        type = "tar.gz",
        sha256 = "e88a8acf291dafb59c2d96e8f59828f3838bb1a70398823ade51a84de6a6deed",
        strip_prefix = "fake-simd-0.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.fake-simd-0.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fastrand__1_4_0",
        url = "https://crates.io/api/v1/crates/fastrand/1.4.0/download",
        type = "tar.gz",
        sha256 = "ca5faf057445ce5c9d4329e382b2ce7ca38550ef3b73a5348362d5f24e0c7fe3",
        strip_prefix = "fastrand-1.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.fastrand-1.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__flate2__1_0_20",
        url = "https://crates.io/api/v1/crates/flate2/1.0.20/download",
        type = "tar.gz",
        sha256 = "cd3aec53de10fe96d7d8c565eb17f2c687bb5518a2ec453b5b1252964526abe0",
        strip_prefix = "flate2-1.0.20",
        build_file = Label("//rust_shared/raze/remote:BUILD.flate2-1.0.20.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__float_cmp__0_8_0",
        url = "https://crates.io/api/v1/crates/float-cmp/0.8.0/download",
        type = "tar.gz",
        sha256 = "e1267f4ac4f343772758f7b1bdcbe767c218bbab93bb432acbf5162bbf85a6c4",
        strip_prefix = "float-cmp-0.8.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.float-cmp-0.8.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fnv__1_0_7",
        url = "https://crates.io/api/v1/crates/fnv/1.0.7/download",
        type = "tar.gz",
        sha256 = "3f9eec918d3f24069decb9af1554cad7c880e2da24a9afd88aca000531ab82c1",
        strip_prefix = "fnv-1.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.fnv-1.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__foreign_types__0_3_2",
        url = "https://crates.io/api/v1/crates/foreign-types/0.3.2/download",
        type = "tar.gz",
        sha256 = "f6f339eb8adc052cd2ca78910fda869aefa38d22d5cb648e6485e4d3fc06f3b1",
        strip_prefix = "foreign-types-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.foreign-types-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__foreign_types_shared__0_1_1",
        url = "https://crates.io/api/v1/crates/foreign-types-shared/0.1.1/download",
        type = "tar.gz",
        sha256 = "00b0228411908ca8685dba7fc2cdd70ec9990a6e753e89b6ac91a84c40fbaf4b",
        strip_prefix = "foreign-types-shared-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.foreign-types-shared-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__form_urlencoded__1_0_1",
        url = "https://crates.io/api/v1/crates/form_urlencoded/1.0.1/download",
        type = "tar.gz",
        sha256 = "5fc25a87fa4fd2094bffb06925852034d90a17f0d1e05197d4956d3555752191",
        strip_prefix = "form_urlencoded-1.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.form_urlencoded-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fragile__1_0_0",
        url = "https://crates.io/api/v1/crates/fragile/1.0.0/download",
        type = "tar.gz",
        sha256 = "69a039c3498dc930fe810151a34ba0c1c70b02b8625035592e74432f678591f2",
        strip_prefix = "fragile-1.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.fragile-1.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fuchsia_cprng__0_1_1",
        url = "https://crates.io/api/v1/crates/fuchsia-cprng/0.1.1/download",
        type = "tar.gz",
        sha256 = "a06f77d526c1a601b7c4cdd98f54b5eaabffc14d5f2f0296febdc7f357c6d3ba",
        strip_prefix = "fuchsia-cprng-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.fuchsia-cprng-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fuchsia_zircon__0_3_3",
        url = "https://crates.io/api/v1/crates/fuchsia-zircon/0.3.3/download",
        type = "tar.gz",
        sha256 = "2e9763c69ebaae630ba35f74888db465e49e259ba1bc0eda7d06f4a067615d82",
        strip_prefix = "fuchsia-zircon-0.3.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.fuchsia-zircon-0.3.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__fuchsia_zircon_sys__0_3_3",
        url = "https://crates.io/api/v1/crates/fuchsia-zircon-sys/0.3.3/download",
        type = "tar.gz",
        sha256 = "3dcaa9ae7725d12cdb85b3ad99a434db70b468c09ded17e012d86b5c1010f7a7",
        strip_prefix = "fuchsia-zircon-sys-0.3.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.fuchsia-zircon-sys-0.3.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__funty__1_1_0",
        url = "https://crates.io/api/v1/crates/funty/1.1.0/download",
        type = "tar.gz",
        sha256 = "fed34cd105917e91daa4da6b3728c47b068749d6a62c59811f06ed2ac71d9da7",
        strip_prefix = "funty-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.funty-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures__0_1_31",
        url = "https://crates.io/api/v1/crates/futures/0.1.31/download",
        type = "tar.gz",
        sha256 = "3a471a38ef8ed83cd6e40aa59c1ffe17db6855c18e3604d9c4ed8c08ebc28678",
        strip_prefix = "futures-0.1.31",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-0.1.31.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures__0_3_14",
        url = "https://crates.io/api/v1/crates/futures/0.3.14/download",
        type = "tar.gz",
        sha256 = "a9d5813545e459ad3ca1bff9915e9ad7f1a47dc6a91b627ce321d5863b7dd253",
        strip_prefix = "futures-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_channel__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-channel/0.3.14/download",
        type = "tar.gz",
        sha256 = "ce79c6a52a299137a6013061e0cf0e688fce5d7f1bc60125f520912fdb29ec25",
        strip_prefix = "futures-channel-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-channel-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_core__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-core/0.3.14/download",
        type = "tar.gz",
        sha256 = "098cd1c6dda6ca01650f1a37a794245eb73181d0d4d4e955e2f3c37db7af1815",
        strip_prefix = "futures-core-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-core-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_cpupool__0_1_8",
        url = "https://crates.io/api/v1/crates/futures-cpupool/0.1.8/download",
        type = "tar.gz",
        sha256 = "ab90cde24b3319636588d0c35fe03b1333857621051837ed769faefb4c2162e4",
        strip_prefix = "futures-cpupool-0.1.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-cpupool-0.1.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_executor__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-executor/0.3.14/download",
        type = "tar.gz",
        sha256 = "10f6cb7042eda00f0049b1d2080aa4b93442997ee507eb3828e8bd7577f94c9d",
        strip_prefix = "futures-executor-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-executor-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_io__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-io/0.3.14/download",
        type = "tar.gz",
        sha256 = "365a1a1fb30ea1c03a830fdb2158f5236833ac81fa0ad12fe35b29cddc35cb04",
        strip_prefix = "futures-io-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-io-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_lite__1_11_3",
        url = "https://crates.io/api/v1/crates/futures-lite/1.11.3/download",
        type = "tar.gz",
        sha256 = "b4481d0cd0de1d204a4fa55e7d45f07b1d958abcb06714b3446438e2eff695fb",
        strip_prefix = "futures-lite-1.11.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-lite-1.11.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_macro__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-macro/0.3.14/download",
        type = "tar.gz",
        sha256 = "668c6733a182cd7deb4f1de7ba3bf2120823835b3bcfbeacf7d2c4a773c1bb8b",
        strip_prefix = "futures-macro-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-macro-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_sink__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-sink/0.3.14/download",
        type = "tar.gz",
        sha256 = "5c5629433c555de3d82861a7a4e3794a4c40040390907cfbfd7143a92a426c23",
        strip_prefix = "futures-sink-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-sink-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_task__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-task/0.3.14/download",
        type = "tar.gz",
        sha256 = "ba7aa51095076f3ba6d9a1f702f74bd05ec65f555d70d2033d55ba8d69f581bc",
        strip_prefix = "futures-task-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-task-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__futures_util__0_3_14",
        url = "https://crates.io/api/v1/crates/futures-util/0.3.14/download",
        type = "tar.gz",
        sha256 = "3c144ad54d60f23927f0a6b6d816e4271278b64f005ad65e4e35291d2de9c025",
        strip_prefix = "futures-util-0.3.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.futures-util-0.3.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__generic_array__0_12_4",
        url = "https://crates.io/api/v1/crates/generic-array/0.12.4/download",
        type = "tar.gz",
        sha256 = "ffdf9f34f1447443d37393cc6c2b8313aebddcd96906caf34e54c68d8e57d7bd",
        strip_prefix = "generic-array-0.12.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.generic-array-0.12.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__generic_array__0_14_4",
        url = "https://crates.io/api/v1/crates/generic-array/0.14.4/download",
        type = "tar.gz",
        sha256 = "501466ecc8a30d1d3b7fc9229b122b2ce8ed6e9d9223f1138d4babb253e51817",
        strip_prefix = "generic-array-0.14.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.generic-array-0.14.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__getrandom__0_1_16",
        url = "https://crates.io/api/v1/crates/getrandom/0.1.16/download",
        type = "tar.gz",
        sha256 = "8fc3cb4d91f53b50155bdcfd23f6a4c39ae1969c2ae85982b135750cccaf5fce",
        strip_prefix = "getrandom-0.1.16",
        build_file = Label("//rust_shared/raze/remote:BUILD.getrandom-0.1.16.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__getrandom__0_2_2",
        url = "https://crates.io/api/v1/crates/getrandom/0.2.2/download",
        type = "tar.gz",
        sha256 = "c9495705279e7140bf035dde1f6e750c162df8b625267cd52cc44e0b156732c8",
        strip_prefix = "getrandom-0.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.getrandom-0.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__gimli__0_23_0",
        url = "https://crates.io/api/v1/crates/gimli/0.23.0/download",
        type = "tar.gz",
        sha256 = "f6503fe142514ca4799d4c26297c4248239fe8838d827db6bd6065c6ed29a6ce",
        strip_prefix = "gimli-0.23.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.gimli-0.23.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__gloo_timers__0_2_1",
        url = "https://crates.io/api/v1/crates/gloo-timers/0.2.1/download",
        type = "tar.gz",
        sha256 = "47204a46aaff920a1ea58b11d03dec6f704287d27561724a4631e450654a891f",
        strip_prefix = "gloo-timers-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.gloo-timers-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__h2__0_1_26",
        url = "https://crates.io/api/v1/crates/h2/0.1.26/download",
        type = "tar.gz",
        sha256 = "a5b34c246847f938a410a03c5458c7fee2274436675e76d8b903c08efc29c462",
        strip_prefix = "h2-0.1.26",
        build_file = Label("//rust_shared/raze/remote:BUILD.h2-0.1.26.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__h2__0_3_2",
        url = "https://crates.io/api/v1/crates/h2/0.3.2/download",
        type = "tar.gz",
        sha256 = "fc018e188373e2777d0ef2467ebff62a08e66c3f5857b23c8fbec3018210dc00",
        strip_prefix = "h2-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.h2-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hashbrown__0_9_1",
        url = "https://crates.io/api/v1/crates/hashbrown/0.9.1/download",
        type = "tar.gz",
        sha256 = "d7afe4a420e3fe79967a00898cc1f4db7c8a49a9333a29f8a4bd76a253d5cd04",
        strip_prefix = "hashbrown-0.9.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.hashbrown-0.9.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__heck__0_3_2",
        url = "https://crates.io/api/v1/crates/heck/0.3.2/download",
        type = "tar.gz",
        sha256 = "87cbf45460356b7deeb5e3415b5563308c0a9b057c85e12b06ad551f98d0a6ac",
        strip_prefix = "heck-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.heck-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hermit_abi__0_1_18",
        url = "https://crates.io/api/v1/crates/hermit-abi/0.1.18/download",
        type = "tar.gz",
        sha256 = "322f4de77956e22ed0e5032c359a0f1273f1f7f0d79bfa3b8ffbc730d7fbcc5c",
        strip_prefix = "hermit-abi-0.1.18",
        build_file = Label("//rust_shared/raze/remote:BUILD.hermit-abi-0.1.18.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hex__0_4_3",
        url = "https://crates.io/api/v1/crates/hex/0.4.3/download",
        type = "tar.gz",
        sha256 = "7f24254aa9a54b5c858eaee2f5bccdb46aaf0e486a595ed5fd8f86ba55232a70",
        strip_prefix = "hex-0.4.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.hex-0.4.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hmac__0_10_1",
        url = "https://crates.io/api/v1/crates/hmac/0.10.1/download",
        type = "tar.gz",
        sha256 = "c1441c6b1e930e2817404b5046f1f989899143a12bf92de603b69f4e0aee1e15",
        strip_prefix = "hmac-0.10.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.hmac-0.10.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hmac__0_7_1",
        url = "https://crates.io/api/v1/crates/hmac/0.7.1/download",
        type = "tar.gz",
        sha256 = "5dcb5e64cda4c23119ab41ba960d1e170a774c8e4b9d9e6a9bc18aabf5e59695",
        strip_prefix = "hmac-0.7.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.hmac-0.7.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hmac__0_9_0",
        url = "https://crates.io/api/v1/crates/hmac/0.9.0/download",
        type = "tar.gz",
        sha256 = "deae6d9dbb35ec2c502d62b8f7b1c000a0822c3b0794ba36b3149c0a1c840dff",
        strip_prefix = "hmac-0.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.hmac-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hostname__0_3_1",
        url = "https://crates.io/api/v1/crates/hostname/0.3.1/download",
        type = "tar.gz",
        sha256 = "3c731c3e10504cc8ed35cfe2f1db4c9274c3d35fa486e3b31df46f068ef3e867",
        strip_prefix = "hostname-0.3.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.hostname-0.3.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__http__0_1_21",
        url = "https://crates.io/api/v1/crates/http/0.1.21/download",
        type = "tar.gz",
        sha256 = "d6ccf5ede3a895d8856620237b2f02972c1bbc78d2965ad7fe8838d4a0ed41f0",
        strip_prefix = "http-0.1.21",
        build_file = Label("//rust_shared/raze/remote:BUILD.http-0.1.21.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__http__0_2_4",
        url = "https://crates.io/api/v1/crates/http/0.2.4/download",
        type = "tar.gz",
        sha256 = "527e8c9ac747e28542699a951517aa9a6945af506cd1f2e1b53a576c17b6cc11",
        strip_prefix = "http-0.2.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.http-0.2.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__http_body__0_1_0",
        url = "https://crates.io/api/v1/crates/http-body/0.1.0/download",
        type = "tar.gz",
        sha256 = "6741c859c1b2463a423a1dbce98d418e6c3c3fc720fb0d45528657320920292d",
        strip_prefix = "http-body-0.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.http-body-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__http_body__0_4_1",
        url = "https://crates.io/api/v1/crates/http-body/0.4.1/download",
        type = "tar.gz",
        sha256 = "5dfb77c123b4e2f72a2069aeae0b4b4949cc7e966df277813fc16347e7549737",
        strip_prefix = "http-body-0.4.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.http-body-0.4.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__httparse__1_3_6",
        url = "https://crates.io/api/v1/crates/httparse/1.3.6/download",
        type = "tar.gz",
        sha256 = "bc35c995b9d93ec174cf9a27d425c7892722101e14993cd227fdb51d70cf9589",
        strip_prefix = "httparse-1.3.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.httparse-1.3.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__httpdate__0_3_2",
        url = "https://crates.io/api/v1/crates/httpdate/0.3.2/download",
        type = "tar.gz",
        sha256 = "494b4d60369511e7dea41cf646832512a94e542f68bb9c49e54518e0f468eb47",
        strip_prefix = "httpdate-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.httpdate-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hyper__0_12_36",
        url = "https://crates.io/api/v1/crates/hyper/0.12.36/download",
        type = "tar.gz",
        sha256 = "5c843caf6296fc1f93444735205af9ed4e109a539005abb2564ae1d6fad34c52",
        strip_prefix = "hyper-0.12.36",
        build_file = Label("//rust_shared/raze/remote:BUILD.hyper-0.12.36.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hyper__0_14_5",
        url = "https://crates.io/api/v1/crates/hyper/0.14.5/download",
        type = "tar.gz",
        sha256 = "8bf09f61b52cfcf4c00de50df88ae423d6c02354e385a86341133b5338630ad1",
        strip_prefix = "hyper-0.14.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.hyper-0.14.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hyper_tls__0_3_2",
        url = "https://crates.io/api/v1/crates/hyper-tls/0.3.2/download",
        type = "tar.gz",
        sha256 = "3a800d6aa50af4b5850b2b0f659625ce9504df908e9733b635720483be26174f",
        strip_prefix = "hyper-tls-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.hyper-tls-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__hyper_tls__0_5_0",
        url = "https://crates.io/api/v1/crates/hyper-tls/0.5.0/download",
        type = "tar.gz",
        sha256 = "d6183ddfa99b85da61a140bea0efc93fdf56ceaa041b37d553518030827f9905",
        strip_prefix = "hyper-tls-0.5.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.hyper-tls-0.5.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ident_case__1_0_1",
        url = "https://crates.io/api/v1/crates/ident_case/1.0.1/download",
        type = "tar.gz",
        sha256 = "b9e0384b61958566e926dc50660321d12159025e767c18e043daf26b70104c39",
        strip_prefix = "ident_case-1.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.ident_case-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__idna__0_1_5",
        url = "https://crates.io/api/v1/crates/idna/0.1.5/download",
        type = "tar.gz",
        sha256 = "38f09e0f0b1fb55fdee1f17470ad800da77af5186a1a76c026b679358b7e844e",
        strip_prefix = "idna-0.1.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.idna-0.1.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__idna__0_2_1",
        url = "https://crates.io/api/v1/crates/idna/0.2.1/download",
        type = "tar.gz",
        sha256 = "de910d521f7cc3135c4de8db1cb910e0b5ed1dc6f57c381cd07e8e661ce10094",
        strip_prefix = "idna-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.idna-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__indexmap__1_6_2",
        url = "https://crates.io/api/v1/crates/indexmap/1.6.2/download",
        type = "tar.gz",
        sha256 = "824845a0bf897a9042383849b02c1bc219c2383772efcd5c6f9766fa4b81aef3",
        strip_prefix = "indexmap-1.6.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.indexmap-1.6.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__instant__0_1_9",
        url = "https://crates.io/api/v1/crates/instant/0.1.9/download",
        type = "tar.gz",
        sha256 = "61124eeebbd69b8190558df225adf7e4caafce0d743919e5d6b19652314ec5ec",
        strip_prefix = "instant-0.1.9",
        build_file = Label("//rust_shared/raze/remote:BUILD.instant-0.1.9.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__iovec__0_1_4",
        url = "https://crates.io/api/v1/crates/iovec/0.1.4/download",
        type = "tar.gz",
        sha256 = "b2b3ea6ff95e175473f8ffe6a7eb7c00d054240321b84c57051175fe3c1e075e",
        strip_prefix = "iovec-0.1.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.iovec-0.1.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ipconfig__0_2_2",
        url = "https://crates.io/api/v1/crates/ipconfig/0.2.2/download",
        type = "tar.gz",
        sha256 = "f7e2f18aece9709094573a9f24f483c4f65caa4298e2f7ae1b71cc65d853fad7",
        strip_prefix = "ipconfig-0.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.ipconfig-0.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ipnet__2_3_0",
        url = "https://crates.io/api/v1/crates/ipnet/2.3.0/download",
        type = "tar.gz",
        sha256 = "47be2f14c678be2fdcab04ab1171db51b2762ce6f0a8ee87c8dd4a04ed216135",
        strip_prefix = "ipnet-2.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.ipnet-2.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__itertools__0_9_0",
        url = "https://crates.io/api/v1/crates/itertools/0.9.0/download",
        type = "tar.gz",
        sha256 = "284f18f85651fe11e8a991b2adb42cb078325c996ed026d994719efcfca1d54b",
        strip_prefix = "itertools-0.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.itertools-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__itoa__0_4_7",
        url = "https://crates.io/api/v1/crates/itoa/0.4.7/download",
        type = "tar.gz",
        sha256 = "dd25036021b0de88a0aff6b850051563c6516d0bf53f8638938edbb9de732736",
        strip_prefix = "itoa-0.4.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.itoa-0.4.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__js_sys__0_3_50",
        url = "https://crates.io/api/v1/crates/js-sys/0.3.50/download",
        type = "tar.gz",
        sha256 = "2d99f9e3e84b8f67f846ef5b4cbbc3b1c29f6c759fcbce6f01aa0e73d932a24c",
        strip_prefix = "js-sys-0.3.50",
        build_file = Label("//rust_shared/raze/remote:BUILD.js-sys-0.3.50.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__jwt__0_13_0",
        url = "https://crates.io/api/v1/crates/jwt/0.13.0/download",
        type = "tar.gz",
        sha256 = "86e46349d67dc03bdbdb28da0337a355a53ca1d5156452722c36fe21d0e6389b",
        strip_prefix = "jwt-0.13.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.jwt-0.13.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__kernel32_sys__0_2_2",
        url = "https://crates.io/api/v1/crates/kernel32-sys/0.2.2/download",
        type = "tar.gz",
        sha256 = "7507624b29483431c0ba2d82aece8ca6cdba9382bff4ddd0f7490560c056098d",
        strip_prefix = "kernel32-sys-0.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.kernel32-sys-0.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__kv_log_macro__1_0_7",
        url = "https://crates.io/api/v1/crates/kv-log-macro/1.0.7/download",
        type = "tar.gz",
        sha256 = "0de8b303297635ad57c9f5059fd9cee7a47f8e8daa09df0fcd07dd39fb22977f",
        strip_prefix = "kv-log-macro-1.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.kv-log-macro-1.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lapin__1_6_9",
        url = "https://crates.io/api/v1/crates/lapin/1.6.9/download",
        type = "tar.gz",
        sha256 = "6b5dafb55214278207bcead8c4be2fb204bba3e6c99aaff473df407c2a0a4f9c",
        strip_prefix = "lapin-1.6.9",
        build_file = Label("//rust_shared/raze/remote:BUILD.lapin-1.6.9.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lazy_static__1_4_0",
        url = "https://crates.io/api/v1/crates/lazy_static/1.4.0/download",
        type = "tar.gz",
        sha256 = "e2abad23fbc42b3700f2f279844dc832adb2b2eb069b2df918f455c4e18cc646",
        strip_prefix = "lazy_static-1.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.lazy_static-1.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lexical_core__0_7_5",
        url = "https://crates.io/api/v1/crates/lexical-core/0.7.5/download",
        type = "tar.gz",
        sha256 = "21f866863575d0e1d654fbeeabdc927292fdf862873dc3c96c6f753357e13374",
        strip_prefix = "lexical-core-0.7.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.lexical-core-0.7.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__libc__0_2_93",
        url = "https://crates.io/api/v1/crates/libc/0.2.93/download",
        type = "tar.gz",
        sha256 = "9385f66bf6105b241aa65a61cb923ef20efc665cb9f9bb50ac2f0c4b7f378d41",
        strip_prefix = "libc-0.2.93",
        build_file = Label("//rust_shared/raze/remote:BUILD.libc-0.2.93.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__linked_hash_map__0_5_4",
        url = "https://crates.io/api/v1/crates/linked-hash-map/0.5.4/download",
        type = "tar.gz",
        sha256 = "7fb9b38af92608140b86b693604b9ffcc5824240a484d1ecd4795bacb2fe88f3",
        strip_prefix = "linked-hash-map-0.5.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.linked-hash-map-0.5.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lock_api__0_3_4",
        url = "https://crates.io/api/v1/crates/lock_api/0.3.4/download",
        type = "tar.gz",
        sha256 = "c4da24a77a3d8a6d4862d95f72e6fdb9c09a643ecdb402d754004a557f2bec75",
        strip_prefix = "lock_api-0.3.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.lock_api-0.3.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lock_api__0_4_3",
        url = "https://crates.io/api/v1/crates/lock_api/0.4.3/download",
        type = "tar.gz",
        sha256 = "5a3c91c24eae6777794bb1997ad98bbb87daf92890acab859f7eaa4320333176",
        strip_prefix = "lock_api-0.4.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.lock_api-0.4.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__log__0_4_14",
        url = "https://crates.io/api/v1/crates/log/0.4.14/download",
        type = "tar.gz",
        sha256 = "51b9bbe6c47d51fc3e1a9b945965946b4c44142ab8792c50835a980d362c2710",
        strip_prefix = "log-0.4.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.log-0.4.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__lru_cache__0_1_2",
        url = "https://crates.io/api/v1/crates/lru-cache/0.1.2/download",
        type = "tar.gz",
        sha256 = "31e24f1ad8321ca0e8a1e0ac13f23cb668e6f5466c2c57319f6a5cf1cc8e3b1c",
        strip_prefix = "lru-cache-0.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.lru-cache-0.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__match_cfg__0_1_0",
        url = "https://crates.io/api/v1/crates/match_cfg/0.1.0/download",
        type = "tar.gz",
        sha256 = "ffbee8634e0d45d258acb448e7eaab3fce7a0a467395d4d9f228e3c1f01fb2e4",
        strip_prefix = "match_cfg-0.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.match_cfg-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__matches__0_1_8",
        url = "https://crates.io/api/v1/crates/matches/0.1.8/download",
        type = "tar.gz",
        sha256 = "7ffc5c5338469d4d3ea17d269fa8ea3512ad247247c30bd2df69e68309ed0a08",
        strip_prefix = "matches-0.1.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.matches-0.1.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__maybe_uninit__2_0_0",
        url = "https://crates.io/api/v1/crates/maybe-uninit/2.0.0/download",
        type = "tar.gz",
        sha256 = "60302e4db3a61da70c0cb7991976248362f30319e88850c487b9b95bbf059e00",
        strip_prefix = "maybe-uninit-2.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.maybe-uninit-2.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__md_5__0_8_0",
        url = "https://crates.io/api/v1/crates/md-5/0.8.0/download",
        type = "tar.gz",
        sha256 = "a18af3dcaf2b0219366cdb4e2af65a6101457b415c3d1a5c71dd9c2b7c77b9c8",
        strip_prefix = "md-5-0.8.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.md-5-0.8.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__memchr__2_3_4",
        url = "https://crates.io/api/v1/crates/memchr/2.3.4/download",
        type = "tar.gz",
        sha256 = "0ee1c47aaa256ecabcaea351eae4a9b01ef39ed810004e298d2511ed284b1525",
        strip_prefix = "memchr-2.3.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.memchr-2.3.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__memoffset__0_5_6",
        url = "https://crates.io/api/v1/crates/memoffset/0.5.6/download",
        type = "tar.gz",
        sha256 = "043175f069eda7b85febe4a74abbaeff828d9f8b448515d3151a14a3542811aa",
        strip_prefix = "memoffset-0.5.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.memoffset-0.5.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mime__0_3_16",
        url = "https://crates.io/api/v1/crates/mime/0.3.16/download",
        type = "tar.gz",
        sha256 = "2a60c7ce501c71e03a9c9c0d35b861413ae925bd979cc7a4e30d060069aaac8d",
        strip_prefix = "mime-0.3.16",
        build_file = Label("//rust_shared/raze/remote:BUILD.mime-0.3.16.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mime_guess__2_0_3",
        url = "https://crates.io/api/v1/crates/mime_guess/2.0.3/download",
        type = "tar.gz",
        sha256 = "2684d4c2e97d99848d30b324b00c8fcc7e5c897b7cbb5819b09e7c90e8baf212",
        strip_prefix = "mime_guess-2.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.mime_guess-2.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__miniz_oxide__0_4_4",
        url = "https://crates.io/api/v1/crates/miniz_oxide/0.4.4/download",
        type = "tar.gz",
        sha256 = "a92518e98c078586bc6c934028adcca4c92a53d6a958196de835170a01d84e4b",
        strip_prefix = "miniz_oxide-0.4.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.miniz_oxide-0.4.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mio__0_6_23",
        url = "https://crates.io/api/v1/crates/mio/0.6.23/download",
        type = "tar.gz",
        sha256 = "4afd66f5b91bf2a3bc13fad0e21caedac168ca4c707504e75585648ae80e4cc4",
        strip_prefix = "mio-0.6.23",
        build_file = Label("//rust_shared/raze/remote:BUILD.mio-0.6.23.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mio__0_7_11",
        url = "https://crates.io/api/v1/crates/mio/0.7.11/download",
        type = "tar.gz",
        sha256 = "cf80d3e903b34e0bd7282b218398aec54e082c840d9baf8339e0080a0c542956",
        strip_prefix = "mio-0.7.11",
        build_file = Label("//rust_shared/raze/remote:BUILD.mio-0.7.11.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__miow__0_2_2",
        url = "https://crates.io/api/v1/crates/miow/0.2.2/download",
        type = "tar.gz",
        sha256 = "ebd808424166322d4a38da87083bfddd3ac4c131334ed55856112eb06d46944d",
        strip_prefix = "miow-0.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.miow-0.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__miow__0_3_7",
        url = "https://crates.io/api/v1/crates/miow/0.3.7/download",
        type = "tar.gz",
        sha256 = "b9f1c5b025cda876f66ef43a113f91ebc9f4ccef34843000e0adf6ebbab84e21",
        strip_prefix = "miow-0.3.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.miow-0.3.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mockall__0_9_1",
        url = "https://crates.io/api/v1/crates/mockall/0.9.1/download",
        type = "tar.gz",
        sha256 = "18d614ad23f9bb59119b8b5670a85c7ba92c5e9adf4385c81ea00c51c8be33d5",
        strip_prefix = "mockall-0.9.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.mockall-0.9.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mockall_derive__0_9_1",
        url = "https://crates.io/api/v1/crates/mockall_derive/0.9.1/download",
        type = "tar.gz",
        sha256 = "5dd4234635bca06fc96c7368d038061e0aae1b00a764dc817e900dc974e3deea",
        strip_prefix = "mockall_derive-0.9.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.mockall_derive-0.9.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__mongodb__1_2_1",
        url = "https://crates.io/api/v1/crates/mongodb/1.2.1/download",
        type = "tar.gz",
        sha256 = "4d2870708d8bdb6c652e20a7a04cf2e7e0493c786a650cc39eecd134a46e919e",
        strip_prefix = "mongodb-1.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.mongodb-1.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__native_tls__0_2_7",
        url = "https://crates.io/api/v1/crates/native-tls/0.2.7/download",
        type = "tar.gz",
        sha256 = "b8d96b2e1c8da3957d58100b09f102c6d9cfdfced01b7ec5a8974044bb09dbd4",
        strip_prefix = "native-tls-0.2.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.native-tls-0.2.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__nb_connect__1_1_0",
        url = "https://crates.io/api/v1/crates/nb-connect/1.1.0/download",
        type = "tar.gz",
        sha256 = "a19900e7eee95eb2b3c2e26d12a874cc80aaf750e31be6fcbe743ead369fa45d",
        strip_prefix = "nb-connect-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.nb-connect-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__net2__0_2_37",
        url = "https://crates.io/api/v1/crates/net2/0.2.37/download",
        type = "tar.gz",
        sha256 = "391630d12b68002ae1e25e8f974306474966550ad82dac6886fb8910c19568ae",
        strip_prefix = "net2-0.2.37",
        build_file = Label("//rust_shared/raze/remote:BUILD.net2-0.2.37.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__nom__4_2_3",
        url = "https://crates.io/api/v1/crates/nom/4.2.3/download",
        type = "tar.gz",
        sha256 = "2ad2a91a8e869eeb30b9cb3119ae87773a8f4ae617f41b1eb9c154b2905f7bd6",
        strip_prefix = "nom-4.2.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.nom-4.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__nom__6_1_2",
        url = "https://crates.io/api/v1/crates/nom/6.1.2/download",
        type = "tar.gz",
        sha256 = "e7413f999671bd4745a7b624bd370a569fb6bc574b23c83a3c5ed2e453f3d5e2",
        strip_prefix = "nom-6.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.nom-6.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__normalize_line_endings__0_3_0",
        url = "https://crates.io/api/v1/crates/normalize-line-endings/0.3.0/download",
        type = "tar.gz",
        sha256 = "61807f77802ff30975e01f4f071c8ba10c022052f98b3294119f3e615d13e5be",
        strip_prefix = "normalize-line-endings-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.normalize-line-endings-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ntapi__0_3_6",
        url = "https://crates.io/api/v1/crates/ntapi/0.3.6/download",
        type = "tar.gz",
        sha256 = "3f6bb902e437b6d86e03cce10a7e2af662292c5dfef23b65899ea3ac9354ad44",
        strip_prefix = "ntapi-0.3.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.ntapi-0.3.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__num_integer__0_1_44",
        url = "https://crates.io/api/v1/crates/num-integer/0.1.44/download",
        type = "tar.gz",
        sha256 = "d2cc698a63b549a70bc047073d2949cce27cd1c7b0a4a862d08a8031bc2801db",
        strip_prefix = "num-integer-0.1.44",
        build_file = Label("//rust_shared/raze/remote:BUILD.num-integer-0.1.44.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__num_traits__0_2_14",
        url = "https://crates.io/api/v1/crates/num-traits/0.2.14/download",
        type = "tar.gz",
        sha256 = "9a64b1ec5cda2586e284722486d802acf1f7dbdc623e2bfc57e65ca1cd099290",
        strip_prefix = "num-traits-0.2.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.num-traits-0.2.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__num_cpus__1_13_0",
        url = "https://crates.io/api/v1/crates/num_cpus/1.13.0/download",
        type = "tar.gz",
        sha256 = "05499f3756671c15885fee9034446956fff3f243d6077b91e5767df161f766b3",
        strip_prefix = "num_cpus-1.13.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.num_cpus-1.13.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__oauth2__3_0_0",
        url = "https://crates.io/api/v1/crates/oauth2/3.0.0/download",
        type = "tar.gz",
        sha256 = "88d21857941367fdd2514553ff1578254267afd9c1e69d2d8592ba98223560a0",
        strip_prefix = "oauth2-3.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.oauth2-3.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__object__0_23_0",
        url = "https://crates.io/api/v1/crates/object/0.23.0/download",
        type = "tar.gz",
        sha256 = "a9a7ab5d64814df0fe4a4b5ead45ed6c5f181ee3ff04ba344313a6c80446c5d4",
        strip_prefix = "object-0.23.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.object-0.23.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__once_cell__1_7_2",
        url = "https://crates.io/api/v1/crates/once_cell/1.7.2/download",
        type = "tar.gz",
        sha256 = "af8b08b04175473088b46763e51ee54da5f9a164bc162f615b91bc179dbf15a3",
        strip_prefix = "once_cell-1.7.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.once_cell-1.7.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__opaque_debug__0_2_3",
        url = "https://crates.io/api/v1/crates/opaque-debug/0.2.3/download",
        type = "tar.gz",
        sha256 = "2839e79665f131bdb5782e51f2c6c9599c133c6098982a54c794358bf432529c",
        strip_prefix = "opaque-debug-0.2.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.opaque-debug-0.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__opaque_debug__0_3_0",
        url = "https://crates.io/api/v1/crates/opaque-debug/0.3.0/download",
        type = "tar.gz",
        sha256 = "624a8340c38c1b80fd549087862da4ba43e08858af025b236e509b6649fc13d5",
        strip_prefix = "opaque-debug-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.opaque-debug-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__openssl__0_10_33",
        url = "https://crates.io/api/v1/crates/openssl/0.10.33/download",
        type = "tar.gz",
        sha256 = "a61075b62a23fef5a29815de7536d940aa35ce96d18ce0cc5076272db678a577",
        strip_prefix = "openssl-0.10.33",
        build_file = Label("//rust_shared/raze/remote:BUILD.openssl-0.10.33.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__openssl_probe__0_1_2",
        url = "https://crates.io/api/v1/crates/openssl-probe/0.1.2/download",
        type = "tar.gz",
        sha256 = "77af24da69f9d9341038eba93a073b1fdaaa1b788221b00a69bce9e762cb32de",
        strip_prefix = "openssl-probe-0.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.openssl-probe-0.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__openssl_sys__0_9_61",
        url = "https://crates.io/api/v1/crates/openssl-sys/0.9.61/download",
        type = "tar.gz",
        sha256 = "313752393519e876837e09e1fa183ddef0be7735868dced3196f4472d536277f",
        strip_prefix = "openssl-sys-0.9.61",
        build_file = Label("//rust_shared/raze/remote:BUILD.openssl-sys-0.9.61.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__os_info__3_0_2",
        url = "https://crates.io/api/v1/crates/os_info/3.0.2/download",
        type = "tar.gz",
        sha256 = "4181c9203e70e66f39def4240a7e33f74d1d95b42c08320f9ac298c580934ab2",
        strip_prefix = "os_info-3.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.os_info-3.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__p12__0_2_0",
        url = "https://crates.io/api/v1/crates/p12/0.2.0/download",
        type = "tar.gz",
        sha256 = "18d9bbc57ec03c625d98cf72abe80d5b6ac4e3b5c4fe6e68150be8778da9c7a0",
        strip_prefix = "p12-0.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.p12-0.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__parking__2_0_0",
        url = "https://crates.io/api/v1/crates/parking/2.0.0/download",
        type = "tar.gz",
        sha256 = "427c3892f9e783d91cc128285287e70a59e206ca452770ece88a76f7a3eddd72",
        strip_prefix = "parking-2.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.parking-2.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__parking_lot__0_11_1",
        url = "https://crates.io/api/v1/crates/parking_lot/0.11.1/download",
        type = "tar.gz",
        sha256 = "6d7744ac029df22dca6284efe4e898991d28e3085c706c972bcd7da4a27a15eb",
        strip_prefix = "parking_lot-0.11.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.parking_lot-0.11.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__parking_lot__0_9_0",
        url = "https://crates.io/api/v1/crates/parking_lot/0.9.0/download",
        type = "tar.gz",
        sha256 = "f842b1982eb6c2fe34036a4fbfb06dd185a3f5c8edfaacdf7d1ea10b07de6252",
        strip_prefix = "parking_lot-0.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.parking_lot-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__parking_lot_core__0_6_2",
        url = "https://crates.io/api/v1/crates/parking_lot_core/0.6.2/download",
        type = "tar.gz",
        sha256 = "b876b1b9e7ac6e1a74a6da34d25c42e17e8862aa409cbbbdcfc8d86c6f3bc62b",
        strip_prefix = "parking_lot_core-0.6.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.parking_lot_core-0.6.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__parking_lot_core__0_8_3",
        url = "https://crates.io/api/v1/crates/parking_lot_core/0.8.3/download",
        type = "tar.gz",
        sha256 = "fa7a782938e745763fe6907fc6ba86946d72f49fe7e21de074e08128a99fb018",
        strip_prefix = "parking_lot_core-0.8.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.parking_lot_core-0.8.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pbkdf2__0_3_0",
        url = "https://crates.io/api/v1/crates/pbkdf2/0.3.0/download",
        type = "tar.gz",
        sha256 = "006c038a43a45995a9670da19e67600114740e8511d4333bf97a56e66a7542d9",
        strip_prefix = "pbkdf2-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.pbkdf2-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__percent_encoding__1_0_1",
        url = "https://crates.io/api/v1/crates/percent-encoding/1.0.1/download",
        type = "tar.gz",
        sha256 = "31010dd2e1ac33d5b46a5b413495239882813e0369f8ed8a5e266f173602f831",
        strip_prefix = "percent-encoding-1.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.percent-encoding-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__percent_encoding__2_1_0",
        url = "https://crates.io/api/v1/crates/percent-encoding/2.1.0/download",
        type = "tar.gz",
        sha256 = "d4fd5641d01c8f18a23da7b6fe29298ff4b55afcccdf78973b24cf3175fee32e",
        strip_prefix = "percent-encoding-2.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.percent-encoding-2.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pin_project__1_0_6",
        url = "https://crates.io/api/v1/crates/pin-project/1.0.6/download",
        type = "tar.gz",
        sha256 = "bc174859768806e91ae575187ada95c91a29e96a98dc5d2cd9a1fed039501ba6",
        strip_prefix = "pin-project-1.0.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.pin-project-1.0.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pin_project_internal__1_0_6",
        url = "https://crates.io/api/v1/crates/pin-project-internal/1.0.6/download",
        type = "tar.gz",
        sha256 = "a490329918e856ed1b083f244e3bfe2d8c4f336407e4ea9e1a9f479ff09049e5",
        strip_prefix = "pin-project-internal-1.0.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.pin-project-internal-1.0.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pin_project_lite__0_1_12",
        url = "https://crates.io/api/v1/crates/pin-project-lite/0.1.12/download",
        type = "tar.gz",
        sha256 = "257b64915a082f7811703966789728173279bdebb956b143dbcd23f6f970a777",
        strip_prefix = "pin-project-lite-0.1.12",
        build_file = Label("//rust_shared/raze/remote:BUILD.pin-project-lite-0.1.12.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pin_project_lite__0_2_6",
        url = "https://crates.io/api/v1/crates/pin-project-lite/0.2.6/download",
        type = "tar.gz",
        sha256 = "dc0e1f259c92177c30a4c9d177246edd0a3568b25756a977d0632cf8fa37e905",
        strip_prefix = "pin-project-lite-0.2.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.pin-project-lite-0.2.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pin_utils__0_1_0",
        url = "https://crates.io/api/v1/crates/pin-utils/0.1.0/download",
        type = "tar.gz",
        sha256 = "8b870d8c151b6f2fb93e84a13146138f05d02ed11c7e7c54f8826aaaf7c9f184",
        strip_prefix = "pin-utils-0.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.pin-utils-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pinky_swear__4_4_0",
        url = "https://crates.io/api/v1/crates/pinky-swear/4.4.0/download",
        type = "tar.gz",
        sha256 = "9bf8cda6f8e1500338634e4e3ce90ac59eb7929a1e088b6946c742be1cc44dc1",
        strip_prefix = "pinky-swear-4.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.pinky-swear-4.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__pkg_config__0_3_19",
        url = "https://crates.io/api/v1/crates/pkg-config/0.3.19/download",
        type = "tar.gz",
        sha256 = "3831453b3449ceb48b6d9c7ad7c96d5ea673e9b470a1dc578c2ce6521230884c",
        strip_prefix = "pkg-config-0.3.19",
        build_file = Label("//rust_shared/raze/remote:BUILD.pkg-config-0.3.19.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__polling__2_0_3",
        url = "https://crates.io/api/v1/crates/polling/2.0.3/download",
        type = "tar.gz",
        sha256 = "4fc12d774e799ee9ebae13f4076ca003b40d18a11ac0f3641e6f899618580b7b",
        strip_prefix = "polling-2.0.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.polling-2.0.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ppv_lite86__0_2_10",
        url = "https://crates.io/api/v1/crates/ppv-lite86/0.2.10/download",
        type = "tar.gz",
        sha256 = "ac74c624d6b2d21f425f752262f42188365d7b8ff1aff74c82e45136510a4857",
        strip_prefix = "ppv-lite86-0.2.10",
        build_file = Label("//rust_shared/raze/remote:BUILD.ppv-lite86-0.2.10.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__predicates__1_0_7",
        url = "https://crates.io/api/v1/crates/predicates/1.0.7/download",
        type = "tar.gz",
        sha256 = "eeb433456c1a57cc93554dea3ce40b4c19c4057e41c55d4a0f3d84ea71c325aa",
        strip_prefix = "predicates-1.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.predicates-1.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__predicates_core__1_0_2",
        url = "https://crates.io/api/v1/crates/predicates-core/1.0.2/download",
        type = "tar.gz",
        sha256 = "57e35a3326b75e49aa85f5dc6ec15b41108cf5aee58eabb1f274dd18b73c2451",
        strip_prefix = "predicates-core-1.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.predicates-core-1.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__predicates_tree__1_0_2",
        url = "https://crates.io/api/v1/crates/predicates-tree/1.0.2/download",
        type = "tar.gz",
        sha256 = "15f553275e5721409451eb85e15fd9a860a6e5ab4496eb215987502b5f5391f2",
        strip_prefix = "predicates-tree-1.0.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.predicates-tree-1.0.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro_error__1_0_4",
        url = "https://crates.io/api/v1/crates/proc-macro-error/1.0.4/download",
        type = "tar.gz",
        sha256 = "da25490ff9892aab3fcf7c36f08cfb902dd3e71ca0f9f9517bea02a73a5ce38c",
        strip_prefix = "proc-macro-error-1.0.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.proc-macro-error-1.0.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro_error_attr__1_0_4",
        url = "https://crates.io/api/v1/crates/proc-macro-error-attr/1.0.4/download",
        type = "tar.gz",
        sha256 = "a1be40180e52ecc98ad80b184934baf3d0d29f979574e439af5a55274b35f869",
        strip_prefix = "proc-macro-error-attr-1.0.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.proc-macro-error-attr-1.0.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro_hack__0_5_19",
        url = "https://crates.io/api/v1/crates/proc-macro-hack/0.5.19/download",
        type = "tar.gz",
        sha256 = "dbf0c48bc1d91375ae5c3cd81e3722dff1abcf81a30960240640d223f59fe0e5",
        strip_prefix = "proc-macro-hack-0.5.19",
        build_file = Label("//rust_shared/raze/remote:BUILD.proc-macro-hack-0.5.19.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro_nested__0_1_7",
        url = "https://crates.io/api/v1/crates/proc-macro-nested/0.1.7/download",
        type = "tar.gz",
        sha256 = "bc881b2c22681370c6a780e47af9840ef841837bc98118431d4e1868bd0c1086",
        strip_prefix = "proc-macro-nested-0.1.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.proc-macro-nested-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro2__0_4_30",
        url = "https://crates.io/api/v1/crates/proc-macro2/0.4.30/download",
        type = "tar.gz",
        sha256 = "cf3d2011ab5c909338f7887f4fc896d35932e29146c12c8d01da6b22a80ba759",
        strip_prefix = "proc-macro2-0.4.30",
        build_file = Label("//rust_shared/raze/remote:BUILD.proc-macro2-0.4.30.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__proc_macro2__1_0_26",
        url = "https://crates.io/api/v1/crates/proc-macro2/1.0.26/download",
        type = "tar.gz",
        sha256 = "a152013215dca273577e18d2bf00fa862b89b24169fb78c4c95aeb07992c9cec",
        strip_prefix = "proc-macro2-1.0.26",
        build_file = Label("//rust_shared/raze/remote:BUILD.proc-macro2-1.0.26.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__prost__0_7_0",
        url = "https://crates.io/api/v1/crates/prost/0.7.0/download",
        type = "tar.gz",
        sha256 = "9e6984d2f1a23009bd270b8bb56d0926810a3d483f59c987d77969e9d8e840b2",
        strip_prefix = "prost-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.prost-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__prost_derive__0_7_0",
        url = "https://crates.io/api/v1/crates/prost-derive/0.7.0/download",
        type = "tar.gz",
        sha256 = "169a15f3008ecb5160cba7d37bcd690a7601b6d30cfb87a117d45e59d52af5d4",
        strip_prefix = "prost-derive-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.prost-derive-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__prost_types__0_7_0",
        url = "https://crates.io/api/v1/crates/prost-types/0.7.0/download",
        type = "tar.gz",
        sha256 = "b518d7cdd93dab1d1122cf07fa9a60771836c668dde9d9e2a139f957f0d9f1bb",
        strip_prefix = "prost-types-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.prost-types-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__publicsuffix__1_5_4",
        url = "https://crates.io/api/v1/crates/publicsuffix/1.5.4/download",
        type = "tar.gz",
        sha256 = "3bbaa49075179162b49acac1c6aa45fb4dafb5f13cf6794276d77bc7fd95757b",
        strip_prefix = "publicsuffix-1.5.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.publicsuffix-1.5.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__quick_error__1_2_3",
        url = "https://crates.io/api/v1/crates/quick-error/1.2.3/download",
        type = "tar.gz",
        sha256 = "a1d01941d82fa2ab50be1e79e6714289dd7cde78eba4c074bc5a4374f650dfe0",
        strip_prefix = "quick-error-1.2.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.quick-error-1.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__quote__0_6_13",
        url = "https://crates.io/api/v1/crates/quote/0.6.13/download",
        type = "tar.gz",
        sha256 = "6ce23b6b870e8f94f81fb0a363d65d86675884b34a09043c81e5562f11c1f8e1",
        strip_prefix = "quote-0.6.13",
        build_file = Label("//rust_shared/raze/remote:BUILD.quote-0.6.13.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__quote__1_0_9",
        url = "https://crates.io/api/v1/crates/quote/1.0.9/download",
        type = "tar.gz",
        sha256 = "c3d0b9745dc2debf507c8422de05d7226cc1f0644216dfdfead988f9b1ab32a7",
        strip_prefix = "quote-1.0.9",
        build_file = Label("//rust_shared/raze/remote:BUILD.quote-1.0.9.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__radium__0_5_3",
        url = "https://crates.io/api/v1/crates/radium/0.5.3/download",
        type = "tar.gz",
        sha256 = "941ba9d78d8e2f7ce474c015eea4d9c6d25b6a3327f9832ee29a4de27f91bbb8",
        strip_prefix = "radium-0.5.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.radium-0.5.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand__0_6_5",
        url = "https://crates.io/api/v1/crates/rand/0.6.5/download",
        type = "tar.gz",
        sha256 = "6d71dacdc3c88c1fde3885a3be3fbab9f35724e6ce99467f7d9c5026132184ca",
        strip_prefix = "rand-0.6.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand-0.6.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand__0_7_3",
        url = "https://crates.io/api/v1/crates/rand/0.7.3/download",
        type = "tar.gz",
        sha256 = "6a6b1679d49b24bbfe0c803429aa1874472f50d9b363131f0e89fc356b544d03",
        strip_prefix = "rand-0.7.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand-0.7.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand__0_8_3",
        url = "https://crates.io/api/v1/crates/rand/0.8.3/download",
        type = "tar.gz",
        sha256 = "0ef9e7e66b4468674bfcb0c81af8b7fa0bb154fa9f28eb840da5c447baeb8d7e",
        strip_prefix = "rand-0.8.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand-0.8.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_chacha__0_1_1",
        url = "https://crates.io/api/v1/crates/rand_chacha/0.1.1/download",
        type = "tar.gz",
        sha256 = "556d3a1ca6600bfcbab7c7c91ccb085ac7fbbcd70e008a98742e7847f4f7bcef",
        strip_prefix = "rand_chacha-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_chacha-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_chacha__0_2_2",
        url = "https://crates.io/api/v1/crates/rand_chacha/0.2.2/download",
        type = "tar.gz",
        sha256 = "f4c8ed856279c9737206bf725bf36935d8666ead7aa69b52be55af369d193402",
        strip_prefix = "rand_chacha-0.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_chacha-0.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_chacha__0_3_0",
        url = "https://crates.io/api/v1/crates/rand_chacha/0.3.0/download",
        type = "tar.gz",
        sha256 = "e12735cf05c9e10bf21534da50a147b924d555dc7a547c42e6bb2d5b6017ae0d",
        strip_prefix = "rand_chacha-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_chacha-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_core__0_3_1",
        url = "https://crates.io/api/v1/crates/rand_core/0.3.1/download",
        type = "tar.gz",
        sha256 = "7a6fdeb83b075e8266dcc8762c22776f6877a63111121f5f8c7411e5be7eed4b",
        strip_prefix = "rand_core-0.3.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_core-0.3.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_core__0_4_2",
        url = "https://crates.io/api/v1/crates/rand_core/0.4.2/download",
        type = "tar.gz",
        sha256 = "9c33a3c44ca05fa6f1807d8e6743f3824e8509beca625669633be0acbdf509dc",
        strip_prefix = "rand_core-0.4.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_core-0.4.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_core__0_5_1",
        url = "https://crates.io/api/v1/crates/rand_core/0.5.1/download",
        type = "tar.gz",
        sha256 = "90bde5296fc891b0cef12a6d03ddccc162ce7b2aff54160af9338f8d40df6d19",
        strip_prefix = "rand_core-0.5.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_core-0.5.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_core__0_6_2",
        url = "https://crates.io/api/v1/crates/rand_core/0.6.2/download",
        type = "tar.gz",
        sha256 = "34cf66eb183df1c5876e2dcf6b13d57340741e8dc255b48e40a26de954d06ae7",
        strip_prefix = "rand_core-0.6.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_core-0.6.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_hc__0_1_0",
        url = "https://crates.io/api/v1/crates/rand_hc/0.1.0/download",
        type = "tar.gz",
        sha256 = "7b40677c7be09ae76218dc623efbf7b18e34bced3f38883af07bb75630a21bc4",
        strip_prefix = "rand_hc-0.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_hc-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_hc__0_2_0",
        url = "https://crates.io/api/v1/crates/rand_hc/0.2.0/download",
        type = "tar.gz",
        sha256 = "ca3129af7b92a17112d59ad498c6f81eaf463253766b90396d39ea7a39d6613c",
        strip_prefix = "rand_hc-0.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_hc-0.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_hc__0_3_0",
        url = "https://crates.io/api/v1/crates/rand_hc/0.3.0/download",
        type = "tar.gz",
        sha256 = "3190ef7066a446f2e7f42e239d161e905420ccab01eb967c9eb27d21b2322a73",
        strip_prefix = "rand_hc-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_hc-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_isaac__0_1_1",
        url = "https://crates.io/api/v1/crates/rand_isaac/0.1.1/download",
        type = "tar.gz",
        sha256 = "ded997c9d5f13925be2a6fd7e66bf1872597f759fd9dd93513dd7e92e5a5ee08",
        strip_prefix = "rand_isaac-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_isaac-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_jitter__0_1_4",
        url = "https://crates.io/api/v1/crates/rand_jitter/0.1.4/download",
        type = "tar.gz",
        sha256 = "1166d5c91dc97b88d1decc3285bb0a99ed84b05cfd0bc2341bdf2d43fc41e39b",
        strip_prefix = "rand_jitter-0.1.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_jitter-0.1.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_os__0_1_3",
        url = "https://crates.io/api/v1/crates/rand_os/0.1.3/download",
        type = "tar.gz",
        sha256 = "7b75f676a1e053fc562eafbb47838d67c84801e38fc1ba459e8f180deabd5071",
        strip_prefix = "rand_os-0.1.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_os-0.1.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_pcg__0_1_2",
        url = "https://crates.io/api/v1/crates/rand_pcg/0.1.2/download",
        type = "tar.gz",
        sha256 = "abf9b09b01790cfe0364f52bf32995ea3c39f4d2dd011eac241d2914146d0b44",
        strip_prefix = "rand_pcg-0.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_pcg-0.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_pcg__0_2_1",
        url = "https://crates.io/api/v1/crates/rand_pcg/0.2.1/download",
        type = "tar.gz",
        sha256 = "16abd0c1b639e9eb4d7c50c0b8100b0d0f849be2349829c740fe8e6eb4816429",
        strip_prefix = "rand_pcg-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_pcg-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rand_xorshift__0_1_1",
        url = "https://crates.io/api/v1/crates/rand_xorshift/0.1.1/download",
        type = "tar.gz",
        sha256 = "cbf7e9e623549b0e21f6e97cf8ecf247c1a8fd2e8a992ae265314300b2455d5c",
        strip_prefix = "rand_xorshift-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rand_xorshift-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rc2__0_5_0",
        url = "https://crates.io/api/v1/crates/rc2/0.5.0/download",
        type = "tar.gz",
        sha256 = "1a067463a4086ea7aab0cc815bbb0c8d473798657643734850bd0c6660846643",
        strip_prefix = "rc2-0.5.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rc2-0.5.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rdrand__0_4_0",
        url = "https://crates.io/api/v1/crates/rdrand/0.4.0/download",
        type = "tar.gz",
        sha256 = "678054eb77286b51581ba43620cc911abf02758c91f93f479767aed0f90458b2",
        strip_prefix = "rdrand-0.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rdrand-0.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__redox_syscall__0_1_57",
        url = "https://crates.io/api/v1/crates/redox_syscall/0.1.57/download",
        type = "tar.gz",
        sha256 = "41cc0f7e4d5d4544e8861606a285bb08d3e70712ccc7d2b84d7c0ccfaf4b05ce",
        strip_prefix = "redox_syscall-0.1.57",
        build_file = Label("//rust_shared/raze/remote:BUILD.redox_syscall-0.1.57.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__redox_syscall__0_2_6",
        url = "https://crates.io/api/v1/crates/redox_syscall/0.2.6/download",
        type = "tar.gz",
        sha256 = "8270314b5ccceb518e7e578952f0b72b88222d02e8f77f5ecf7abbb673539041",
        strip_prefix = "redox_syscall-0.2.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.redox_syscall-0.2.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__regex__1_4_5",
        url = "https://crates.io/api/v1/crates/regex/1.4.5/download",
        type = "tar.gz",
        sha256 = "957056ecddbeba1b26965114e191d2e8589ce74db242b6ea25fc4062427a5c19",
        strip_prefix = "regex-1.4.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.regex-1.4.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__regex_syntax__0_6_23",
        url = "https://crates.io/api/v1/crates/regex-syntax/0.6.23/download",
        type = "tar.gz",
        sha256 = "24d5f089152e60f62d28b835fbff2cd2e8dc0baf1ac13343bef92ab7eed84548",
        strip_prefix = "regex-syntax-0.6.23",
        build_file = Label("//rust_shared/raze/remote:BUILD.regex-syntax-0.6.23.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__remove_dir_all__0_5_3",
        url = "https://crates.io/api/v1/crates/remove_dir_all/0.5.3/download",
        type = "tar.gz",
        sha256 = "3acd125665422973a33ac9d3dd2df85edad0f4ae9b00dafb1a05e43a9f5ef8e7",
        strip_prefix = "remove_dir_all-0.5.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.remove_dir_all-0.5.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__reqwest__0_11_3",
        url = "https://crates.io/api/v1/crates/reqwest/0.11.3/download",
        type = "tar.gz",
        sha256 = "2296f2fac53979e8ccbc4a1136b25dcefd37be9ed7e4a1f6b05a6029c84ff124",
        strip_prefix = "reqwest-0.11.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.reqwest-0.11.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__reqwest__0_9_24",
        url = "https://crates.io/api/v1/crates/reqwest/0.9.24/download",
        type = "tar.gz",
        sha256 = "f88643aea3c1343c804950d7bf983bd2067f5ab59db6d613a08e05572f2714ab",
        strip_prefix = "reqwest-0.9.24",
        build_file = Label("//rust_shared/raze/remote:BUILD.reqwest-0.9.24.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__resolv_conf__0_7_0",
        url = "https://crates.io/api/v1/crates/resolv-conf/0.7.0/download",
        type = "tar.gz",
        sha256 = "52e44394d2086d010551b14b53b1f24e31647570cd1deb0379e2c21b329aba00",
        strip_prefix = "resolv-conf-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.resolv-conf-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ring__0_16_20",
        url = "https://crates.io/api/v1/crates/ring/0.16.20/download",
        type = "tar.gz",
        sha256 = "3053cf52e236a3ed746dfc745aa9cacf1b791d846bdaf412f60a8d7d6e17c8fc",
        strip_prefix = "ring-0.16.20",
        build_file = Label("//rust_shared/raze/remote:BUILD.ring-0.16.20.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustc_demangle__0_1_18",
        url = "https://crates.io/api/v1/crates/rustc-demangle/0.1.18/download",
        type = "tar.gz",
        sha256 = "6e3bad0ee36814ca07d7968269dd4b7ec89ec2da10c4bb613928d3077083c232",
        strip_prefix = "rustc-demangle-0.1.18",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustc-demangle-0.1.18.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustc_version__0_2_3",
        url = "https://crates.io/api/v1/crates/rustc_version/0.2.3/download",
        type = "tar.gz",
        sha256 = "138e3e0acb6c9fb258b19b67cb8abd63c00679d2851805ea151465464fe9030a",
        strip_prefix = "rustc_version-0.2.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustc_version-0.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustls__0_17_0",
        url = "https://crates.io/api/v1/crates/rustls/0.17.0/download",
        type = "tar.gz",
        sha256 = "c0d4a31f5d68413404705d6982529b0e11a9aacd4839d1d6222ee3b8cb4015e1",
        strip_prefix = "rustls-0.17.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustls-0.17.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustls__0_19_0",
        url = "https://crates.io/api/v1/crates/rustls/0.19.0/download",
        type = "tar.gz",
        sha256 = "064fd21ff87c6e87ed4506e68beb42459caa4a0e2eb144932e6776768556980b",
        strip_prefix = "rustls-0.19.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustls-0.19.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustls_connector__0_13_1",
        url = "https://crates.io/api/v1/crates/rustls-connector/0.13.1/download",
        type = "tar.gz",
        sha256 = "4ffaf21b0bac725875490d079bb503cf88684618310c2dff167640fa006217cb",
        strip_prefix = "rustls-connector-0.13.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustls-connector-0.13.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustls_native_certs__0_5_0",
        url = "https://crates.io/api/v1/crates/rustls-native-certs/0.5.0/download",
        type = "tar.gz",
        sha256 = "5a07b7c1885bd8ed3831c289b7870b13ef46fe0e856d288c30d9cc17d75a2092",
        strip_prefix = "rustls-native-certs-0.5.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustls-native-certs-0.5.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__rustversion__1_0_4",
        url = "https://crates.io/api/v1/crates/rustversion/1.0.4/download",
        type = "tar.gz",
        sha256 = "cb5d2a036dc6d2d8fd16fde3498b04306e29bd193bf306a57427019b823d5acd",
        strip_prefix = "rustversion-1.0.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.rustversion-1.0.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ryu__1_0_5",
        url = "https://crates.io/api/v1/crates/ryu/1.0.5/download",
        type = "tar.gz",
        sha256 = "71d301d4193d031abdd79ff7e3dd721168a9572ef3fe51a1517aba235bd8f86e",
        strip_prefix = "ryu-1.0.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.ryu-1.0.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__schannel__0_1_19",
        url = "https://crates.io/api/v1/crates/schannel/0.1.19/download",
        type = "tar.gz",
        sha256 = "8f05ba609c234e60bee0d547fe94a4c7e9da733d1c962cf6e59efa4cd9c8bc75",
        strip_prefix = "schannel-0.1.19",
        build_file = Label("//rust_shared/raze/remote:BUILD.schannel-0.1.19.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__scopeguard__1_1_0",
        url = "https://crates.io/api/v1/crates/scopeguard/1.1.0/download",
        type = "tar.gz",
        sha256 = "d29ab0c6d3fc0ee92fe66e2d99f700eab17a8d57d1c1d3b748380fb20baa78cd",
        strip_prefix = "scopeguard-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.scopeguard-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__sct__0_6_1",
        url = "https://crates.io/api/v1/crates/sct/0.6.1/download",
        type = "tar.gz",
        sha256 = "b362b83898e0e69f38515b82ee15aa80636befe47c3b6d3d89a911e78fc228ce",
        strip_prefix = "sct-0.6.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.sct-0.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__security_framework__2_2_0",
        url = "https://crates.io/api/v1/crates/security-framework/2.2.0/download",
        type = "tar.gz",
        sha256 = "3670b1d2fdf6084d192bc71ead7aabe6c06aa2ea3fbd9cc3ac111fa5c2b1bd84",
        strip_prefix = "security-framework-2.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.security-framework-2.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__security_framework_sys__2_2_0",
        url = "https://crates.io/api/v1/crates/security-framework-sys/2.2.0/download",
        type = "tar.gz",
        sha256 = "3676258fd3cfe2c9a0ec99ce3038798d847ce3e4bb17746373eb9f0f1ac16339",
        strip_prefix = "security-framework-sys-2.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.security-framework-sys-2.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__semver__0_9_0",
        url = "https://crates.io/api/v1/crates/semver/0.9.0/download",
        type = "tar.gz",
        sha256 = "1d7eb9ef2c18661902cc47e535f9bc51b78acd254da71d375c2f6720d9a40403",
        strip_prefix = "semver-0.9.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.semver-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__semver_parser__0_7_0",
        url = "https://crates.io/api/v1/crates/semver-parser/0.7.0/download",
        type = "tar.gz",
        sha256 = "388a1df253eca08550bef6c72392cfe7c30914bf41df5269b68cbd6ff8f570a3",
        strip_prefix = "semver-parser-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.semver-parser-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde__1_0_125",
        url = "https://crates.io/api/v1/crates/serde/1.0.125/download",
        type = "tar.gz",
        sha256 = "558dc50e1a5a5fa7112ca2ce4effcb321b0300c0d4ccf0776a9f60cd89031171",
        strip_prefix = "serde-1.0.125",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde-1.0.125.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_derive__1_0_125",
        url = "https://crates.io/api/v1/crates/serde_derive/1.0.125/download",
        type = "tar.gz",
        sha256 = "b093b7a2bb58203b5da3056c05b4ec1fed827dcfdb37347a8841695263b3d06d",
        strip_prefix = "serde_derive-1.0.125",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde_derive-1.0.125.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_json__1_0_64",
        url = "https://crates.io/api/v1/crates/serde_json/1.0.64/download",
        type = "tar.gz",
        sha256 = "799e97dc9fdae36a5c8b8f2cae9ce2ee9fdce2058c57a93e6099d919fd982f79",
        strip_prefix = "serde_json-1.0.64",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde_json-1.0.64.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_urlencoded__0_5_5",
        url = "https://crates.io/api/v1/crates/serde_urlencoded/0.5.5/download",
        type = "tar.gz",
        sha256 = "642dd69105886af2efd227f75a520ec9b44a820d65bc133a9131f7d229fd165a",
        strip_prefix = "serde_urlencoded-0.5.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde_urlencoded-0.5.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_urlencoded__0_7_0",
        url = "https://crates.io/api/v1/crates/serde_urlencoded/0.7.0/download",
        type = "tar.gz",
        sha256 = "edfa57a7f8d9c1d260a549e7224100f6c43d43f9103e06dd8b4095a9b2b43ce9",
        strip_prefix = "serde_urlencoded-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde_urlencoded-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_with__1_8_0",
        url = "https://crates.io/api/v1/crates/serde_with/1.8.0/download",
        type = "tar.gz",
        sha256 = "e557c650adfb38b32a5aec07082053253c703bc3cec654b27a5dbcf61995bb9b",
        strip_prefix = "serde_with-1.8.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde_with-1.8.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__serde_with_macros__1_4_1",
        url = "https://crates.io/api/v1/crates/serde_with_macros/1.4.1/download",
        type = "tar.gz",
        sha256 = "e48b35457e9d855d3dc05ef32a73e0df1e2c0fd72c38796a4ee909160c8eeec2",
        strip_prefix = "serde_with_macros-1.4.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.serde_with_macros-1.4.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__sha_1__0_8_2",
        url = "https://crates.io/api/v1/crates/sha-1/0.8.2/download",
        type = "tar.gz",
        sha256 = "f7d94d0bede923b3cea61f3f1ff57ff8cdfd77b400fb8f9998949e0cf04163df",
        strip_prefix = "sha-1-0.8.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.sha-1-0.8.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__sha_1__0_9_4",
        url = "https://crates.io/api/v1/crates/sha-1/0.9.4/download",
        type = "tar.gz",
        sha256 = "dfebf75d25bd900fd1e7d11501efab59bc846dbc76196839663e6637bba9f25f",
        strip_prefix = "sha-1-0.9.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.sha-1-0.9.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__sha2__0_8_2",
        url = "https://crates.io/api/v1/crates/sha2/0.8.2/download",
        type = "tar.gz",
        sha256 = "a256f46ea78a0c0d9ff00077504903ac881a1dafdc20da66545699e7776b3e69",
        strip_prefix = "sha2-0.8.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.sha2-0.8.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__sha2__0_9_3",
        url = "https://crates.io/api/v1/crates/sha2/0.9.3/download",
        type = "tar.gz",
        sha256 = "fa827a14b29ab7f44778d14a88d3cb76e949c45083f7dbfa507d0cb699dc12de",
        strip_prefix = "sha2-0.9.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.sha2-0.9.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__slab__0_4_2",
        url = "https://crates.io/api/v1/crates/slab/0.4.2/download",
        type = "tar.gz",
        sha256 = "c111b5bd5695e56cffe5129854aa230b39c93a305372fdbb2668ca2394eea9f8",
        strip_prefix = "slab-0.4.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.slab-0.4.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smallvec__0_6_14",
        url = "https://crates.io/api/v1/crates/smallvec/0.6.14/download",
        type = "tar.gz",
        sha256 = "b97fcaeba89edba30f044a10c6a3cc39df9c3f17d7cd829dd1446cab35f890e0",
        strip_prefix = "smallvec-0.6.14",
        build_file = Label("//rust_shared/raze/remote:BUILD.smallvec-0.6.14.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smallvec__1_6_1",
        url = "https://crates.io/api/v1/crates/smallvec/1.6.1/download",
        type = "tar.gz",
        sha256 = "fe0f37c9e8f3c5a4a66ad655a93c74daac4ad00c441533bf5c6e7990bb42604e",
        strip_prefix = "smallvec-1.6.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.smallvec-1.6.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smd_macro__0_0_7",
        url = "https://crates.io/api/v1/crates/smd_macro/0.0.7/download",
        type = "tar.gz",
        sha256 = "5af3a85611bc0d4138a8bfdd7700dd5a16a1aed781f4f7326d2e873ee7048602",
        strip_prefix = "smd_macro-0.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.smd_macro-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smithy__0_0_7",
        url = "https://crates.io/api/v1/crates/smithy/0.0.7/download",
        type = "tar.gz",
        sha256 = "00b3c43789dc877cfe5707411d4aefb56ab986b53d8a520496f0fda88de08aa9",
        strip_prefix = "smithy-0.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.smithy-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smithy_core__0_0_7",
        url = "https://crates.io/api/v1/crates/smithy_core/0.0.7/download",
        type = "tar.gz",
        sha256 = "4ea5c9f0aa281ce00937cf6d96510afe8c118e595527be14e7812b75608e937b",
        strip_prefix = "smithy_core-0.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.smithy_core-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__smithy_types__0_0_7",
        url = "https://crates.io/api/v1/crates/smithy_types/0.0.7/download",
        type = "tar.gz",
        sha256 = "6e99122db47faeea05dc2377ad7b517de1268ecdc9aa82d1318101d06104d783",
        strip_prefix = "smithy_types-0.0.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.smithy_types-0.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__socket2__0_3_19",
        url = "https://crates.io/api/v1/crates/socket2/0.3.19/download",
        type = "tar.gz",
        sha256 = "122e570113d28d773067fab24266b66753f6ea915758651696b6e35e49f88d6e",
        strip_prefix = "socket2-0.3.19",
        build_file = Label("//rust_shared/raze/remote:BUILD.socket2-0.3.19.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__socket2__0_4_0",
        url = "https://crates.io/api/v1/crates/socket2/0.4.0/download",
        type = "tar.gz",
        sha256 = "9e3dfc207c526015c632472a77be09cf1b6e46866581aecae5cc38fb4235dea2",
        strip_prefix = "socket2-0.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.socket2-0.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__sonic_channel__0_4_0",
        url = "https://crates.io/api/v1/crates/sonic-channel/0.4.0/download",
        type = "tar.gz",
        sha256 = "085ed6444c6801c635066e062f64f43608697de2d62ca736376abb2c48f3d7da",
        strip_prefix = "sonic-channel-0.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.sonic-channel-0.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__spin__0_5_2",
        url = "https://crates.io/api/v1/crates/spin/0.5.2/download",
        type = "tar.gz",
        sha256 = "6e63cff320ae2c57904679ba7cb63280a3dc4613885beafb148ee7bf9aa9042d",
        strip_prefix = "spin-0.5.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.spin-0.5.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__static_assertions__1_1_0",
        url = "https://crates.io/api/v1/crates/static_assertions/1.1.0/download",
        type = "tar.gz",
        sha256 = "a2eb9349b6444b326872e140eb1cf5e7c522154d69e7a0ffb0fb81c06b37543f",
        strip_prefix = "static_assertions-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.static_assertions-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__string__0_2_1",
        url = "https://crates.io/api/v1/crates/string/0.2.1/download",
        type = "tar.gz",
        sha256 = "d24114bfcceb867ca7f71a0d3fe45d45619ec47a6fbfa98cb14e14250bfa5d6d",
        strip_prefix = "string-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.string-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__stringprep__0_1_2",
        url = "https://crates.io/api/v1/crates/stringprep/0.1.2/download",
        type = "tar.gz",
        sha256 = "8ee348cb74b87454fff4b551cbf727025810a004f88aeacae7f85b87f4e9a1c1",
        strip_prefix = "stringprep-0.1.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.stringprep-0.1.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__strsim__0_10_0",
        url = "https://crates.io/api/v1/crates/strsim/0.10.0/download",
        type = "tar.gz",
        sha256 = "73473c0e59e6d5812c5dfe2a064a6444949f089e20eec9a2e5506596494e4623",
        strip_prefix = "strsim-0.10.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.strsim-0.10.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__subtle__1_0_0",
        url = "https://crates.io/api/v1/crates/subtle/1.0.0/download",
        type = "tar.gz",
        sha256 = "2d67a5a62ba6e01cb2192ff309324cb4875d0c451d55fe2319433abe7a05a8ee",
        strip_prefix = "subtle-1.0.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.subtle-1.0.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__subtle__2_4_0",
        url = "https://crates.io/api/v1/crates/subtle/2.4.0/download",
        type = "tar.gz",
        sha256 = "1e81da0851ada1f3e9d4312c704aa4f8806f0f9d69faaf8df2f3464b4a9437c2",
        strip_prefix = "subtle-2.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.subtle-2.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__syn__1_0_69",
        url = "https://crates.io/api/v1/crates/syn/1.0.69/download",
        type = "tar.gz",
        sha256 = "48fe99c6bd8b1cc636890bcc071842de909d902c81ac7dab53ba33c421ab8ffb",
        strip_prefix = "syn-1.0.69",
        build_file = Label("//rust_shared/raze/remote:BUILD.syn-1.0.69.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__synstructure__0_12_4",
        url = "https://crates.io/api/v1/crates/synstructure/0.12.4/download",
        type = "tar.gz",
        sha256 = "b834f2d66f734cb897113e34aaff2f1ab4719ca946f9a7358dba8f8064148701",
        strip_prefix = "synstructure-0.12.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.synstructure-0.12.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__take_mut__0_2_2",
        url = "https://crates.io/api/v1/crates/take_mut/0.2.2/download",
        type = "tar.gz",
        sha256 = "f764005d11ee5f36500a149ace24e00e3da98b0158b3e2d53a7495660d3f4d60",
        strip_prefix = "take_mut-0.2.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.take_mut-0.2.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tap__1_0_1",
        url = "https://crates.io/api/v1/crates/tap/1.0.1/download",
        type = "tar.gz",
        sha256 = "55937e1799185b12863d447f42597ed69d9928686b8d88a1df17376a097d8369",
        strip_prefix = "tap-1.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.tap-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tcp_stream__0_20_6",
        url = "https://crates.io/api/v1/crates/tcp-stream/0.20.6/download",
        type = "tar.gz",
        sha256 = "04c180af0da0f0a75ca080465175892fcdaa750076f125cb953127721e676413",
        strip_prefix = "tcp-stream-0.20.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.tcp-stream-0.20.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tempfile__3_2_0",
        url = "https://crates.io/api/v1/crates/tempfile/3.2.0/download",
        type = "tar.gz",
        sha256 = "dac1c663cfc93810f88aed9b8941d48cabf856a1b111c29a40439018d870eb22",
        strip_prefix = "tempfile-3.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.tempfile-3.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__thiserror__1_0_24",
        url = "https://crates.io/api/v1/crates/thiserror/1.0.24/download",
        type = "tar.gz",
        sha256 = "e0f4a65597094d4483ddaed134f409b2cb7c1beccf25201a9f73c719254fa98e",
        strip_prefix = "thiserror-1.0.24",
        build_file = Label("//rust_shared/raze/remote:BUILD.thiserror-1.0.24.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__thiserror_impl__1_0_24",
        url = "https://crates.io/api/v1/crates/thiserror-impl/1.0.24/download",
        type = "tar.gz",
        sha256 = "7765189610d8241a44529806d6fd1f2e0a08734313a35d5b3a556f92b381f3c0",
        strip_prefix = "thiserror-impl-1.0.24",
        build_file = Label("//rust_shared/raze/remote:BUILD.thiserror-impl-1.0.24.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__time__0_1_44",
        url = "https://crates.io/api/v1/crates/time/0.1.44/download",
        type = "tar.gz",
        sha256 = "6db9e6914ab8b1ae1c260a4ae7a49b6c5611b40328a735b21862567685e73255",
        strip_prefix = "time-0.1.44",
        build_file = Label("//rust_shared/raze/remote:BUILD.time-0.1.44.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio__0_1_22",
        url = "https://crates.io/api/v1/crates/tokio/0.1.22/download",
        type = "tar.gz",
        sha256 = "5a09c0b5bb588872ab2f09afa13ee6e9dac11e10a0ec9e8e3ba39a5a5d530af6",
        strip_prefix = "tokio-0.1.22",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-0.1.22.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio__0_2_25",
        url = "https://crates.io/api/v1/crates/tokio/0.2.25/download",
        type = "tar.gz",
        sha256 = "6703a273949a90131b290be1fe7b039d0fc884aa1935860dfcbe056f28cd8092",
        strip_prefix = "tokio-0.2.25",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-0.2.25.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio__1_5_0",
        url = "https://crates.io/api/v1/crates/tokio/1.5.0/download",
        type = "tar.gz",
        sha256 = "83f0c8e7c0addab50b663055baf787d0af7f413a46e6e7fb9559a4e4db7137a5",
        strip_prefix = "tokio-1.5.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-1.5.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_buf__0_1_1",
        url = "https://crates.io/api/v1/crates/tokio-buf/0.1.1/download",
        type = "tar.gz",
        sha256 = "8fb220f46c53859a4b7ec083e41dec9778ff0b1851c0942b211edb89e0ccdc46",
        strip_prefix = "tokio-buf-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-buf-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_current_thread__0_1_7",
        url = "https://crates.io/api/v1/crates/tokio-current-thread/0.1.7/download",
        type = "tar.gz",
        sha256 = "b1de0e32a83f131e002238d7ccde18211c0a5397f60cbfffcb112868c2e0e20e",
        strip_prefix = "tokio-current-thread-0.1.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-current-thread-0.1.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_executor__0_1_10",
        url = "https://crates.io/api/v1/crates/tokio-executor/0.1.10/download",
        type = "tar.gz",
        sha256 = "fb2d1b8f4548dbf5e1f7818512e9c406860678f29c300cdf0ebac72d1a3a1671",
        strip_prefix = "tokio-executor-0.1.10",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-executor-0.1.10.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_io__0_1_13",
        url = "https://crates.io/api/v1/crates/tokio-io/0.1.13/download",
        type = "tar.gz",
        sha256 = "57fc868aae093479e3131e3d165c93b1c7474109d13c90ec0dda2a1bbfff0674",
        strip_prefix = "tokio-io-0.1.13",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-io-0.1.13.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_macros__0_2_6",
        url = "https://crates.io/api/v1/crates/tokio-macros/0.2.6/download",
        type = "tar.gz",
        sha256 = "e44da00bfc73a25f814cd8d7e57a68a5c31b74b3152a0a1d1f590c97ed06265a",
        strip_prefix = "tokio-macros-0.2.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-macros-0.2.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_macros__1_1_0",
        url = "https://crates.io/api/v1/crates/tokio-macros/1.1.0/download",
        type = "tar.gz",
        sha256 = "caf7b11a536f46a809a8a9f0bb4237020f70ecbf115b842360afb127ea2fda57",
        strip_prefix = "tokio-macros-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-macros-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_native_tls__0_3_0",
        url = "https://crates.io/api/v1/crates/tokio-native-tls/0.3.0/download",
        type = "tar.gz",
        sha256 = "f7d995660bd2b7f8c1568414c1126076c13fbb725c40112dc0120b78eb9b717b",
        strip_prefix = "tokio-native-tls-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-native-tls-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_reactor__0_1_12",
        url = "https://crates.io/api/v1/crates/tokio-reactor/0.1.12/download",
        type = "tar.gz",
        sha256 = "09bc590ec4ba8ba87652da2068d150dcada2cfa2e07faae270a5e0409aa51351",
        strip_prefix = "tokio-reactor-0.1.12",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-reactor-0.1.12.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_rustls__0_13_1",
        url = "https://crates.io/api/v1/crates/tokio-rustls/0.13.1/download",
        type = "tar.gz",
        sha256 = "15cb62a0d2770787abc96e99c1cd98fcf17f94959f3af63ca85bdfb203f051b4",
        strip_prefix = "tokio-rustls-0.13.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-rustls-0.13.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_stream__0_1_5",
        url = "https://crates.io/api/v1/crates/tokio-stream/0.1.5/download",
        type = "tar.gz",
        sha256 = "e177a5d8c3bf36de9ebe6d58537d8879e964332f93fb3339e43f618c81361af0",
        strip_prefix = "tokio-stream-0.1.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-stream-0.1.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_sync__0_1_8",
        url = "https://crates.io/api/v1/crates/tokio-sync/0.1.8/download",
        type = "tar.gz",
        sha256 = "edfe50152bc8164fcc456dab7891fa9bf8beaf01c5ee7e1dd43a397c3cf87dee",
        strip_prefix = "tokio-sync-0.1.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-sync-0.1.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_tcp__0_1_4",
        url = "https://crates.io/api/v1/crates/tokio-tcp/0.1.4/download",
        type = "tar.gz",
        sha256 = "98df18ed66e3b72e742f185882a9e201892407957e45fbff8da17ae7a7c51f72",
        strip_prefix = "tokio-tcp-0.1.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-tcp-0.1.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_threadpool__0_1_18",
        url = "https://crates.io/api/v1/crates/tokio-threadpool/0.1.18/download",
        type = "tar.gz",
        sha256 = "df720b6581784c118f0eb4310796b12b1d242a7eb95f716a8367855325c25f89",
        strip_prefix = "tokio-threadpool-0.1.18",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-threadpool-0.1.18.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_timer__0_2_13",
        url = "https://crates.io/api/v1/crates/tokio-timer/0.2.13/download",
        type = "tar.gz",
        sha256 = "93044f2d313c95ff1cb7809ce9a7a05735b012288a888b62d4434fd58c94f296",
        strip_prefix = "tokio-timer-0.2.13",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-timer-0.2.13.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tokio_util__0_6_6",
        url = "https://crates.io/api/v1/crates/tokio-util/0.6.6/download",
        type = "tar.gz",
        sha256 = "940a12c99365c31ea8dd9ba04ec1be183ffe4920102bb7122c2f515437601e8e",
        strip_prefix = "tokio-util-0.6.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.tokio-util-0.6.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tonic__0_4_2",
        url = "https://crates.io/api/v1/crates/tonic/0.4.2/download",
        type = "tar.gz",
        sha256 = "556dc31b450f45d18279cfc3d2519280273f460d5387e6b7b24503e65d206f8b",
        strip_prefix = "tonic-0.4.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.tonic-0.4.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tower__0_4_6",
        url = "https://crates.io/api/v1/crates/tower/0.4.6/download",
        type = "tar.gz",
        sha256 = "f715efe02c0862926eb463e49368d38ddb119383475686178e32e26d15d06a66",
        strip_prefix = "tower-0.4.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.tower-0.4.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tower_layer__0_3_1",
        url = "https://crates.io/api/v1/crates/tower-layer/0.3.1/download",
        type = "tar.gz",
        sha256 = "343bc9466d3fe6b0f960ef45960509f84480bf4fd96f92901afe7ff3df9d3a62",
        strip_prefix = "tower-layer-0.3.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.tower-layer-0.3.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tower_service__0_3_1",
        url = "https://crates.io/api/v1/crates/tower-service/0.3.1/download",
        type = "tar.gz",
        sha256 = "360dfd1d6d30e05fda32ace2c8c70e9c0a9da713275777f5a4dbb8a1893930c6",
        strip_prefix = "tower-service-0.3.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.tower-service-0.3.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tracing__0_1_25",
        url = "https://crates.io/api/v1/crates/tracing/0.1.25/download",
        type = "tar.gz",
        sha256 = "01ebdc2bb4498ab1ab5f5b73c5803825e60199229ccba0698170e3be0e7f959f",
        strip_prefix = "tracing-0.1.25",
        build_file = Label("//rust_shared/raze/remote:BUILD.tracing-0.1.25.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tracing_attributes__0_1_15",
        url = "https://crates.io/api/v1/crates/tracing-attributes/0.1.15/download",
        type = "tar.gz",
        sha256 = "c42e6fa53307c8a17e4ccd4dc81cf5ec38db9209f59b222210375b54ee40d1e2",
        strip_prefix = "tracing-attributes-0.1.15",
        build_file = Label("//rust_shared/raze/remote:BUILD.tracing-attributes-0.1.15.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tracing_core__0_1_17",
        url = "https://crates.io/api/v1/crates/tracing-core/0.1.17/download",
        type = "tar.gz",
        sha256 = "f50de3927f93d202783f4513cda820ab47ef17f624b03c096e86ef00c67e6b5f",
        strip_prefix = "tracing-core-0.1.17",
        build_file = Label("//rust_shared/raze/remote:BUILD.tracing-core-0.1.17.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__tracing_futures__0_2_5",
        url = "https://crates.io/api/v1/crates/tracing-futures/0.2.5/download",
        type = "tar.gz",
        sha256 = "97d095ae15e245a057c8e8451bab9b3ee1e1f68e9ba2b4fbc18d0ac5237835f2",
        strip_prefix = "tracing-futures-0.2.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.tracing-futures-0.2.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__treeline__0_1_0",
        url = "https://crates.io/api/v1/crates/treeline/0.1.0/download",
        type = "tar.gz",
        sha256 = "a7f741b240f1a48843f9b8e0444fb55fb2a4ff67293b50a9179dfd5ea67f8d41",
        strip_prefix = "treeline-0.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.treeline-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__trust_dns_proto__0_19_7",
        url = "https://crates.io/api/v1/crates/trust-dns-proto/0.19.7/download",
        type = "tar.gz",
        sha256 = "1cad71a0c0d68ab9941d2fb6e82f8fb2e86d9945b94e1661dd0aaea2b88215a9",
        strip_prefix = "trust-dns-proto-0.19.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.trust-dns-proto-0.19.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__trust_dns_resolver__0_19_7",
        url = "https://crates.io/api/v1/crates/trust-dns-resolver/0.19.7/download",
        type = "tar.gz",
        sha256 = "710f593b371175db53a26d0b38ed2978fafb9e9e8d3868b1acd753ea18df0ceb",
        strip_prefix = "trust-dns-resolver-0.19.7",
        build_file = Label("//rust_shared/raze/remote:BUILD.trust-dns-resolver-0.19.7.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__try_lock__0_2_3",
        url = "https://crates.io/api/v1/crates/try-lock/0.2.3/download",
        type = "tar.gz",
        sha256 = "59547bce71d9c38b83d9c0e92b6066c4253371f15005def0c30d9657f50c7642",
        strip_prefix = "try-lock-0.2.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.try-lock-0.2.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__try_from__0_3_2",
        url = "https://crates.io/api/v1/crates/try_from/0.3.2/download",
        type = "tar.gz",
        sha256 = "283d3b89e1368717881a9d51dad843cc435380d8109c9e47d38780a324698d8b",
        strip_prefix = "try_from-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.try_from-0.3.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__typed_builder__0_4_1",
        url = "https://crates.io/api/v1/crates/typed-builder/0.4.1/download",
        type = "tar.gz",
        sha256 = "dfc955f27acc7a547f328f52f4a5a568986a31efec2fc6de865279f3995787b9",
        strip_prefix = "typed-builder-0.4.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.typed-builder-0.4.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__typenum__1_13_0",
        url = "https://crates.io/api/v1/crates/typenum/1.13.0/download",
        type = "tar.gz",
        sha256 = "879f6906492a7cd215bfa4cf595b600146ccfac0c79bcbd1f3000162af5e8b06",
        strip_prefix = "typenum-1.13.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.typenum-1.13.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicase__2_6_0",
        url = "https://crates.io/api/v1/crates/unicase/2.6.0/download",
        type = "tar.gz",
        sha256 = "50f37be617794602aabbeee0be4f259dc1778fabe05e2d67ee8f79326d5cb4f6",
        strip_prefix = "unicase-2.6.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.unicase-2.6.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_bidi__0_3_5",
        url = "https://crates.io/api/v1/crates/unicode-bidi/0.3.5/download",
        type = "tar.gz",
        sha256 = "eeb8be209bb1c96b7c177c7420d26e04eccacb0eeae6b980e35fcb74678107e0",
        strip_prefix = "unicode-bidi-0.3.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.unicode-bidi-0.3.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_normalization__0_1_9",
        url = "https://crates.io/api/v1/crates/unicode-normalization/0.1.9/download",
        type = "tar.gz",
        sha256 = "09c8070a9942f5e7cfccd93f490fdebd230ee3c3c9f107cb25bad5351ef671cf",
        strip_prefix = "unicode-normalization-0.1.9",
        build_file = Label("//rust_shared/raze/remote:BUILD.unicode-normalization-0.1.9.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_segmentation__1_7_1",
        url = "https://crates.io/api/v1/crates/unicode-segmentation/1.7.1/download",
        type = "tar.gz",
        sha256 = "bb0d2e7be6ae3a5fa87eed5fb451aff96f2573d2694942e40543ae0bbe19c796",
        strip_prefix = "unicode-segmentation-1.7.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.unicode-segmentation-1.7.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_xid__0_1_0",
        url = "https://crates.io/api/v1/crates/unicode-xid/0.1.0/download",
        type = "tar.gz",
        sha256 = "fc72304796d0818e357ead4e000d19c9c174ab23dc11093ac919054d20a6a7fc",
        strip_prefix = "unicode-xid-0.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.unicode-xid-0.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__unicode_xid__0_2_1",
        url = "https://crates.io/api/v1/crates/unicode-xid/0.2.1/download",
        type = "tar.gz",
        sha256 = "f7fe0bb3479651439c9112f72b6c505038574c9fbb575ed1bf3b797fa39dd564",
        strip_prefix = "unicode-xid-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.unicode-xid-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__untrusted__0_7_1",
        url = "https://crates.io/api/v1/crates/untrusted/0.7.1/download",
        type = "tar.gz",
        sha256 = "a156c684c91ea7d62626509bce3cb4e1d9ed5c4d978f7b4352658f96a4c26b4a",
        strip_prefix = "untrusted-0.7.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.untrusted-0.7.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__url__1_7_2",
        url = "https://crates.io/api/v1/crates/url/1.7.2/download",
        type = "tar.gz",
        sha256 = "dd4e7c0d531266369519a4aa4f399d748bd37043b00bde1e4ff1f60a120b355a",
        strip_prefix = "url-1.7.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.url-1.7.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__url__2_2_1",
        url = "https://crates.io/api/v1/crates/url/2.2.1/download",
        type = "tar.gz",
        sha256 = "9ccd964113622c8e9322cfac19eb1004a07e636c545f325da085d5cdde6f1f8b",
        strip_prefix = "url-2.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.url-2.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__uuid__0_7_4",
        url = "https://crates.io/api/v1/crates/uuid/0.7.4/download",
        type = "tar.gz",
        sha256 = "90dbc611eb48397705a6b0f6e917da23ae517e4d127123d2cf7674206627d32a",
        strip_prefix = "uuid-0.7.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.uuid-0.7.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__uuid__0_8_2",
        url = "https://crates.io/api/v1/crates/uuid/0.8.2/download",
        type = "tar.gz",
        sha256 = "bc5cf98d8186244414c848017f0e2676b3fcb46807f6668a97dfe67359a3c4b7",
        strip_prefix = "uuid-0.8.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.uuid-0.8.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__value_bag__1_0_0_alpha_6",
        url = "https://crates.io/api/v1/crates/value-bag/1.0.0-alpha.6/download",
        type = "tar.gz",
        sha256 = "6b676010e055c99033117c2343b33a40a30b91fecd6c49055ac9cd2d6c305ab1",
        strip_prefix = "value-bag-1.0.0-alpha.6",
        build_file = Label("//rust_shared/raze/remote:BUILD.value-bag-1.0.0-alpha.6.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__vcpkg__0_2_11",
        url = "https://crates.io/api/v1/crates/vcpkg/0.2.11/download",
        type = "tar.gz",
        sha256 = "b00bca6106a5e23f3eee943593759b7fcddb00554332e856d990c893966879fb",
        strip_prefix = "vcpkg-0.2.11",
        build_file = Label("//rust_shared/raze/remote:BUILD.vcpkg-0.2.11.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__vec_arena__1_1_0",
        url = "https://crates.io/api/v1/crates/vec-arena/1.1.0/download",
        type = "tar.gz",
        sha256 = "34b2f665b594b07095e3ac3f718e13c2197143416fae4c5706cffb7b1af8d7f1",
        strip_prefix = "vec-arena-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.vec-arena-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__version_check__0_1_5",
        url = "https://crates.io/api/v1/crates/version_check/0.1.5/download",
        type = "tar.gz",
        sha256 = "914b1a6776c4c929a602fafd8bc742e06365d4bcbe48c30f9cca5824f70dc9dd",
        strip_prefix = "version_check-0.1.5",
        build_file = Label("//rust_shared/raze/remote:BUILD.version_check-0.1.5.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__version_check__0_9_3",
        url = "https://crates.io/api/v1/crates/version_check/0.9.3/download",
        type = "tar.gz",
        sha256 = "5fecdca9a5291cc2b8dcf7dc02453fee791a280f3743cb0905f8822ae463b3fe",
        strip_prefix = "version_check-0.9.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.version_check-0.9.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__waker_fn__1_1_0",
        url = "https://crates.io/api/v1/crates/waker-fn/1.1.0/download",
        type = "tar.gz",
        sha256 = "9d5b2c62b4012a3e1eca5a7e077d13b3bf498c4073e33ccd58626607748ceeca",
        strip_prefix = "waker-fn-1.1.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.waker-fn-1.1.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__want__0_2_0",
        url = "https://crates.io/api/v1/crates/want/0.2.0/download",
        type = "tar.gz",
        sha256 = "b6395efa4784b027708f7451087e647ec73cc74f5d9bc2e418404248d679a230",
        strip_prefix = "want-0.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.want-0.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__want__0_3_0",
        url = "https://crates.io/api/v1/crates/want/0.3.0/download",
        type = "tar.gz",
        sha256 = "1ce8a968cb1cd110d136ff8b819a556d6fb6d919363c61534f6860c7eb172ba0",
        strip_prefix = "want-0.3.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.want-0.3.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasi__0_10_0_wasi_snapshot_preview1",
        url = "https://crates.io/api/v1/crates/wasi/0.10.0+wasi-snapshot-preview1/download",
        type = "tar.gz",
        sha256 = "1a143597ca7c7793eff794def352d41792a93c481eb1042423ff7ff72ba2c31f",
        strip_prefix = "wasi-0.10.0+wasi-snapshot-preview1",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasi-0.10.0+wasi-snapshot-preview1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasi__0_9_0_wasi_snapshot_preview1",
        url = "https://crates.io/api/v1/crates/wasi/0.9.0+wasi-snapshot-preview1/download",
        type = "tar.gz",
        sha256 = "cccddf32554fecc6acb585f82a32a72e28b48f8c4c1883ddfeeeaa96f7d8e519",
        strip_prefix = "wasi-0.9.0+wasi-snapshot-preview1",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasi-0.9.0+wasi-snapshot-preview1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen/0.2.73/download",
        type = "tar.gz",
        sha256 = "83240549659d187488f91f33c0f8547cbfef0b2088bc470c116d1d260ef623d9",
        strip_prefix = "wasm-bindgen-0.2.73",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_backend__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-backend/0.2.73/download",
        type = "tar.gz",
        sha256 = "ae70622411ca953215ca6d06d3ebeb1e915f0f6613e3b495122878d7ebec7dae",
        strip_prefix = "wasm-bindgen-backend-0.2.73",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-backend-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_futures__0_3_27",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-futures/0.3.27/download",
        type = "tar.gz",
        sha256 = "83420b37346c311b9ed822af41ec2e82839bfe99867ec6c54e2da43b7538771c",
        strip_prefix = "wasm-bindgen-futures-0.3.27",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-futures-0.3.27.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_futures__0_4_23",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-futures/0.4.23/download",
        type = "tar.gz",
        sha256 = "81b8b767af23de6ac18bf2168b690bed2902743ddf0fb39252e36f9e2bfc63ea",
        strip_prefix = "wasm-bindgen-futures-0.4.23",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-futures-0.4.23.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_macro__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-macro/0.2.73/download",
        type = "tar.gz",
        sha256 = "3e734d91443f177bfdb41969de821e15c516931c3c3db3d318fa1b68975d0f6f",
        strip_prefix = "wasm-bindgen-macro-0.2.73",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-macro-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_macro_support__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-macro-support/0.2.73/download",
        type = "tar.gz",
        sha256 = "d53739ff08c8a68b0fdbcd54c372b8ab800b1449ab3c9d706503bc7dd1621b2c",
        strip_prefix = "wasm-bindgen-macro-support-0.2.73",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-macro-support-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wasm_bindgen_shared__0_2_73",
        url = "https://crates.io/api/v1/crates/wasm-bindgen-shared/0.2.73/download",
        type = "tar.gz",
        sha256 = "d9a543ae66aa233d14bb765ed9af4a33e81b8b58d1584cf1b47ff8cd0b9e4489",
        strip_prefix = "wasm-bindgen-shared-0.2.73",
        build_file = Label("//rust_shared/raze/remote:BUILD.wasm-bindgen-shared-0.2.73.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__web_sys__0_3_50",
        url = "https://crates.io/api/v1/crates/web-sys/0.3.50/download",
        type = "tar.gz",
        sha256 = "a905d57e488fec8861446d3393670fb50d27a262344013181c2cdf9fff5481be",
        strip_prefix = "web-sys-0.3.50",
        build_file = Label("//rust_shared/raze/remote:BUILD.web-sys-0.3.50.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__webpki__0_21_4",
        url = "https://crates.io/api/v1/crates/webpki/0.21.4/download",
        type = "tar.gz",
        sha256 = "b8e38c0608262c46d4a56202ebabdeb094cef7e560ca7a226c6bf055188aa4ea",
        strip_prefix = "webpki-0.21.4",
        build_file = Label("//rust_shared/raze/remote:BUILD.webpki-0.21.4.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__webpki_roots__0_18_0",
        url = "https://crates.io/api/v1/crates/webpki-roots/0.18.0/download",
        type = "tar.gz",
        sha256 = "91cd5736df7f12a964a5067a12c62fa38e1bd8080aff1f80bc29be7c80d19ab4",
        strip_prefix = "webpki-roots-0.18.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.webpki-roots-0.18.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wepoll_sys__3_0_1",
        url = "https://crates.io/api/v1/crates/wepoll-sys/3.0.1/download",
        type = "tar.gz",
        sha256 = "0fcb14dea929042224824779fbc82d9fab8d2e6d3cbc0ac404de8edf489e77ff",
        strip_prefix = "wepoll-sys-3.0.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.wepoll-sys-3.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__widestring__0_4_3",
        url = "https://crates.io/api/v1/crates/widestring/0.4.3/download",
        type = "tar.gz",
        sha256 = "c168940144dd21fd8046987c16a46a33d5fc84eec29ef9dcddc2ac9e31526b7c",
        strip_prefix = "widestring-0.4.3",
        build_file = Label("//rust_shared/raze/remote:BUILD.widestring-0.4.3.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winapi__0_2_8",
        url = "https://crates.io/api/v1/crates/winapi/0.2.8/download",
        type = "tar.gz",
        sha256 = "167dc9d6949a9b857f3451275e911c3f44255842c1f7a76f33c55103a909087a",
        strip_prefix = "winapi-0.2.8",
        build_file = Label("//rust_shared/raze/remote:BUILD.winapi-0.2.8.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winapi__0_3_9",
        url = "https://crates.io/api/v1/crates/winapi/0.3.9/download",
        type = "tar.gz",
        sha256 = "5c839a674fcd7a98952e593242ea400abe93992746761e38641405d28b00f419",
        strip_prefix = "winapi-0.3.9",
        build_file = Label("//rust_shared/raze/remote:BUILD.winapi-0.3.9.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winapi_build__0_1_1",
        url = "https://crates.io/api/v1/crates/winapi-build/0.1.1/download",
        type = "tar.gz",
        sha256 = "2d315eee3b34aca4797b2da6b13ed88266e6d612562a0c46390af8299fc699bc",
        strip_prefix = "winapi-build-0.1.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.winapi-build-0.1.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winapi_i686_pc_windows_gnu__0_4_0",
        url = "https://crates.io/api/v1/crates/winapi-i686-pc-windows-gnu/0.4.0/download",
        type = "tar.gz",
        sha256 = "ac3b87c63620426dd9b991e5ce0329eff545bccbbb34f3be09ff6fb6ab51b7b6",
        strip_prefix = "winapi-i686-pc-windows-gnu-0.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.winapi-i686-pc-windows-gnu-0.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winapi_x86_64_pc_windows_gnu__0_4_0",
        url = "https://crates.io/api/v1/crates/winapi-x86_64-pc-windows-gnu/0.4.0/download",
        type = "tar.gz",
        sha256 = "712e227841d057c1ee1cd2fb22fa7e5a5461ae8e48fa2ca79ec42cfc1931183f",
        strip_prefix = "winapi-x86_64-pc-windows-gnu-0.4.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.winapi-x86_64-pc-windows-gnu-0.4.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winreg__0_6_2",
        url = "https://crates.io/api/v1/crates/winreg/0.6.2/download",
        type = "tar.gz",
        sha256 = "b2986deb581c4fe11b621998a5e53361efe6b48a151178d0cd9eeffa4dc6acc9",
        strip_prefix = "winreg-0.6.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.winreg-0.6.2.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__winreg__0_7_0",
        url = "https://crates.io/api/v1/crates/winreg/0.7.0/download",
        type = "tar.gz",
        sha256 = "0120db82e8a1e0b9fb3345a539c478767c0048d842860994d96113d5b667bd69",
        strip_prefix = "winreg-0.7.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.winreg-0.7.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__ws2_32_sys__0_2_1",
        url = "https://crates.io/api/v1/crates/ws2_32-sys/0.2.1/download",
        type = "tar.gz",
        sha256 = "d59cefebd0c892fa2dd6de581e937301d8552cb44489cdff035c6187cb63fa5e",
        strip_prefix = "ws2_32-sys-0.2.1",
        build_file = Label("//rust_shared/raze/remote:BUILD.ws2_32-sys-0.2.1.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__wyz__0_2_0",
        url = "https://crates.io/api/v1/crates/wyz/0.2.0/download",
        type = "tar.gz",
        sha256 = "85e60b0d1b5f99db2556934e21937020776a5d31520bf169e851ac44e6420214",
        strip_prefix = "wyz-0.2.0",
        build_file = Label("//rust_shared/raze/remote:BUILD.wyz-0.2.0.bazel"),
    )

    maybe(
        http_archive,
        name = "cards_mono__yasna__0_3_2",
        url = "https://crates.io/api/v1/crates/yasna/0.3.2/download",
        type = "tar.gz",
        sha256 = "0de7bff972b4f2a06c85f6d8454b09df153af7e3a4ec2aac81db1b105b684ddb",
        strip_prefix = "yasna-0.3.2",
        build_file = Label("//rust_shared/raze/remote:BUILD.yasna-0.3.2.bazel"),
    )
